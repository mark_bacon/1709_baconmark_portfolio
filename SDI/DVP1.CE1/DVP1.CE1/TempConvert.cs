﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class TempConvert
    {
        // Name:​ Mark Bacon
        // Date:​ 1709
        // Course:​ Project & Portfolio 1
        // Synopsis:​ part 4 of Code Exercise challenges

        //create member variables
        double mFahrenheitTemperature;
        double mCelsiusTemperature;

        public TempConvert(double _fahrenheitTemperature, double _celsiusTemperature)
        {
            mFahrenheitTemperature = _fahrenheitTemperature;
            mCelsiusTemperature = _celsiusTemperature;

        }

        //Getter methods
        public double GetFahrenheit()
        {
            return mFahrenheitTemperature;
        }

        public double GetCelsius()
        {
            return mCelsiusTemperature;
        }

        //method to convert fahrenheit to celsius
        public double FahrenheitConvert(double _fahrenheitTemperature)
        {
            double CelsiusTemperature;

            CelsiusTemperature= (_fahrenheitTemperature -= 32) / 1.8;

            return CelsiusTemperature;
        }

        //method to convert celsius to fahrenheit
        public double CelsiusConvert(double _celsiusTemperature)
        {
            double FahrenheitTemperature;

            FahrenheitTemperature = (_celsiusTemperature * 1.8) + 32;

            return FahrenheitTemperature;
        }
    }
}
