﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Backwards
    {
        // Name:​ Mark Bacon
        // Date:​ 1709
        // Course:​ Project & Portfolio 1
        // Synopsis:​ part 2 of Code Exercise challenges

        //create member variables
        string mSentence;
        int mSentenceLength;





        public Backwards(string _sentence, int _sentenceLength)
        {
            mSentence = _sentence;
            mSentenceLength = _sentenceLength;

        }

        //Getter Methods
        public string GetSentence()
        {
            return mSentence;
        }

        //getter for sentence length
        public int GetSentenceLength()
        {
            return mSentenceLength;
        }

        //setter methods
        //set new sentence
        public void SetSentence(string _sentence)
        {
            mSentence = _sentence;
        }

        //setter method for sentence length
        public void SetSentenceLength(int _sentenceLength)
        {
            mSentenceLength = _sentenceLength;
        }



        public string reverseSentence(string _sentence)
        {

            char[] sentArray = _sentence.ToCharArray();

            Array.Reverse(sentArray);
            return new string(sentArray);
        }

    }


}
