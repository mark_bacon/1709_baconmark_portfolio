﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class SwapInfo
    {

        // Name:​ Mark Bacon
        // Date:​ 1709
        // Course:​ Project & Portfolio 1
        // Synopsis:​ part 1 of Code Exercise challenges

        //create member variables
        string mFirstName;
        string mLastName;

        //create constructor function
        public SwapInfo(string _firstName, string _lastName)
        {
            mFirstName = _firstName;
            mLastName = _lastName;


        }


        //Getter Methods
        public string GetFirstName()
        {
            //return the first name
            return mFirstName;
        }

        public string GetLastName()
        {
            //return the last name
            return mLastName;
        }


        //setter methods
        //setter method for first name
        public void SetFirstName(string _firstName)
        {
            mFirstName = _firstName;

            while (string.IsNullOrWhiteSpace(_firstName))
            {
                //alert user they have left the entry blank
                Console.WriteLine("You have not entered a first name. \r\n Please enter a first name");
            }
        }
        //setter method for last name
        public void SetLastName(string _lastName)
        {
            mLastName = _lastName;

            while (string.IsNullOrWhiteSpace(_lastName))
            {
                //alert user they have left the entry blank
                Console.WriteLine("You have not entered a last name. \r\n Please enter a last name");
            }
        }

        //create a method that reverses the name 
        public string reverseName(string _firstName, string _lastName)
        {

            string reverseNameString = mLastName + " " + mFirstName;

            return reverseNameString;
        }



    }
}
