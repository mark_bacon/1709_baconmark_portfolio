﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Name:​ Mark Bacon
            // Date:​ 1709
            // Course:​ Project & Portfolio 1
            // Synopsis:​ part 1 of Code Exercise challenges

            while (true)
            {
                Console.WriteLine("\r\nPlease select a program you would like to run:");
                Console.WriteLine("Chose:\r\n(1) for SwapInfo\r\n(2) for Backwards\r\n(3) for AgeConvert\r\n(4) for TempConvert ");

                //capture user selection
                String menuChoiceString = Console.ReadLine();

                //define variable to convert number
                int menuChoiceNumber;

                while (!(int.TryParse(menuChoiceString, out menuChoiceNumber)))
                {
                    //tell user what happened
                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter a number from 1 to 4:");

                    //re-define the variable to save response
                    menuChoiceString = Console.ReadLine();
                }
                //this is an if statement that will catch if somebody enters a number NOT between 1 and 4
                while (!(menuChoiceNumber >= 1 && menuChoiceNumber <= 4))
                {
                    //prompt user that they have entered a choice that is not an option
                    Console.WriteLine("You have entered a number that is not an available choice. Please try again:");

                    //re-define the variable to save response
                    menuChoiceString = Console.ReadLine();
                }

                //*******************start of menu choices
                //Challenge Exercise 1
                //if user selects option 1 for SwapInfo
                if (menuChoiceNumber == 1)
                {
                    //prompt the title of program
                    Console.WriteLine("\r\nWelcome to SwapInfo");

                    //prompt user for their first name
                    Console.WriteLine("\r\nPlease enter your first name:");

                    //capture user data
                    string firstName = Console.ReadLine();
                    while (string.IsNullOrWhiteSpace(firstName))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please enter your first name:");

                        //Capture the response again
                        firstName = Console.ReadLine();
                    }


                    //prompt user for their last name
                    Console.WriteLine("Please enter your last name:");

                    //capture user data
                    string lastName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(lastName))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please enter your last name:");

                        //Capture the response again
                        lastName = Console.ReadLine();
                    }


                    //instantiate new full name to enter
                    SwapInfo firstFullName = new SwapInfo(firstName, lastName);

                    //Display the info that the user has entered.
                    Console.WriteLine("You have entered the name {0} {1}.\r\n Please verify with yes or no if the first name and last name are correct.", firstFullName.GetFirstName(), firstFullName.GetLastName());

                    //capture the user input 
                    string yesOrNoVerify = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(yesOrNoVerify))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please verify with yes or no");

                        //Capture the response again
                        yesOrNoVerify = Console.ReadLine();
                    }

                    if (yesOrNoVerify == "Yes" || yesOrNoVerify == "yes" || yesOrNoVerify == "y")
                    {
                        Console.WriteLine("Thank you for verifying the name {0} {1}", firstFullName.GetFirstName(), firstFullName.GetLastName());

                    }


                    else
                    {

                        Console.WriteLine("Please enter the first name again:");

                        firstFullName.SetFirstName(Console.ReadLine());

                        Console.WriteLine("Please enter the last name again");

                        firstFullName.SetLastName(Console.ReadLine());

                        Console.WriteLine("Thank you for verifying the name {0} {1}", firstFullName.GetFirstName(), firstFullName.GetLastName());



                    }

                    Console.WriteLine("The reversed name is {0}", firstFullName.reverseName(firstName, lastName));


                    /*test data
                     * first name: Mark, Last name: Bacon, reveresed name: Bacon Mark
                    */

                }
                //******************menu choice 2
                //Challenge exercise 2
                //if user selects option 2 for Backwards
                if (menuChoiceNumber == 2)
                {
                    //prompt user to the program selected
                    Console.WriteLine("\r\nWelcome to Backwards");
                    Console.WriteLine("\r\nPlease enter a sentence with a minimum of 6 words");




                    string firstSentenceString = Console.ReadLine();

                    int totalSentenceLength = SentLengthCalc(firstSentenceString);

                    Backwards firstSentence = new Backwards(firstSentenceString, totalSentenceLength);

                    while (firstSentence.GetSentenceLength() < 6)
                    {
                        //prompt user that the sentence is too short
                        Console.WriteLine("The sentence that you have entered \"{0}\" is too short.\r\n Please try again ", firstSentence.GetSentence());

                        firstSentence.SetSentence(Console.ReadLine());
                        firstSentence.SetSentenceLength(SentLengthCalc(firstSentence.GetSentence()));
                    }
                    if (firstSentence.GetSentenceLength() >= 6)
                    {
                        //prompt user that sentence is the correct length
                        Console.WriteLine("\r\nThe sentence that you have entered \"{0}\" is the correct length", firstSentence.GetSentence());

                        Console.WriteLine("\r\nThe reversed sentence is \"{0}\"", firstSentence.reverseSentence(firstSentence.GetSentence()));

                    }




                    //test to count the words in the sentence
                    //Console.WriteLine("there are {0} words in the sentence", firstSentence.sentLength(firstSentenceString));

                    /*test data
                     * sentence entered: today is going to be a great day
                     * reversed sentence: yad taerg a eb ot gniog si yadot


                     */


                }
                //******************menu Choice 3
                //Challenge exercise 3
                //if user selects option 3 for AgeConvert
                if (menuChoiceNumber == 3)
                {
                    Console.WriteLine("Welcome to AgeConvert");
                    //prompt user to enter name
                    Console.WriteLine("\r\nPlease enter your name:");

                    string userName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(userName))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please enter your name:");

                        //Capture the response again
                        userName = Console.ReadLine();
                    }

                    //prompt user to enter age
                    Console.WriteLine("\r\nPlease enter your age:");

                    string userAgeString = Console.ReadLine();

                    int userAgeNumber;

                    while (!(int.TryParse(userAgeString, out userAgeNumber)))
                    {
                        //tell user what happened
                        Console.WriteLine("You have entered something other than a number.\r\nPlease enter your age:");

                        //re-define the variable to save response
                        userAgeString = Console.ReadLine();
                    }

                    //instantiate new name and age to our class 
                    AgeConvert firstAgeConvert = new AgeConvert(userName, userAgeNumber);

                    Console.WriteLine("\r\nPlease confirm that the name and age entered are correct");
                    Console.WriteLine("\r\nThe name that you have entered is {0}.\r\nPlease verify if this is correct by entering yes or no:", firstAgeConvert.GetUserName());

                    //capture the user input 
                    string verifyUserName = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(verifyUserName))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please verify with yes or no");

                        //Capture the response again
                        verifyUserName = Console.ReadLine();
                    }

                    if (verifyUserName == "Yes" || verifyUserName == "yes" || verifyUserName == "y")
                    {
                        Console.WriteLine("Thank you for verifying the name {0}", firstAgeConvert.GetUserName());

                    }


                    else
                    {

                        Console.WriteLine("Please enter the name again:");

                        firstAgeConvert.SetUserName(Console.ReadLine());



                        Console.WriteLine("Thank you for verifying the name {0}", firstAgeConvert.GetUserName());
                    }

                    //Verify user age that they entered
                    Console.WriteLine("The age that you have entered is {0}.\r\nPlease verify if this is correct by entering yes or no:", firstAgeConvert.GetUserAge());

                    //capture the user input 
                    string verifyUserAge = Console.ReadLine();

                    while (string.IsNullOrWhiteSpace(verifyUserAge))
                    {
                        //then this means the user left this blank
                        //Tell the user what went wrong
                        Console.WriteLine("Please do not leave this blank. Please verify with yes or no");

                        //Capture the response again
                        verifyUserAge = Console.ReadLine();
                    }

                    if (verifyUserAge == "Yes" || verifyUserAge == "yes" || verifyUserAge == "y")
                    {
                        Console.WriteLine("Thank you for verifying the age of {0}", firstAgeConvert.GetUserAge());

                    }


                    else
                    {

                        Console.WriteLine("Please enter the age again:");

                        firstAgeConvert.SetUserName(Console.ReadLine());



                        Console.WriteLine("Thank you for verifying the name {0}", firstAgeConvert.GetUserAge());
                    }

                    Console.WriteLine("\r\n{0} your age in days is {1}", firstAgeConvert.GetUserName(), firstAgeConvert.ageConvertDays(firstAgeConvert.GetUserAge()));
                    Console.WriteLine("\r\n{0} your age in hours is {1}", firstAgeConvert.GetUserName(), firstAgeConvert.ageConvertHours(firstAgeConvert.GetUserAge()));
                    Console.WriteLine("\r\n{0} your age in minutes is {1}", firstAgeConvert.GetUserName(), firstAgeConvert.ageConvertMinutes(firstAgeConvert.GetUserAge()));
                    Console.WriteLine("\r\n{0} your age in seconds is {1}", firstAgeConvert.GetUserName(), firstAgeConvert.ageConvertSeconds(firstAgeConvert.GetUserAge()));

                    /*test data
                     * user name Mark, age 37, output data resulted correctly.
                     * user name Amanda, age 32, output data resulted correctly.
                     */

                }
                //******************menu choice 4
                //Challenge exercise 4
                //if user selects option 4 for TempConvert
                if (menuChoiceNumber == 4)
                {

                    //prompt user to the program selected
                    Console.WriteLine("\r\nWelcome to TempConvert");
                    Console.WriteLine("\r\nPlease enter a temperature in Fahrenheit:");

                    string fahrenheitString = Console.ReadLine();
                    double fahrenheitNumber;

                    while (!(double.TryParse(fahrenheitString, out fahrenheitNumber)))
                    {
                        //tell user what happened
                        Console.WriteLine("You have entered something other than a number.\r\nPlease enter the temperature in Fahrenheit:");

                        //re-define the variable to save response
                        fahrenheitString = Console.ReadLine();
                    }

                    Console.WriteLine("\r\nPlease enter a temperature in Celsius:");

                    string celsiusString = Console.ReadLine();
                    double celsiusNumber;

                    while (!(double.TryParse(celsiusString, out celsiusNumber)))
                    {
                        //tell user what happened
                        Console.WriteLine("You have entered something other than a number.\r\nPlease enter the temperature in Celsius:");

                        //re-define the variable to save response
                        celsiusString = Console.ReadLine();
                    }

                    //instantiate the new inputs into the TempConvert class
                    TempConvert firstTempConvert = new TempConvert(fahrenheitNumber, celsiusNumber);

                    Console.WriteLine("\r\nThe temperature entered in Fahrenheit is {0}.\r\nWhen converted to Celsius the temperature is {1}.", firstTempConvert.GetFahrenheit(), firstTempConvert.FahrenheitConvert(firstTempConvert.GetFahrenheit()));

                    

                    Console.WriteLine("\r\nThe temperature entered in Celsius is {0}\r\nWhen converted to Fahrenheit the temperature is {1}.", firstTempConvert.GetCelsius(), firstTempConvert.CelsiusConvert(firstTempConvert.GetCelsius()));

                    /*Test Data
                     * fahrenheit entered:97, Celsius Entered:32
                     * results celsius is 36.1111111 and fahrenhiet is 89.6
                     *
                     */

                }


            }
            }
        

        public static int SentLengthCalc(string a)
        {
            int sentenceLength = 1;
            foreach (char space in a)
            {
                if (space == ' ') sentenceLength++;
            }
            return sentenceLength;
        }


    }
    }


    



