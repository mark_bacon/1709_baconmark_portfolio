﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    class AgeConvert
    {
        // Name:​ Mark Bacon
        // Date:​ 1709
        // Course:​ Project & Portfolio 1
        // Synopsis:​ part 3 of Code Exercise challenges


        //create member variables
        string mUserName;
        int mUserAge;

        //create constructor function
        public AgeConvert(string _UserName, int _UserAge)
        {
            mUserName = _UserName;
            mUserAge = _UserAge;

          

        }

        //getter methods
        public string GetUserName()
        {
            //return user name
            return mUserName;
        }

        public int GetUserAge()
        {
            return mUserAge;
        }

        //setter methods

        public void SetUserName(string _userName)
        {
            //set user name
            mUserName = _userName;
        }

        public void SetUserAge(int _userAge)
        {
            //set user age
            mUserAge = _userAge;
        }

        public int ageConvertDays(int _userAge)
        {
            int userAgeInDays;
            int leapYearDays;

            leapYearDays = _userAge / 4;
            userAgeInDays = (_userAge * 365) + leapYearDays;
            //return the amount of days that the user has been alive
            return userAgeInDays;
            
        }

        public int ageConvertHours(int _userAge)
        {
            int userAgeHours;

            userAgeHours = ((_userAge * 365) + (_userAge / 4))* 24 ;


            return userAgeHours;
        }


        public int ageConvertMinutes(int _userAge)
        {
            int userAgeMinutes;

            userAgeMinutes = (((_userAge * 365) + (_userAge / 4)) * 24) * 60;

            return userAgeMinutes;
        }

        public int ageConvertSeconds(int _userAge)
        {
            int userAgeSeconds;

            userAgeSeconds = ((((_userAge * 365) + (_userAge / 4)) * 24) * 60) * 60;

            return userAgeSeconds;
        }
    }
}
