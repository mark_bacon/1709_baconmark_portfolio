﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaconMark_FindErrorsClasses
{
    class MovieTheater
    {

        //Create member variables
        //describe movie theater
        string mMovieTheaterName;
        string mNumOfScreens;
        int mNumOfMovies;
        decimal mAvgTicketPrice;

        //create the first constructor for the movie theater
        public MovieTheater(string _movieTheaterName, int _numOfMovies, decimal _avgTicketPrices)
        {
            mMovieTheaterName = _movieTheaterName;
            mNumOfMovies = _numOfMovies;
            mAvgTicketPrice = _avgTicketPrices;
        }

        //create the second constructor for the movie theater
        public MovieTheater(string _movieTheaterName, string _numOfScreens, decimal _avgTicketPrices)
        {
            mMovieTheaterName = _movieTheaterName;
            mNumOfScreens = _numOfScreens;
            mAvgTicketPrice = _avgTicketPrices;
        }

        //create the Getters
        public string GetName()
        {
            //returning the value of the name of the movie theater
            return mMovieTheaterName;
        }
        public string GetNumScreens()
        {
            //returning the valude of the number of screens at the theater
            return mNumOfScreens;
        }
        public int GetNumMovies()
        {
            //return the valude of the number of movies
            return mNumOfMovies;
        }
        public decimal GetTicketPrice()
        {
            //return the value for the average ticket price
            return mAvgTicketPrice;
        }

        //create custom method for total ticket price
        public decimal TotalTicketCost(int _ticketsNeeded)
        {

            //create equation for number of tickets times the price of tickets
            decimal TotalTicketCost = _ticketsNeeded * mAvgTicketPrice;

            //return the value total amount for the tickets
            return TotalTicketCost;
        }


        //create setter for ticket price of movie tickets
        public void SetTicketPrice(decimal _ticketCoupon)
        {
            //apply the ticket coupon to the price of the ticket
            mAvgTicketPrice = mAvgTicketPrice - _ticketCoupon;
            //return the new ticket price 
            
        }
    }
}
