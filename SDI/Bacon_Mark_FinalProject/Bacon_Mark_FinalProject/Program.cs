﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Mark Bacon
             * 7/25/2017 - July term
             * SDI Section 01
             * Final Project
             
             */


            
         
            

            //prompt user to enter in a list of events they would like to attend
            Console.WriteLine("Please enter a list of events you would like to buy tickets for.\r\nPlease separate each item with a comma.");

            string ticketListString = Console.ReadLine();
            //verify the user did not leave blank
            while (string.IsNullOrWhiteSpace(ticketListString))
            {
                //then this means the user left this blank
                //Tell the user what went wrong
                Console.WriteLine("Please do not leave this blank. Please enter a list of events you would like to buy tickets for.\r\n Please separate each with a comma.");

                //Capture the response again
                ticketListString = Console.ReadLine();
            }


            Array ticketList = TextToArray(ticketListString);


            // test to cycle through the array using a foreach loop
            /*   foreach (string item in ticketList)
               {
                   Console.WriteLine("{0}",item);

               }

              */

            //function call, input ticketlist array and return ticketCost array list
            ArrayList ticketCost = PromptForCost(ticketList);

            //test to see if items were being placed in the array
            /* foreach (string item in ticketCost)
             {
                 Console.WriteLine("{0}", item);

             }
             */

            //function call the SumOfCosts
            decimal sumOfTickets = SumOfCosts(ticketCost);
            //prompt the user the total of the all the events they listed
            Console.WriteLine("The total costs of all your events will be ${0}", sumOfTickets.ToString("0.00"));


            /* Test Date
             * events - Concert - 35, baseball game- 17, play 48
             * results - "The total costs of all your events will be $100.00"
             * events - rock concert - 76.50, wine tasting - 22.35, football game - 43
             * results - "The total costs of all your events will be $141.85"
             * event - Fiddler on the Roof-50, guardians of the galaxy 2-12,Wonder Woman-15
             * results - "The total costs of all your events will be $77.00"
             * */


        }
        //create method for converting string to array
        public static Array TextToArray(string a)
        {
            string[] text2Array = a.Trim().Split(',');



            return text2Array;
        }

        //convert array to arraylist 
        //add items to new arraylist 
        public static ArrayList PromptForCost(Array a)
        {
            //create new arraylist
            ArrayList arrayListConvert = new ArrayList();
            //add the array items to the new arrayList
            arrayListConvert.AddRange(a);
            //create an arrayList to store the ticket prices
            ArrayList costOfEventsArray = new ArrayList();

            //loop to prompt user how much each event will cost.
            //this will prompt the user for as many events they entered
            foreach (string item in arrayListConvert)
            {
                
                //prompt the user for the cost of each event. 
                //trim off the space after each comma from user response.
                Console.WriteLine("What is the cost of the {0} event?", item.Trim());

                string eventCostString = Console.ReadLine();

                //declare variable
                decimal costOfEvents;

                
                //converting string entered by user to number and also testing to make sure they entered a number.
                while (!(decimal.TryParse(eventCostString, out costOfEvents)))
                {
                    //Alert the user to the error
                    Console.WriteLine("Please only type in numbers and do not leave blank\r\nWhat is the cost of the {0} event?", item);

                    //re-capture the user's response in the SAME variable as before
                    eventCostString = Console.ReadLine();


                }
                //add the items to the array as numbers
                costOfEventsArray.Add(costOfEvents);

              }
            //return the arraylist
            return costOfEventsArray;
           }

        //create method to add up the items in the array list
        public static decimal SumOfCosts(ArrayList a)
        {
            //declare variable and starting point. 
            decimal sum = 0;
            //loop through each of the costs and add them together
            foreach (decimal item in a)
            {
                
              sum= sum + item;
                
            }
            //return the sum of the tickets 
            return sum;

        }
        
    
        }
    }

