﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Mark Bacon
             * 7/22/2017
             * SDI section 01
             * String Objects
             
             */

            //Problem#1: Email address checker

            //prompt user for email address
            Console.WriteLine("Please enter your email address:");

            //Catch user response
            string emailAddressString = Console.ReadLine();

           
     
            //call function
            string verifiedEmail = EmailFunction(emailAddressString);
            Console.WriteLine("The email address of {0} is a valid email address", verifiedEmail);


            /*Test Data
             * email - test@fullsail.edu - "The email address of test@fullsail.com is a valid email"
             * email - test@full@sail.edu - "The email address of test@full@sail.com is not a valid email"
             * email - test@full sail.edu - "The email address of test@full sail.com is not a valid email"
             * email - mrbacon@fullsail.edu
             */


            //Problem#2:Separator Swap Out

            string list = "monday, tuesday, wednesday, thursday";
            char separator = ':';
            char newSeparator = ',';


            Console.WriteLine(list);
            //index of the comma
           // int firstcomma = list.IndexOf(",");

           // Console.WriteLine(firstcomma);
           // string newl



//Console.WriteLine(list.Replace(',', '-'));

            string finalList = SeparatorSwap(list);
            Console.WriteLine("The original list was {0} with new sperator is {1}", list, finalList);

            /*test data
             * list - "1,2,3,4,5", original separator- ",", new separator "-"
             * results - "The original list of 1,2,3,4,5 with the new separator is 1-2-3-4-5"
             * List - "red: blue: green: pink" original separator- ":", new separator- ","
             * results - the original list was red: blue: green: pink  with new separator is red, blue, green, pink
             * list - "monday, tuesday, wednesday, thursday" original separator- ",", new separator ";"
             * results -"The original list of monday, tuesday, wednesday, thursday with new separator is monday; tuesday; wednesday; thursday"
             
             */

            
        }

        public static string SeparatorSwap(string list)
        {

            char separator = ',';
            char newSeparator = ';';
            string newlist = list.Replace(separator, newSeparator);
            return newlist;

        }


        public static string EmailFunction(string e)
        {
            //  Console.WriteLine(e.Contains("@"));
            var atSymbol = "@";
            var period = ".";
            var space = " ";
           
           
           {
                if (e.IndexOf(atSymbol) != e.LastIndexOf(atSymbol) || e.IndexOf(period) != e.LastIndexOf(period) || (e.IndexOf(space) > 0) || e.IndexOf(atSymbol) > e.LastIndexOf(period))
                {


                    Console.WriteLine("The email address of {0} is not valid. Please try again:", e);

                    //re-capture string
                    e = Console.ReadLine();
                    return e;
                }
                else 
                {

                    return e;
                }

            }





            //    Console.WriteLine("You have more than one @ symbol in your email address. Please try again:");
            //     return e.Contains("@");

        }

    }

}
    

