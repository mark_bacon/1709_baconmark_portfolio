﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {   /*
                Mark Bacon
                SDI secion 01
                Restaurant Calculator
                7/2/2017            
             
             
             */
            //Restaurant Calculator
            //Prompt the user for 3 prices of their meals and store their responses inside of the the 


            //prompt user for price of first meal
            Console.WriteLine("Please enter the price of the first meal:");
            //catch the answer for first meal
            string firstMeal = Console.ReadLine();
            //convert string to number
            double firstMealNumber = double.Parse(firstMeal);


            //prompt user for price of second meal
            Console.WriteLine("Please enter the price of the second meal:");
            //catch the answer for second meal
            string secondMeal = Console.ReadLine();
            //convert string to number
            double secondMealNumber = double.Parse(secondMeal);


            //prompt user for price of third meal
            Console.WriteLine("Please enter the price of the third meal:");
            //catch the answer for second meal
            string thirdMeal = Console.ReadLine();
            //convert string to number
            double thirdMealNumber = double.Parse(thirdMeal);

            //prompt the user for the tip % based on how good their was
            Console.WriteLine("Please enter the percentage you would like to tip:");
            //catch the answer for the tip percentage
            string tipPercentage = Console.ReadLine();
            //Convert the string to a number to be used in equations
            double tipPercentageNumber = double.Parse(tipPercentage);
            //Convert percentage to number for later math
            double tipPercMath = tipPercentageNumber / 100;






            //calculate the following with info gathered from the prompts
            //create new variable for solutions
            //subtotal of all the meals 
            double subtotalMeals = firstMealNumber + secondMealNumber + thirdMealNumber;
            Console.WriteLine("SUBTOTAL: "+subtotalMeals);

            //subtotal of meals * tip percerntage equals the tip amount
            double tipTotal = subtotalMeals * tipPercMath;
            Console.WriteLine("TIP: "+tipTotal);

            //total bill including tip add tiptotal and subtotalMeals together 
            //create variable for final bill
            double finalBill = tipTotal + subtotalMeals;
            Console.WriteLine("FINAL TOTAL: "+finalBill);
            //amount per guest to be paid, assuming they will split evenly between them.
            //create variable totalPerPerson, divide finalbill by 3;
            double totalPerPerson = finalBill /3;
            Console.WriteLine("TOTAL PER PERSON: "+totalPerPerson);



            /*Test values used on the Calculator
            Test 1
            check 1 - 10.00
            check 2 - 15.00
            check 3 - 25.00
            tip % - 20
            subtotal without tip - 50.00
            total tip - 10.00
            Grand total with tip - 60.00
            cost per person - 20.00

            test 2
            check 1 - 20.25
            check 2 - 17.75
            check 3 - 23.90
            tip % - 10
            subtotal without tip - 61.90
            total tip - 6.19
            grand total with tip - 68.09
            cost per person - 22.69666 or rounded to 22.70




            */
           
            



        }
    }
}
