﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_CustomClass
{
    class Dog
    {
        //Create the member variables 
        string mDogShelterName;
        int mMaxDogs;
        int mMinDogs;
        int mCurrentDogs;


        //Create Constructor function
        public Dog(string _dogShelterName, int _maxDogs, int _minDogs, int _currentDogs)
        {
            //use incoming variables to initialize member variables
            //create conditional statements for the rules of each variable
            //minimum amount of dogs cannot be less than zero
            mDogShelterName = _dogShelterName;

            if (_minDogs>=0)
            {

                //change the value of member variable
                mMinDogs = _minDogs;

            } else
            {
                //Alert the user to not use negative numbers
                Console.WriteLine("This value can not be negative.\r\nPlease try again.");
            }

            if (_maxDogs > 0)
            {
                //change the value of the member variable
                mMaxDogs = _maxDogs;

            }
            else
            {
                //Alert the user they have entered more a number greater than the shelter can hold
                Console.WriteLine("This value can not be negative.\r\nPlease try again.");
            }

            if (_currentDogs <= mMaxDogs && _currentDogs > 0)
            {
                //change the value of the member variable
                mCurrentDogs = _currentDogs;
            }
            else
            {
                //Alert user they have entered in more dogs than the shelter will hold
                Console.WriteLine("You have entered in more dogs than the shelter can hold.\r\nPlease try again");
            }


            
            
            
           
        }

        //setter methods
        public void SetMaxDogs(int _maxDogs)
        {
            if (_maxDogs < mMaxDogs && _maxDogs > 0)
            {
                //change the value of the member variable
                mMaxDogs = _maxDogs;

            }
            else
            {
                //Alert the user they have entered more a number greater than the shelter can hold
                Console.WriteLine("This value has exceeded the amount the shelter can hold.\r\nPlease try again.");


            }
        }
       
        public void SetMinDogs(int _minDogs)
        {
            if (_minDogs >= 0)
            {

                //change the value of member variable
                mMinDogs = _minDogs;

            }
            else {

                //Alert the user to not use negative numbers
                Console.WriteLine("This value can not be negative.\r\nPlease try again.");
            }


           
        }

        public void SetCurrentDogs(int _currentDogs)
        {

            if (_currentDogs < mMaxDogs && _currentDogs > 0)
            {
                //change the value of the member variable
                mCurrentDogs = _currentDogs;
            }
            else
            {
                //Alert user they have entered in more dogs than the shelter will hold
                Console.WriteLine("You have entered in more dogs than the shelter can hold.\r\nPlease try again");
            }

            
        }

        //Getter Methods
        public string GetDogName()
        {
            //return name of shelter
            return mDogShelterName;
        }
        public int GetMaxDog()
        {
            //return count of maximum amount of dogs
            return mMaxDogs;
        }
        public int GetMinDog()
        {
            //return count of minimum amount of dogs
            //most likely zero
            return mMinDogs;
        }
        public int GetCurrentDog()
        {
            //return total dogs they have currently in the clinic
            return mCurrentDogs;
        }



    }

}
