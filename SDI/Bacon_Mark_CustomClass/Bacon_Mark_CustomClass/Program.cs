﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Mark Bacon
             * 7/26/2017
             * SDI Section 01
             * Assignment Custom Class - Animal shelter
             
             */
            //instantiate the animal shelter (max, min, current)
            Dog harvestHills = new Dog("Harvest Hills Animal Clinic", 25, 0, 10);



            //create a for loop to run 5 times asking if dogs have been added or removed to the shelter
            for (int i = 0; i <= 5; i++)
            {

                //ask user if dogs have been added or removed
                Console.WriteLine("Have there been any dogs added or removed from the {0}?\r\nPlease enter yes or no:", harvestHills.GetDogName());

                string yesOrNo = Console.ReadLine();
                if (yesOrNo == "no" || yesOrNo == "No")

                {
                    Console.WriteLine("No updates to the current dog amount this time.");
                }
                else if (yesOrNo == "yes" || yesOrNo == "Yes")
                {
                    Console.WriteLine("What is the new current dog total for the shelter?");

                    string newDogtotal = Console.ReadLine();

                    int dogTotalNum;

                    while (!(int.TryParse(newDogtotal, out dogTotalNum)))
                    {
                        //Alert the user to the error 
                        Console.WriteLine("Please only type in numbers and do not leave blank!\r\nWhat is the current dog total for the shelter?");

                        //re-capture the user's response in the SAME variable as before 
                        newDogtotal = Console.ReadLine();
                    }
                    //set the new current dog total amount to the member variable
                    harvestHills.SetCurrentDogs(dogTotalNum);
                    Console.WriteLine("The current amount of dogs for {0} is {1}.", harvestHills.GetDogName(), harvestHills.GetCurrentDog());

                }
                else
                {
                    //prompt user that something other than yes or no was entered

                    int newYesOrNo;

                    while (!(int.TryParse(yesOrNo, out newYesOrNo)))
                    {
                        //Alert the user to the error 
                        Console.WriteLine("You entered an invalid response, Please only enter yes or no.");

                        //re-capture the user's response in the SAME variable as before 
                        yesOrNo = Console.ReadLine();
                    }


                }

                Console.WriteLine("The maximum amount of dogs for {0} is {1}.", harvestHills.GetDogName(), harvestHills.GetMaxDog());
                Console.WriteLine("The minimum amount of dogs for {0} is {1}.", harvestHills.GetDogName(), harvestHills.GetMinDog());


            }




        }
    }
}


