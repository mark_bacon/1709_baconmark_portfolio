﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             Mark Bacon
             SDI secion 01
             Madlibs assignment
             7/2/2017
             
             */

            //prompt users for 4 different types of strings
            
            //asking the user for a color
            Console.WriteLine("Please enter your favorite color: ");
            //catch the answer for the color and gie variable name
            string firstWord = Console.ReadLine();
           
            //ask user for noun
            Console.WriteLine("Please enter a noun: ");
            //catch the answer for the noun and give variable name
            string secondWord = Console.ReadLine();

            //ask user for a verb
            Console.WriteLine("Please enter a verb: ");
            //catch the answer for the verb and give variable name
            string thirdWord = Console.ReadLine();

            //ask user for their favorite animal
            Console.WriteLine("Please enter your favorite animal: ");
            //catch the answer for favorite animal and give variable name
            string fourthWord = Console.ReadLine();

            //prompt the users for 3 numbers

            //ask user for day number
            Console.WriteLine("Please enter day number of you birthday: ");
            //catch the answer for b-day number and give variable name
            string firstNumber= Console.ReadLine();
            //convert string to number 
            int madNumberOne = int.Parse(firstNumber);

            //ask user for age
            Console.WriteLine("Please enter your age: ");
            //catch the answer for age and give it a variable name
            string secondNumber = Console.ReadLine();
            //convert string to number 
            int madNumberTwo = int.Parse(secondNumber);

            //ask user how many pets they have
            Console.WriteLine("Please enter how many pets you have: ");
            //catch the answer to question and give it a variable name
            string thirdNumber = Console.ReadLine();
            //convert string to number
            int madNumberThree = int.Parse(thirdNumber);

            //put new numbers in to an array 
            int numberArray = madNumberOne + madNumberTwo + madNumberThree;


            Console.Write("There was once a boy named Jack. He owned an amazing "+fourthWord+". Jack's pet "+fourthWord+" had "+madNumberOne+" "+firstWord+" stripes all over his body with "+madNumberTwo+ " eyes and "+madNumberThree+" legs. Jack named his pet "+secondWord+". "+secondWord+" loved to "+thirdWord+" all of the time.");
           

            /*
             test 1
             favorite color - green
             noun - california
             verb - run
             animal - giraffe
             day number for birthday - 16
             age - 31
             number of pets - 9
             
             

             
             
             
             */



        }
    }
}
