﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_Methods
{
    class Program
    {
        static void Main(string[] args)
        {

            /* Mark Bacon
             * 7/20/2017
             * SDI section 01
             * methods
             */

            //Problem#1 Painting a Wall

            Console.WriteLine("Paint Calculator");

            //ask for the width of the wall in feet
            Console.WriteLine("What is the width of the wall in feet?");

            //capture user response
            string widthWallString = Console.ReadLine();

            //declare variable for converting number to
            decimal width;

            //validate with while loop and convert number
            while (!(decimal.TryParse(widthWallString, out width)))
            {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank\r\nWhat is the width of the wall?");

                //re-capture the user's response in the SAME variable as before
                widthWallString = Console.ReadLine();

            }



            //ask for the height of the wall in feet
            Console.WriteLine("What is the height of the wall in feet?");

            //capture user response
            string heightWallString = Console.ReadLine();

            //declare variable for converting 
            decimal height;

            //validate with while loop and convert number
            while (!(decimal.TryParse(heightWallString, out height)))
            {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank\r\nWhat is the height of the wall?");

                //re-capture the user's response in the SAME variable as before
                heightWallString = Console.ReadLine();

            }


            //number of coats of paint
            Console.WriteLine("How many coats of paint will be applied?");

            //capture user response
            string coatsPaintString = Console.ReadLine();

            //declare variable for converting
            decimal coatsPaint;

            //validate with while loop and convert number
            while (!(decimal.TryParse(coatsPaintString, out coatsPaint)))
            {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank\r\nHow many coats of paint would you like to apply?");

                //re-capture the user's response in the SAME variable as before
                coatsPaintString = Console.ReadLine();

            }


            //ask the surface area one gallon of paint will cover in feet squared
            Console.WriteLine("What is the surface area one gallon of paint will cover in feet squared?");

            //capture user response
            string gallonPaintCoversString = Console.ReadLine();

            //declare variable for converting
            decimal gallonPaintCovers;

            //Validate with while loop and convert number
            while (!(decimal.TryParse(gallonPaintCoversString, out gallonPaintCovers)))
            {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank\r\nWhat is the surface area one gallon of paint will cover in feet squared?");

                //re-capture the user's response in the SAME variable as before
                gallonPaintCoversString = Console.ReadLine();

            }


            ////Tell the user
            Console.WriteLine("You typed in a width of {0} in feet, a height of {1} in feet, number of coats of paint is {2}, and the surface area for one gallon of paint is {3} in feet squared.\r\n", width, height, coatsPaint, gallonPaintCovers);

            //function call and save variable
            decimal paintCans = CalcPaint(width, height, coatsPaint, gallonPaintCovers);



            //Tell the user how much paint they will need
            Console.WriteLine("For {0} coats on that wall, you will need {1} gallons of paint.\r\n", coatsPaint, paintCans.ToString("0.00"));


            /*test data
             * Width-8, height-10, coats-2, surface area-300
             *  results-""For {0} coats on that wall, you will need {1} gallons of paint.""
             *  width-30, height-12.5, coats-3, surface area-350
             *  "For 3 coats on that wall, you will need 3.21 gallons of paint."
             *  width-16, height-8, coats-2, surface area-200
             *  "For 2 coats on that wall, you will need 1.28 gallons of paint."
             */


            //Problem#2:Stung!
            Console.WriteLine("Bee Sting Calculator");
            Console.WriteLine("What is the weight of the animal in pounds?");

            //capture user response
            string animalWeightString = Console.ReadLine();

            //declare variable for converting
            int animalWeight;

            //validate with while loop and convert number
            while (!(int.TryParse(animalWeightString, out animalWeight)))
            {
                //Alert the user to the error
                Console.WriteLine("Please only type in numbers and do not leave blank\r\nWhat is the weight of the animal in pounds?");

                //re-capture the user's response in the SAME variable as before
                animalWeightString = Console.ReadLine();

            }



            int totalBeeStings = BeeStingCalc(animalWeight);
            Console.WriteLine("It takes {0} bee stings to kill this animal.", totalBeeStings);


            /*test data
             * animal weight - 10, results - “It takes 90 bee stings to kill this animal."
             * animal weight - 160, results - "It takes 1440 bee stings to kill this animal."
             * animal weight - twenty - re-prompt for number value
             * animal weight - 22 - results - "It takes 198 bee stings to kill this animal."
             */





            //Problem#3: Reverse It


            ArrayList listOfItems = new ArrayList() { "apple", "pear","peach", "coconut", "kiwi"};



            Console.WriteLine(listOfItems.Count);

            //function call 
            ArrayList finalArray = ReverseCalc(listOfItems);

            foreach (string item in finalArray)
            {
                Console.WriteLine(item);
            }

           

            /*test data
             * Initial array – [“apple”, “pear”, “peach”, “coconut”, “kiwi”]
                 Results reversed as	[“kiwi”, “coconut”,	“peach”, “pear”, “apple”]
                Initial array	– [“red”,	“orange”,	“yellow”,	“green”, “blue”, ”indigo”,	“violet”]
                 Results reversed as	[“violet”,	“indigo”, “blue”, “green”, “yellow”, “orange”, “red”]
             
             */



        }

  /*      public static ArrayList ReverseCalc(ArrayList a)
        {   foreach (string item in a) {
                var b =a.Count;
                Console.WriteLine(b);
                return a;
            }
        } 
*/
        public static ArrayList ReverseCalc(ArrayList a )
        {

            ArrayList backwardsList = new ArrayList() { };
            for (int i = a.Count - 1; i >= 0; i--)
            {

               
                // Console.WriteLine(a[i]);
                backwardsList.Add(a[i]);

                
            }
            return backwardsList;

        }

        public static decimal CalcPaint(decimal w, decimal h, decimal c, decimal g)
        {
            //do the math and store the variable
            decimal gallonsPaint = (w * h * c) / g;
            //return the value
            return gallonsPaint;

        }

        public static int BeeStingCalc(int w)
        {
            //do the math and storage the 
            int beeStings = w * 9;
            //return the value 
            return beeStings;

        }






    }



        




    }




