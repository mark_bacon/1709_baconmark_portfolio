﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;


namespace MarkBacon_CE07
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE07
     Application crashes under normal use. -5 points

Your grade before penalties would have been 92%.

Structure / Efficiency: Excellent 10/10
Layout: Good 7/10, The textboxes should be wide enough to accommodate the text.
Web: Excellent 20/20
User Input Controls: Excellent 20/20
File I/O: Good 15/20, When trying to load a file not saved by the application crashes the application with an argument out of range exception.
Main Form: Excellent 20/20
     */
    public partial class Form1 : Form
    {
        //add some instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();
        // API starting point for VFW
        string apiVfwPoint = "http://mdv-vfw.com/vfw.json";

        //API starting point for ASD
        string apiAsdPoint = "http://mdv-vfw.com/asd.json";

        const string ApplicationIdentifier = "MarkBacon_CE07";

        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rdoAsdClass.Checked = true;
            ClearFields();
        }

        private void ClearFields()
        {
            txtClassName.Text = "";
            txtCourseCode.Text = "";
            txtDescription.Text = "";
            numMonth.Value = 0;
            numCreditHours.Value = 0;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            var endpoint = rdoAsdClass.Checked ? apiAsdPoint : apiVfwPoint;

            var client = new WebClient();
            try
            {
                var downloadedData = client.DownloadString(endpoint);

                //Class downloadedClass = JsonConvert.DeserializeObject<Class>(downloadedData);
                JObject parsed = JObject.Parse(downloadedData);

                var downloadedClass = new Class();

                downloadedClass.CourseName = parsed["class"]["course_name_clean"].ToString();
                downloadedClass.CourseCode = parsed["class"]["course_code_long"].ToString();
                downloadedClass.Description = parsed["class"]["course_description"].ToString();
                downloadedClass.CreditHours = Convert.ToInt32(parsed["class"]["credit"]);
                downloadedClass.Month = Convert.ToInt32(parsed["class"]["sequence"]);

                PopulateFields(downloadedClass);
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("There was an error parsing the data: " + ex.Message);
                ClearFields();
            }
            catch (WebException ex)
            {
                MessageBox.Show("There was an error downloading the data: " + ex.Message);
                ClearFields();
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a general error: " + ex.Message);
                ClearFields();
            }

        }
        private void PopulateFields(Class fromClass)
        {
            this.txtClassName.Text = fromClass.CourseName;
            this.txtCourseCode.Text = fromClass.CourseCode;
            this.txtDescription.Text = fromClass.Description;
            this.numMonth.Value = fromClass.Month;
            this.numCreditHours.Value = fromClass.CreditHours;


        }

        private Class LoadClass(string fileName)
        {
            var fileContents = File.ReadAllText(fileName);

            int index = fileContents.IndexOf(Environment.NewLine);
            var firstLine = fileContents.Substring(0, index);

            if (firstLine == ApplicationIdentifier)
            {

                // Remove the identifier line
                fileContents = fileContents.Substring(index + Environment.NewLine.Length);

                // Load the class object from the rest of the string
                var loadedClass = new Class();

                var split = fileContents.Split(new string[] { Class.Separator }, StringSplitOptions.None);

                loadedClass.CourseName = split[0];
                loadedClass.CourseCode = split[1];
                loadedClass.Description = split[2];
                loadedClass.Month = Convert.ToInt32(split[3]);
                loadedClass.CreditHours = Convert.ToInt32(split[4]);

                return loadedClass;
            }
            else
            {
                MessageBox.Show("Invalid File");
            }

            return new Class();
        }

        private void Save(Class classToSave, string destinationFile)
        {
            using (var sw = new StreamWriter(destinationFile))
            {
                sw.WriteLine(ApplicationIdentifier);
                sw.WriteLine(classToSave.ToString());
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                //var classFromFile = Load(dlg.FileName);
                //PopulateFields(classFromFile);

                var classToSave = new Class
                {
                    CourseCode = txtCourseCode.Text,
                    CourseName = txtClassName.Text,
                    Description = txtDescription.Text,
                    CreditHours = (int)numCreditHours.Value,
                    Month = (int)numMonth.Value
                };

                Save(classToSave, dlg.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                var classFromFile = LoadClass(dlg.FileName);
                PopulateFields(classFromFile);
            }
        }
    }
    }

