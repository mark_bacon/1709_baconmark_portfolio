﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE07
     */

    class Class
    {
        string courseName;
        string courseCode;
        string description;
        int month;
        int creditHours;

        public string CourseName { get => courseName; set => courseName = value; }
        public string CourseCode { get => courseCode; set => courseCode = value; }
        public string Description { get => description; set => description = value; }
        public int Month { get => month; set => month = value; }
        public int CreditHours { get => creditHours; set => creditHours = value; }

        public override string ToString()
        {
            return CourseName + Separator + CourseCode + Separator + Description  + Separator + Month + Separator + CreditHours; // TODO put all the fields
        }

        public static string Separator = "##";
    }
}
