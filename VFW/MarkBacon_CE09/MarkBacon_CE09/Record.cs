﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE09
{
    /*Mark Bacon
     * VFW
     * May 2018
     * CE09
     */
    public class Record
    {
        string dvdTitle;
        string year;
        string rating;
        string publicRating;

        public string DvdTitle { get => dvdTitle; set => dvdTitle = value; }
        public string Year { get => year; set => year = value; }
        public string Rating { get => rating; set => rating = value; }
        public string PublicRating { get => publicRating; set => publicRating = value; }
    }
}
