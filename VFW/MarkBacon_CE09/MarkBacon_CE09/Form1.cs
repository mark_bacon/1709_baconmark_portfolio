﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//directive to include MySQL
using MySql.Data.MySqlClient;
//directive to use File IO
using System.IO;
//directive for saving XML file
using System.Xml;

namespace MarkBacon_CE09
{
    /*Mark Bacon
     * VFW
     * May 2018
     * CE09
     */

    public partial class Form1 : Form
    {

        //global connection variable
        MySqlConnection conn = new MySqlConnection();

        //make data table global so we can use it again. It is only populated in RetrieveData() method. 
        DataTable theData = new DataTable();

        //row counter
        int currentRow = 0;

        public Form1()
        {
            InitializeComponent();

            //invoke the method to build the connection string
            string connString = BuildConnectionString();

            //invoke the method to create the connection
            Connect(connString);

            //get the data from the database
            RetrieveData();
        }



        //method to retrieve data from example_1709 database
        public bool RetrieveData()
        {
            //create the SQL statement thats going to get our data
            string sql = "SELECT DVD_Title, Year, Rating, publicRating FROM DVD LIMIT 25;";

            //create the mySQL data adapter
            MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

            //set the type for the select command
            adr.SelectCommand.CommandType = CommandType.Text;

            //now we can instantiate the table that will hold the record set
            // DataTable theData = new DataTable();

            //This method adds the rows from the data source to the DataTable using the MySQLDataAdapter
            adr.Fill(theData);
            
            //now we can put the first record data into the form
            UpdateInterface();

            conn.Close();

            return true;
        }

        //method to connect to mySql database
        public void Connect(string myConnString)
        {
            try
            {
                conn.ConnectionString = myConnString;
                conn.Open();
                //DEBUG: show a message box
                //MessageBox.Show("Connected!");
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        MessageBox.Show("Can't resolve the host address.\n" + myConnString);
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password.");
                        break;
                    default:
                        MessageBox.Show(e.ToString() + "\n" + myConnString);
                        break;
                }

            }
        }

        //method to build the connection string
        public string BuildConnectionString()
        {
            //variable to hold the IP address
            string serverIP = "";

            try
            {
                //open the text files using the stream reader
                using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
                {
                    //read the server data from the text file
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            //var builder = new MySqlConnectionStringBuilder();

            //builder.UserID = "dbsAdmin";
            //builder.Password = "password";
            //builder.Server = serverIP;
            //builder.Database = "exampleDatabase";
            //builder.Port = 8889;
            //this fixes the SSL bug so that the latest versions of MySQL directive can be installed. 
            //builder.SslMode = MySqlSslMode.None;

            //return builder.ToString();


            //return the entire connection string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;sslmode=None;port=8889";
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //should save the current record to a properly formatted XML file   

            var dlg = new SaveFileDialog();

            dlg.DefaultExt = "xml";
            dlg.Filter = "XML files (*.xml)|*.xml";

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                var recordToSave = new Record
                {
                    DvdTitle = txtDVDTitle.Text,
                    Year = numYear.Text,
                    Rating = cmbRating.Text,
                    PublicRating = numPublicRating.Text

                };
                SaveXml(recordToSave, dlg.FileName);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ToolTip TP = new ToolTip();
            TP.ShowAlways = true;
            TP.SetToolTip(txtDVDTitle, $"{txtDVDTitle.Text}");

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //check to see if we are on the last record
            //we dont want to cause an error when it can't return anything beyond last record.
            if (currentRow + 1 < theData.Select().Length)
            {
                currentRow++;
                //update the data on the form
                UpdateInterface();
            }

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            //check to see if we are on the first record
            //we dont want to go past the first record
            if (currentRow > 0)
            {
                currentRow--;
                //update the data on the form
                UpdateInterface();
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            //need to go to the last record available
            //check to 

            //go to the end of the data
            currentRow = theData.Select().Length - 1;

            UpdateInterface();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {

            //go to the beginning of the data
            currentRow = 0;

            UpdateInterface();
        }

        public void UpdateInterface()
        {
            //we can get the count of the number of rows using the
            //select method, which returns an array of the DataRow
            //objects
            int numberOfRecords = theData.Select().Length;

            if (numberOfRecords <= 0)
            {
                lblStatus.Text = "No records returned";

                btnStart.Enabled = false;
                btnPrevious.Enabled = false;
                btnNext.Enabled = false;
                btnLast.Enabled = false;

            }
            else
            {
                txtDVDTitle.Text = theData.Rows[currentRow]["DVD_Title"].ToString();
                numYear.Text = theData.Rows[currentRow]["Year"].ToString();
                cmbRating.Text = theData.Rows[currentRow]["Rating"].ToString();
                numPublicRating.Text = theData.Rows[currentRow]["publicRating"].ToString();

                lblStatus.Text = $"Record {currentRow + 1} of {numberOfRecords}";

                btnStart.Enabled = currentRow > 0;
                btnPrevious.Enabled = btnStart.Enabled;

                btnNext.Enabled = currentRow < numberOfRecords - 1;
                btnLast.Enabled = btnNext.Enabled;
            }


        }

        private void SaveXml(Record recordToSave, string destinationFile)
        {
            var settings = new XmlWriterSettings
            {
                ConformanceLevel = ConformanceLevel.Document
            };

            using (var writer = XmlWriter.Create(destinationFile, settings))
            {
                writer.WriteStartElement("Record");

                writer.WriteElementString("DVDTitle", recordToSave.DvdTitle);
                writer.WriteElementString("Year", recordToSave.Year);
                writer.WriteElementString("Rating", recordToSave.Rating);
                writer.WriteElementString("PublicRating", recordToSave.PublicRating);

                writer.WriteEndElement();
            }
        }
    }
}
