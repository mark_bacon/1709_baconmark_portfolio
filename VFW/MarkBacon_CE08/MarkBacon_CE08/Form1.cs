﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Xml;

namespace MarkBacon_CE08
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE08

        Application crashes under normal use. -5 points

Your grade before penalties would have been 92%.

Structure / Efficiency: Excellent 10/10
Layout: Good 7/10, The textboxes should be wide enough to accommodate the text. Also, the Data Selection groupBox is too high on the form and covers the bottom of the menu highlight when the mouse moves over it.
Web: Excellent 20/20
User Input Controls: Excellent 20/20
File I/O: Good 15/20, When trying to load a file not saved by the application crashes the application with an argument out of range exception.
Main Form: Excellent 20/20
     */


    public partial class Form1 : Form
    {
        //add some instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();
        // API starting point for VFW
        string apiVfwPoint = "http://mdv-vfw.com/vfw";

        //API starting point for ASD
        string apiAsdPoint = "http://mdv-vfw.com/asd";

        const string ApplicationIdentifier = "MarkBacon_CE08";

        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rdoAsdClass.Checked = true;
            ClearFields();
        }

        private void ClearFields()
        {
            txtClassName.Text = "";
            txtCourseCode.Text = "";
            txtDescription.Text = "";
            numMonth.Value = 0;
            numCreditHours.Value = 0;
        }

        private Class ReadFromJson(string endpoint)
        {
            var client = new WebClient();
            Class downloadedResult = null;

            try
            {
                var downloadedData = client.DownloadString(endpoint);

                JObject parsed = JObject.Parse(downloadedData);

                downloadedResult = new Class();

                downloadedResult.CourseName = parsed["class"]["course_name_clean"].ToString();
                downloadedResult.CourseCode = parsed["class"]["course_code_long"].ToString();
                downloadedResult.Description = parsed["class"]["course_description"].ToString();
                downloadedResult.CreditHours = Convert.ToInt32(parsed["class"]["credit"]);
                downloadedResult.Month = Convert.ToInt32(parsed["class"]["sequence"]);

            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("There was an error parsing the data: " + ex.Message);
            }
            catch (WebException ex)
            {
                MessageBox.Show("There was an error downloading the data: " + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a general error: " + ex.Message);
            }

            return downloadedResult;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            var endpoint = rdoAsdClass.Checked ? apiAsdPoint : apiVfwPoint;

            var downloadedClass = rdoJSON.Checked ? ReadFromJson(endpoint + ".json") : ReadFromXML(endpoint + ".xml");

            if (downloadedClass != null)
            {
                PopulateFields(downloadedClass);
            }
            else
            {
                ClearFields();
            }


        }

        private Class ReadFromXML(string endpoint)
        {
            Class downloadedResult = null;
            try
            {
                using (XmlReader reader = XmlReader.Create(endpoint))
                {
                    downloadedResult = new Class();
                    while (reader.Read())
                    {
                        switch (reader.Name.ToLower())
                        {
                            case "course_name_clean":
                                downloadedResult.CourseName = reader.ReadElementContentAsString();
                                break;
                            case "course_code_long":
                                downloadedResult.CourseCode = reader.ReadElementContentAsString();
                                break;
                            case "course_description":
                                downloadedResult.Description = reader.ReadElementContentAsString();
                                break;
                            case "credit":
                                downloadedResult.CreditHours = reader.ReadElementContentAsInt();
                                break;
                            case "sequence":
                                downloadedResult.Month = reader.ReadElementContentAsInt();
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a general error: " + ex.Message);
            }
            return downloadedResult;
        }

        private void PopulateFields(Class fromClass)
        {
            this.txtClassName.Text = fromClass.CourseName;
            this.txtCourseCode.Text = fromClass.CourseCode;
            this.txtDescription.Text = fromClass.Description;
            this.numMonth.Value = fromClass.Month;
            this.numCreditHours.Value = fromClass.CreditHours;


        }

        private Class LoadTxt(string fileName)
        {
            var fileContents = File.ReadAllText(fileName);

            int index = fileContents.IndexOf(Environment.NewLine);
            var firstLine = fileContents.Substring(0, index);

            if (firstLine == ApplicationIdentifier)
            {

                // Remove the identifier line
                fileContents = fileContents.Substring(index + Environment.NewLine.Length);

                // Load the class object from the rest of the string
                var loadedClass = new Class();

                var split = fileContents.Split(new string[] { Class.Separator }, StringSplitOptions.None);

                loadedClass.CourseName = split[0];
                loadedClass.CourseCode = split[1];
                loadedClass.Description = split[2];
                loadedClass.Month = Convert.ToInt32(split[3]);
                loadedClass.CreditHours = Convert.ToInt32(split[4]);

                return loadedClass;
            }
            else
            {
                MessageBox.Show("Invalid File");
            }

            return null;
        }

        private Class LoadXml(string fileName)
        {
            try
            {
                string loadedIdentifier = null;

                var settings = new XmlReaderSettings()
                {
                    IgnoreComments = true,
                    IgnoreWhitespace = true,
                    
                };
                using (XmlReader reader = XmlReader.Create(fileName, settings))
                {
                    reader.MoveToContent();

                    var loadedResult = new Class();

                    // https://stackoverflow.com/questions/24991218/c-sharp-xmlreader-skips-nodes-after-using-readelementcontentas

                    while (reader.Read())
                    {
                        if (reader.Name == "Identifier" && reader.IsStartElement())
                        {
                            loadedIdentifier = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "CourseCode" && reader.IsStartElement())
                        {
                            loadedResult.CourseCode = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "CourseName" && reader.IsStartElement())
                        {
                            loadedResult.CourseName = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "CreditHours" && reader.IsStartElement())
                        {
                            loadedResult.CreditHours = reader.ReadElementContentAsInt();
                        }
                        if (reader.Name == "Description" && reader.IsStartElement())
                        {
                            loadedResult.Description = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "Month" && reader.IsStartElement())
                        {
                            loadedResult.Month = reader.ReadElementContentAsInt();
                        }
                    }

                    if (loadedIdentifier != ApplicationIdentifier)
                    {
                        throw new Exception("Invalid File");
                    }

                    return loadedResult;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a general error: " + ex.Message);
            }

            return null;
        }

        private void SaveText(Class classToSave, string destinationFile)
        {
            using (var sw = new StreamWriter(destinationFile))
            {
                sw.WriteLine(ApplicationIdentifier);
                sw.WriteLine(classToSave.ToString());
            }
        }

        private void SaveXml(Class classToSave, string destinationFile)
        {
            var settings = new XmlWriterSettings
            {
                ConformanceLevel = ConformanceLevel.Document
            };

            using (var writer = XmlWriter.Create(destinationFile, settings))
            {
                writer.WriteStartElement("Class");
                writer.WriteElementString("Identifier", ApplicationIdentifier);
                writer.WriteElementString("CourseCode", classToSave.CourseCode);
                writer.WriteElementString("CourseName", classToSave.CourseName);
                writer.WriteElementString("CreditHours", classToSave.CreditHours.ToString());
                writer.WriteElementString("Description", classToSave.Description);
                writer.WriteElementString("Month", classToSave.Month.ToString());
                writer.WriteEndElement();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new SaveFileDialog();
            if (rdoJSON.Checked)
            {
                dlg.DefaultExt = "txt";
                dlg.Filter = "Text files (*.txt)|*.txt";
            }
            else
            {
                dlg.DefaultExt = "xml";
                dlg.Filter = "XML files (*.xml)|*.xml";
            }

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                //var classFromFile = Load(dlg.FileName);
                //PopulateFields(classFromFile);

                var classToSave = new Class
                {
                    CourseCode = txtCourseCode.Text,
                    CourseName = txtClassName.Text,
                    Description = txtDescription.Text,
                    CreditHours = (int)numCreditHours.Value,
                    Month = (int)numMonth.Value
                };

                if (rdoJSON.Checked)
                {
                    SaveText(classToSave, dlg.FileName);
                }
                else
                {
                    SaveXml(classToSave, dlg.FileName);
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();

            dlg.Filter = "XML files (*.xml)|*.xml|Text Files (*.txt)|*.txt";

            var result = dlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                // C:\folder\file.txt

                var extension = Path.GetExtension(dlg.FileName);

                Class classFromFile = null;

                if (extension == ".txt")
                {
                    classFromFile = LoadTxt(dlg.FileName);
                }
                else if (extension == ".xml")
                {
                    classFromFile = LoadXml(dlg.FileName);

                }

                if (classFromFile != null)
                {
                    PopulateFields(classFromFile);
                }
                else
                {
                    ClearFields();
                }
            }
        }
    }
}

