﻿namespace MarkBacon_CE05
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largeIconToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallIconToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stsStrpNumberOfShips = new System.Windows.Forms.StatusStrip();
            this.toolStrpNumberOfShips = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblShipCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.lvwShips = new System.Windows.Forms.ListView();
            this.imageListLargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.imageListSmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1.SuspendLayout();
            this.stsStrpNumberOfShips.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.shipToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(694, 42);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(324, 38);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(324, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // shipToolStripMenuItem
            // 
            this.shipToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem});
            this.shipToolStripMenuItem.Name = "shipToolStripMenuItem";
            this.shipToolStripMenuItem.Size = new System.Drawing.Size(74, 38);
            this.shipToolStripMenuItem.Text = "Ship";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(324, 38);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.largeIconToolStripMenuItem,
            this.smallIconToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(78, 38);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // largeIconToolStripMenuItem
            // 
            this.largeIconToolStripMenuItem.Name = "largeIconToolStripMenuItem";
            this.largeIconToolStripMenuItem.Size = new System.Drawing.Size(324, 38);
            this.largeIconToolStripMenuItem.Text = "Large Icon";
            this.largeIconToolStripMenuItem.Click += new System.EventHandler(this.largeIconToolStripMenuItem_Click);
            // 
            // smallIconToolStripMenuItem
            // 
            this.smallIconToolStripMenuItem.Name = "smallIconToolStripMenuItem";
            this.smallIconToolStripMenuItem.Size = new System.Drawing.Size(324, 38);
            this.smallIconToolStripMenuItem.Text = "Small Icon";
            this.smallIconToolStripMenuItem.Click += new System.EventHandler(this.smallIconToolStripMenuItem_Click);
            // 
            // stsStrpNumberOfShips
            // 
            this.stsStrpNumberOfShips.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.stsStrpNumberOfShips.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrpNumberOfShips,
            this.lblShipCount});
            this.stsStrpNumberOfShips.Location = new System.Drawing.Point(0, 529);
            this.stsStrpNumberOfShips.Name = "stsStrpNumberOfShips";
            this.stsStrpNumberOfShips.Size = new System.Drawing.Size(694, 37);
            this.stsStrpNumberOfShips.TabIndex = 1;
            this.stsStrpNumberOfShips.Text = "statusStrip1";
            // 
            // toolStrpNumberOfShips
            // 
            this.toolStrpNumberOfShips.Name = "toolStrpNumberOfShips";
            this.toolStrpNumberOfShips.Size = new System.Drawing.Size(198, 32);
            this.toolStrpNumberOfShips.Text = "Number of ships:";
            // 
            // lblShipCount
            // 
            this.lblShipCount.Name = "lblShipCount";
            this.lblShipCount.Size = new System.Drawing.Size(28, 32);
            this.lblShipCount.Text = "0";
            // 
            // lvwShips
            // 
            this.lvwShips.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwShips.LargeImageList = this.imageListLargeIcons;
            this.lvwShips.Location = new System.Drawing.Point(0, 42);
            this.lvwShips.Name = "lvwShips";
            this.lvwShips.Size = new System.Drawing.Size(694, 487);
            this.lvwShips.SmallImageList = this.imageListSmallIcons;
            this.lvwShips.TabIndex = 2;
            this.lvwShips.UseCompatibleStateImageBehavior = false;
            this.lvwShips.View = System.Windows.Forms.View.SmallIcon;
            this.lvwShips.DoubleClick += new System.EventHandler(this.lvwShips_DoubleClick);
            // 
            // imageListLargeIcons
            // 
            this.imageListLargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLargeIcons.ImageStream")));
            this.imageListLargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLargeIcons.Images.SetKeyName(0, "cruiserLarge.png");
            this.imageListLargeIcons.Images.SetKeyName(1, "destroyerLarge.png");
            this.imageListLargeIcons.Images.SetKeyName(2, "freighterLarge.png");
            // 
            // imageListSmallIcons
            // 
            this.imageListSmallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmallIcons.ImageStream")));
            this.imageListSmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListSmallIcons.Images.SetKeyName(0, "cruiserSmall.png");
            this.imageListSmallIcons.Images.SetKeyName(1, "destroyerSmall.png");
            this.imageListSmallIcons.Images.SetKeyName(2, "freighterSmall.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 566);
            this.Controls.Add(this.lvwShips);
            this.Controls.Add(this.stsStrpNumberOfShips);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Main";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.stsStrpNumberOfShips.ResumeLayout(false);
            this.stsStrpNumberOfShips.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largeIconToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallIconToolStripMenuItem;
        private System.Windows.Forms.StatusStrip stsStrpNumberOfShips;
        private System.Windows.Forms.ToolStripStatusLabel toolStrpNumberOfShips;
        private System.Windows.Forms.ListView lvwShips;
        private System.Windows.Forms.ImageList imageListLargeIcons;
        private System.Windows.Forms.ImageList imageListSmallIcons;
        private System.Windows.Forms.ToolStripStatusLabel lblShipCount;
    }
}

