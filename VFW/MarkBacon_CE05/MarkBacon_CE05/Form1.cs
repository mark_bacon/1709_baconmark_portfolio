﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE05
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE05
     */

      /* Structure / Efficiency: Excellent 10/10
        Layout: Excellent 10/10
        User Input Dialog: Excellent 25/25
        ListView/Main Window: Good 18/25, The default view is not checked on the menu when the application first runs.The current view is never checked when the view changes.
        Events: Fair 12/30, A custom EventArgs class is not used to pass data in an event.
    */

    public partial class Form1 : Form
    {
        public List<Ship> Ships { get; set; } = new List<Ship>();
        
        public Form1()
        {
            InitializeComponent();
        }
        
       /* public Form1(List<Ship> ships)
        {
            InitializeComponent();
            this.Ships = ships;
            if (formListView == null)
            {
                formListView = new Form1(this.Ships);


            }
            formListView.Show();
        }
        */
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //instantiate new form
            ShipDetails ShipDetailsForm = new ShipDetails();

            //subscribe the delegate to the method
            ShipDetailsForm.ShipAdded += ShipDetailsForm_ShipAdded;


            ShipDetailsForm.ShowDialog();
        }

        private void ShipDetailsForm_ShipAdded(object sender, ShipEventArgs shipEventArgs)
        {
            //this allows us to add a ship 
            Ships.Add(shipEventArgs.Ship);

            //update our count of ships
            UpdateShipCount();

            //update listview window if opened
            UpdateShipListView();
            
            
        }

        private void UpdateShipCount()
        {
            lblShipCount.Text = Ships.Count.ToString();
        }

        private void UpdateShipListView()
        {
        

          lvwShips.Clear();

            foreach (var ship in Ships)
            {
                ListViewItem lvi = new ListViewItem();

                //ListViewItem with several properties with can set
                lvi.Text = ship.ToString();
                lvi.ImageIndex = ship.ShipImageIndex;

                //allows us to reference the object so we can access everything
                lvi.Tag = ship;

                lvwShips.Items.Add(lvi);
            }
            
        }

        private void lvwShips_DoubleClick(object sender, EventArgs e)
        {
            if (lvwShips.SelectedItems.Count <= 0)
                return;

            var selectedShip = lvwShips.SelectedItems[0].Tag as Ship;

            var shipUpdateForm = new ShipDetails(selectedShip);





            //subscribe to the event handler method with the custom
            //event handler and custom event args
            shipUpdateForm.ShipAdded += ShipDetailsForm_ShipAdded;
            shipUpdateForm.ShipUpdated += ShipUpdateForm_ShipUpdated;

            shipUpdateForm.ShowDialog();
        }

        private void ShipUpdateForm_ShipUpdated(object sender, ShipEventArgs e)
        {
            UpdateShipListView();
        }

        private void largeIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvwShips.View = View.LargeIcon;
        }

        private void smallIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvwShips.View = View.SmallIcon;
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ships.Clear();

            UpdateShipCount();

            UpdateShipListView();

            
        }
    }
}
