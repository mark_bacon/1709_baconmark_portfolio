﻿using System;

namespace MarkBacon_CE05
{/*
     MarkBacon
     Visual Frameworks
     May 2018
     CE05
     */
    public class ShipEventArgs : EventArgs
    {
        public Ship Ship { get; set; }
        public ShipEventArgs(Ship ship)
        {
            Ship = ship;
        }
    }
}
