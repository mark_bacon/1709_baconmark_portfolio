﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE05
{/*
     MarkBacon
     Visual Frameworks
     May 2018
     CE05
     */
    public class Ship
    {
        string name;
        decimal crewSize;
        bool activeDuty;
        int shipImageIndex;

        public string Name { get => name; set => name = value; }
        public decimal CrewSize { get => crewSize; set => crewSize = value; }
        public bool ActiveDuty { get => activeDuty; set => activeDuty = value; }
        public int ShipImageIndex { get => shipImageIndex; set => shipImageIndex = value; }

        public override string ToString()
        {
            return Name;
        }
    }
}
