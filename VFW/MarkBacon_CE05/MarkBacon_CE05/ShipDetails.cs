﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE05
{/*
     MarkBacon
     Visual Frameworks
     May 2018
     CE05
     */
    public partial class ShipDetails : Form
    {

        //This form opens in two modes

             //add new ship mode
        public ShipDetails()
        {
            InitializeComponent();
        
            this.Ship = new Ship();
            groupBox1.Text = "Add New Ship";
            btnApply.Visible = false;
        }

            //update ship mode
        public ShipDetails(Ship existingShip)
        {
            InitializeComponent();
            groupBox1.Text = "Update Ship";
            this.Ship = existingShip;
        }

        private Ship _ship;
        public Ship Ship
        {
            get { return _ship; }
            set
            {
                _ship = value;
                UpdateFields();
            }
        }
      

        

      
        private void UpdateFields()
        {
            
            txtShipName.Text = this.Ship.Name;
            numCrewSize.Value = this.Ship.CrewSize;
            chkIsActive.Checked = this.Ship.ActiveDuty;

            switch (this.Ship.ShipImageIndex)
            {
                case 0:
                    rdoBtnCruiser.Checked = true;
                    break;
                case 1:
                    rdoBtnDestroyer.Checked = true;
                    break;
                case 2:
                    rdoBtnFreighter.Checked = true;
                    break;
            }

        }

        //delegate for adding ship info to the listview
        public event EventHandler<ShipEventArgs> ShipAdded;

        private void OnShipAdded()
        {
            if (ShipAdded != null)
                ShipAdded(this, new ShipEventArgs(this.Ship));
        }

        public event EventHandler<ShipEventArgs> ShipUpdated;

        private void OnShipUpdated()
        {
            if (ShipUpdated != null)
                ShipUpdated(this, new ShipEventArgs(this.Ship));
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            
            string shipName = txtShipName.Text;
            decimal crewSize = numCrewSize.Value;
            bool activeDuty = chkIsActive.Checked;
            //this will get the index number of the ship  to select the pic icon
            
            int index = 0;

            if (rdoBtnDestroyer.Checked)
            {
                index = 1;
            }
            else if (rdoBtnFreighter.Checked)
            {
                index = 2;
            }

            this.Ship = new Ship
            {
                ShipImageIndex = index,
                Name = shipName,
                CrewSize = crewSize,
                ActiveDuty = activeDuty
            };



            //  ChangeShipDetails(this, new ChangeShipDetailsArgs(shipName, crewSize, activeDuty, index));

            //add to list
            OnShipAdded();

            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            string shipName = txtShipName.Text;
            decimal crewSize = numCrewSize.Value;
            bool activeDuty = chkIsActive.Checked;
            //this will get the index number of the ship  to select the pic icon

            int index = 0;

            if (rdoBtnDestroyer.Checked)
            {
                index = 1;
            }
            else if (rdoBtnFreighter.Checked)
            {
                index = 2;
            }
            this.Ship.ShipImageIndex = index;

            this.Ship.Name = shipName;
            this.Ship.CrewSize = crewSize;
            this.Ship.ActiveDuty = activeDuty;

            //  ChangeShipDetails(this, new ChangeShipDetailsArgs(shipName, crewSize, activeDuty, index));

            //add to list
            OnShipUpdated();

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ShipDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            ///e.Cancel = true;
        }
    }
}
