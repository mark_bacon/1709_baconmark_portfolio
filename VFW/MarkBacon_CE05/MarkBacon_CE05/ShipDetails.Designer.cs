﻿namespace MarkBacon_CE05
{
    partial class ShipDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkIsActive = new System.Windows.Forms.CheckBox();
            this.numCrewSize = new System.Windows.Forms.NumericUpDown();
            this.txtShipName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdoBtnFreighter = new System.Windows.Forms.RadioButton();
            this.rdoBtnDestroyer = new System.Windows.Forms.RadioButton();
            this.rdoBtnCruiser = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCrewSize)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.chkIsActive);
            this.groupBox1.Controls.Add(this.numCrewSize);
            this.groupBox1.Controls.Add(this.txtShipName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 288);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ship Info";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Crew size:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name:";
            // 
            // chkIsActive
            // 
            this.chkIsActive.AutoSize = true;
            this.chkIsActive.Location = new System.Drawing.Point(306, 221);
            this.chkIsActive.Name = "chkIsActive";
            this.chkIsActive.Size = new System.Drawing.Size(153, 29);
            this.chkIsActive.TabIndex = 5;
            this.chkIsActive.Text = "Active Duty";
            this.chkIsActive.UseVisualStyleBackColor = true;
            // 
            // numCrewSize
            // 
            this.numCrewSize.Location = new System.Drawing.Point(306, 147);
            this.numCrewSize.Name = "numCrewSize";
            this.numCrewSize.Size = new System.Drawing.Size(150, 31);
            this.numCrewSize.TabIndex = 4;
            // 
            // txtShipName
            // 
            this.txtShipName.Location = new System.Drawing.Point(156, 75);
            this.txtShipName.Name = "txtShipName";
            this.txtShipName.Size = new System.Drawing.Size(301, 31);
            this.txtShipName.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdoBtnFreighter);
            this.groupBox2.Controls.Add(this.rdoBtnDestroyer);
            this.groupBox2.Controls.Add(this.rdoBtnCruiser);
            this.groupBox2.Location = new System.Drawing.Point(12, 306);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(462, 154);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select a ship";
            // 
            // rdoBtnFreighter
            // 
            this.rdoBtnFreighter.AutoSize = true;
            this.rdoBtnFreighter.Location = new System.Drawing.Point(266, 69);
            this.rdoBtnFreighter.Name = "rdoBtnFreighter";
            this.rdoBtnFreighter.Size = new System.Drawing.Size(129, 29);
            this.rdoBtnFreighter.TabIndex = 2;
            this.rdoBtnFreighter.TabStop = true;
            this.rdoBtnFreighter.Text = "Freighter";
            this.rdoBtnFreighter.UseVisualStyleBackColor = true;
            // 
            // rdoBtnDestroyer
            // 
            this.rdoBtnDestroyer.AutoSize = true;
            this.rdoBtnDestroyer.Location = new System.Drawing.Point(124, 69);
            this.rdoBtnDestroyer.Name = "rdoBtnDestroyer";
            this.rdoBtnDestroyer.Size = new System.Drawing.Size(136, 29);
            this.rdoBtnDestroyer.TabIndex = 1;
            this.rdoBtnDestroyer.TabStop = true;
            this.rdoBtnDestroyer.Text = "Destroyer";
            this.rdoBtnDestroyer.UseVisualStyleBackColor = true;
            // 
            // rdoBtnCruiser
            // 
            this.rdoBtnCruiser.AutoSize = true;
            this.rdoBtnCruiser.Location = new System.Drawing.Point(6, 69);
            this.rdoBtnCruiser.Name = "rdoBtnCruiser";
            this.rdoBtnCruiser.Size = new System.Drawing.Size(112, 29);
            this.rdoBtnCruiser.TabIndex = 0;
            this.rdoBtnCruiser.TabStop = true;
            this.rdoBtnCruiser.Text = "Crusier";
            this.rdoBtnCruiser.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 477);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(150, 89);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(168, 477);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 89);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(324, 477);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(150, 89);
            this.btnApply.TabIndex = 2;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // ShipDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 602);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ShipDetails";
            this.Text = "ShipDetails";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ShipDetails_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCrewSize)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkIsActive;
        private System.Windows.Forms.NumericUpDown numCrewSize;
        private System.Windows.Forms.TextBox txtShipName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdoBtnFreighter;
        private System.Windows.Forms.RadioButton rdoBtnDestroyer;
        private System.Windows.Forms.RadioButton rdoBtnCruiser;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
    }
}