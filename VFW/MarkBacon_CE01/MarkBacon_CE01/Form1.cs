﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MarkBacon_CE01
{
    /*
     Mark Bacon
     Visual Frameworks
     May 2018
     CE01
     */
    //adjusted the form so that the groupbox line was not lost on the left edge
    //also moved it down a little so that it was not covered by the menu strip. 

    // feedback from 5/7/2018
    // I didn't catch this last time, but you're saving the values with spaces 
    // before and after the |, which means there is an extraneous space after 
    // the text in the firstName text box, and a space before and after the text 
    // in the lastName text box.You should use .Trim to ensure any extraneous spaces are removed.


    public partial class Form1 : Form
    {

        OpenFileDialog openfile = new OpenFileDialog();
        string line = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void exitApplication(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            genderComboBox.SelectedItem = "";
            subscribeCheckbox.Checked = false;
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = new StreamReader(openFileDialog1.FileName);

                line = reader.ReadLine();
                List<string> Values = new List<string>();
                
                    
                   
                    Values = line.Split('|').ToList();
                    firstNameTextBox.Text = Values[0];
                    lastNameTextBox.Text = Values[1];
                    genderComboBox.SelectedText = Values[2];
                    subscribeCheckbox.Checked = Convert.ToBoolean(Values[3]);
                    
               
                reader.Close();
                
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var path = saveFileDialog1.FileName;
                StreamWriter writer = new StreamWriter(path);
                
                    writer.WriteLine("{0} | {1} | {2} | {3}", firstNameTextBox.Text, lastNameTextBox.Text, genderComboBox.SelectedItem, subscribeCheckbox.Checked);
                writer.Close();
            }
        }

      //  public List<string> Values { get; set; }

      //  public  Record(string line)
      //  {
      //      Values = line.Split('|').ToList();
        }
    }

        

    

