﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE02
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE02
     */

    class Record
    {
       
        public List<string> Values { get; set; }

        public Record(string line)
        {
            Values = line.Split(' ').ToList();

        }
        
    }
}
