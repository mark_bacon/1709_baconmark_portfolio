﻿namespace MarkBacon_CE02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.resetButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.gradeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.addClassTwoButton = new System.Windows.Forms.Button();
            this.addClassOneButton = new System.Windows.Forms.Button();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextbox = new System.Windows.Forms.TextBox();
            this.Class1groupbox = new System.Windows.Forms.GroupBox();
            this.removeClassOneButton = new System.Windows.Forms.Button();
            this.classOneList = new System.Windows.Forms.ListBox();
            this.Class2GroupBox = new System.Windows.Forms.GroupBox();
            this.removeClassTwoButton = new System.Windows.Forms.Button();
            this.classTwoList = new System.Windows.Forms.ListBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayStudentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnMoveRightArrow = new System.Windows.Forms.Button();
            this.btnMoveLeftArrow = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.numAge = new System.Windows.Forms.NumericUpDown();
            this.chkIsGraduate = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.Class1groupbox.SuspendLayout();
            this.Class2GroupBox.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkIsGraduate);
            this.groupBox1.Controls.Add(this.numAge);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.resetButton);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.gradeComboBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.genderComboBox);
            this.groupBox1.Controls.Add(this.addClassTwoButton);
            this.groupBox1.Controls.Add(this.addClassOneButton);
            this.groupBox1.Controls.Add(this.lastNameLabel);
            this.groupBox1.Controls.Add(this.firstNameLabel);
            this.groupBox1.Controls.Add(this.lastNameTextBox);
            this.groupBox1.Controls.Add(this.firstNameTextbox);
            this.groupBox1.Location = new System.Drawing.Point(12, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(387, 621);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Student Info";
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(90, 542);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(202, 57);
            this.resetButton.TabIndex = 7;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 195);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 25);
            this.label2.TabIndex = 12;
            this.label2.Text = "Grade";
            // 
            // gradeComboBox
            // 
            this.gradeComboBox.FormattingEnabled = true;
            this.gradeComboBox.Items.AddRange(new object[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "F"});
            this.gradeComboBox.Location = new System.Drawing.Point(205, 192);
            this.gradeComboBox.Name = "gradeComboBox";
            this.gradeComboBox.Size = new System.Drawing.Size(158, 33);
            this.gradeComboBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "Gender";
            // 
            // genderComboBox
            // 
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Items.AddRange(new object[] {
            "",
            "Male",
            "Female"});
            this.genderComboBox.Location = new System.Drawing.Point(205, 144);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(158, 33);
            this.genderComboBox.TabIndex = 2;
            // 
            // addClassTwoButton
            // 
            this.addClassTwoButton.Location = new System.Drawing.Point(205, 373);
            this.addClassTwoButton.Name = "addClassTwoButton";
            this.addClassTwoButton.Size = new System.Drawing.Size(169, 62);
            this.addClassTwoButton.TabIndex = 6;
            this.addClassTwoButton.Text = "Add to Class 2";
            this.addClassTwoButton.UseVisualStyleBackColor = true;
            this.addClassTwoButton.Click += new System.EventHandler(this.addClassTwoButton_Click);
            // 
            // addClassOneButton
            // 
            this.addClassOneButton.Location = new System.Drawing.Point(6, 373);
            this.addClassOneButton.Name = "addClassOneButton";
            this.addClassOneButton.Size = new System.Drawing.Size(169, 62);
            this.addClassOneButton.TabIndex = 5;
            this.addClassOneButton.Text = "Add to Class 1";
            this.addClassOneButton.UseVisualStyleBackColor = true;
            this.addClassOneButton.Click += new System.EventHandler(this.addClassOneButton_Click);
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(41, 99);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(115, 25);
            this.lastNameLabel.TabIndex = 3;
            this.lastNameLabel.Text = "Last Name";
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(40, 53);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(116, 25);
            this.firstNameLabel.TabIndex = 2;
            this.firstNameLabel.Text = "First Name";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(205, 96);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(158, 31);
            this.lastNameTextBox.TabIndex = 1;
            // 
            // firstNameTextbox
            // 
            this.firstNameTextbox.Location = new System.Drawing.Point(205, 50);
            this.firstNameTextbox.Name = "firstNameTextbox";
            this.firstNameTextbox.Size = new System.Drawing.Size(158, 31);
            this.firstNameTextbox.TabIndex = 0;
            // 
            // Class1groupbox
            // 
            this.Class1groupbox.Controls.Add(this.removeClassOneButton);
            this.Class1groupbox.Controls.Add(this.classOneList);
            this.Class1groupbox.Location = new System.Drawing.Point(415, 66);
            this.Class1groupbox.Name = "Class1groupbox";
            this.Class1groupbox.Size = new System.Drawing.Size(424, 621);
            this.Class1groupbox.TabIndex = 1;
            this.Class1groupbox.TabStop = false;
            this.Class1groupbox.Text = "Class 1";
            // 
            // removeClassOneButton
            // 
            this.removeClassOneButton.Location = new System.Drawing.Point(18, 373);
            this.removeClassOneButton.Name = "removeClassOneButton";
            this.removeClassOneButton.Size = new System.Drawing.Size(274, 62);
            this.removeClassOneButton.TabIndex = 9;
            this.removeClassOneButton.Text = "Remove from Class 1";
            this.removeClassOneButton.UseVisualStyleBackColor = true;
            this.removeClassOneButton.Click += new System.EventHandler(this.removeClassOneButton_Click);
            // 
            // classOneList
            // 
            this.classOneList.FormattingEnabled = true;
            this.classOneList.ItemHeight = 25;
            this.classOneList.Location = new System.Drawing.Point(18, 50);
            this.classOneList.Name = "classOneList";
            this.classOneList.Size = new System.Drawing.Size(354, 279);
            this.classOneList.TabIndex = 8;
            this.classOneList.SelectedIndexChanged += new System.EventHandler(this.classOneList_SelectedIndexChanged);
            // 
            // Class2GroupBox
            // 
            this.Class2GroupBox.Controls.Add(this.removeClassTwoButton);
            this.Class2GroupBox.Controls.Add(this.classTwoList);
            this.Class2GroupBox.Location = new System.Drawing.Point(944, 66);
            this.Class2GroupBox.Name = "Class2GroupBox";
            this.Class2GroupBox.Size = new System.Drawing.Size(424, 621);
            this.Class2GroupBox.TabIndex = 2;
            this.Class2GroupBox.TabStop = false;
            this.Class2GroupBox.Text = "Class 2";
            // 
            // removeClassTwoButton
            // 
            this.removeClassTwoButton.Location = new System.Drawing.Point(26, 373);
            this.removeClassTwoButton.Name = "removeClassTwoButton";
            this.removeClassTwoButton.Size = new System.Drawing.Size(274, 62);
            this.removeClassTwoButton.TabIndex = 13;
            this.removeClassTwoButton.Text = "Remove from Class 2";
            this.removeClassTwoButton.UseVisualStyleBackColor = true;
            this.removeClassTwoButton.Click += new System.EventHandler(this.removeClassTwoButton_Click);
            // 
            // classTwoList
            // 
            this.classTwoList.FormattingEnabled = true;
            this.classTwoList.ItemHeight = 25;
            this.classTwoList.Location = new System.Drawing.Point(26, 50);
            this.classTwoList.Name = "classTwoList";
            this.classTwoList.Size = new System.Drawing.Size(372, 279);
            this.classTwoList.TabIndex = 12;
            this.classTwoList.SelectedIndexChanged += new System.EventHandler(this.classTwoList_SelectedIndexChanged);
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.detailsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1380, 40);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // detailsToolStripMenuItem
            // 
            this.detailsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayStudentDetailsToolStripMenuItem});
            this.detailsToolStripMenuItem.Name = "detailsToolStripMenuItem";
            this.detailsToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.detailsToolStripMenuItem.Text = "Stats";
            // 
            // displayStudentDetailsToolStripMenuItem
            // 
            this.displayStudentDetailsToolStripMenuItem.Name = "displayStudentDetailsToolStripMenuItem";
            this.displayStudentDetailsToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.displayStudentDetailsToolStripMenuItem.Text = "Display";
            this.displayStudentDetailsToolStripMenuItem.Click += new System.EventHandler(this.displayStudentDetailsToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // btnMoveRightArrow
            // 
            this.btnMoveRightArrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightArrow.Location = new System.Drawing.Point(854, 176);
            this.btnMoveRightArrow.Name = "btnMoveRightArrow";
            this.btnMoveRightArrow.Size = new System.Drawing.Size(75, 67);
            this.btnMoveRightArrow.TabIndex = 10;
            this.btnMoveRightArrow.Text = ">";
            this.btnMoveRightArrow.UseVisualStyleBackColor = true;
            this.btnMoveRightArrow.Click += new System.EventHandler(this.btnMoveRightArrow_Click);
            // 
            // btnMoveLeftArrow
            // 
            this.btnMoveLeftArrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftArrow.Location = new System.Drawing.Point(854, 272);
            this.btnMoveLeftArrow.Name = "btnMoveLeftArrow";
            this.btnMoveLeftArrow.Size = new System.Drawing.Size(75, 67);
            this.btnMoveLeftArrow.TabIndex = 11;
            this.btnMoveLeftArrow.Text = "<";
            this.btnMoveLeftArrow.UseVisualStyleBackColor = true;
            this.btnMoveLeftArrow.Click += new System.EventHandler(this.btnMoveLeftArrow_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 25);
            this.label3.TabIndex = 15;
            this.label3.Text = "Age";
            // 
            // numAge
            // 
            this.numAge.Location = new System.Drawing.Point(205, 242);
            this.numAge.Name = "numAge";
            this.numAge.Size = new System.Drawing.Size(157, 31);
            this.numAge.TabIndex = 4;
            // 
            // chkIsGraduate
            // 
            this.chkIsGraduate.AutoSize = true;
            this.chkIsGraduate.Location = new System.Drawing.Point(205, 288);
            this.chkIsGraduate.Name = "chkIsGraduate";
            this.chkIsGraduate.Size = new System.Drawing.Size(133, 29);
            this.chkIsGraduate.TabIndex = 17;
            this.chkIsGraduate.Text = "Graduate";
            this.chkIsGraduate.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1380, 699);
            this.Controls.Add(this.btnMoveLeftArrow);
            this.Controls.Add(this.btnMoveRightArrow);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.Class2GroupBox);
            this.Controls.Add(this.Class1groupbox);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "School Info";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Class1groupbox.ResumeLayout(false);
            this.Class2GroupBox.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox firstNameTextbox;
        private System.Windows.Forms.GroupBox Class1groupbox;
        private System.Windows.Forms.Button removeClassOneButton;
        private System.Windows.Forms.ListBox classOneList;
        private System.Windows.Forms.GroupBox Class2GroupBox;
        private System.Windows.Forms.Button removeClassTwoButton;
        private System.Windows.Forms.ListBox classTwoList;
        private System.Windows.Forms.Button addClassOneButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox gradeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.Button addClassTwoButton;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayStudentDetailsToolStripMenuItem;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnMoveRightArrow;
        private System.Windows.Forms.Button btnMoveLeftArrow;
        private System.Windows.Forms.CheckBox chkIsGraduate;
        private System.Windows.Forms.NumericUpDown numAge;
        private System.Windows.Forms.Label label3;
    }
}

