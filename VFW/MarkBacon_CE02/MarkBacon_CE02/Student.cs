﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE02
{

    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE02
     */

    public class Student
    {
        string firstName;
        string lastName;
        string gender;
        string grade;
        decimal age;
        bool graduate;

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
            }

        }
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }
        public string Gender
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
            }
        }
        public string Grade
        {
            get
            {
                return grade;
            }
            set
            {
                grade = value;
            }
        }

        public decimal Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
            }
        }
        public bool Graduate
        {
            get
            {
                return graduate;
            }
            set
            {
                graduate = value;
            }
        }
        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
