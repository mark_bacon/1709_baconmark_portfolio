﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MarkBacon_CE02
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE02
     */
    //added name/class assigment details to each page of code.
    //adjusted the class 1 and class 2 groupboxes to make room for the move arrow buttons. 
    //added move arrow buttons and increased font size so they would be more visible to user
    //added functionality to the right and left move arrows. 
    //removed checkmark from the display menu command
    //added two additional unique controls. Age is a numericUpDown control and Graduate is a checkbox.
    //updated the streamwriter and streamreader to include the Age and Graduate inputs

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string line = null;
        
        public Student Info
        {
            get
            {
                Student c = new Student();
                c.FirstName = firstNameTextbox.Text;
                c.LastName = lastNameTextBox.Text;
                c.Gender = genderComboBox.Text;
                c.Grade = gradeComboBox.Text;
                c.Age = numAge.Value;
                c.Graduate = chkIsGraduate.Checked;

                return c;
            }
            set
            {
                firstNameTextbox.Text = value.FirstName;
                lastNameTextBox.Text = value.LastName;
                genderComboBox.Text = value.Gender;
                gradeComboBox.Text = value.Grade;
                numAge.Value = value.Age;
                chkIsGraduate.Checked = value.Graduate;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void addClassOneButton_Click(object sender, EventArgs e)
        {
            

            classOneList.Items.Add(Info);

            Info = new Student();
        }

        private void addClassTwoButton_Click(object sender, EventArgs e)
        {
            

            classTwoList.Items.Add(Info);

            Info = new Student();
        }

        private void classOneList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checks to make sure the selection is one of the items in the list
            if (classOneList.SelectedIndex >= 0 && classOneList.SelectedIndex < classOneList.Items.Count)
            {
                //adds it to the list as a new item
                Student c = classOneList.Items[classOneList.SelectedIndex] as Student;

                Info = c;
            }

        }

        private void classTwoList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //checks to make sure the selection is one of the items in the list
            if (classTwoList.SelectedIndex >= 0 && classTwoList.SelectedIndex < classTwoList.Items.Count)
            {
                //adds to the list as new item.
                Student c = classTwoList.Items[classTwoList.SelectedIndex] as Student;

                Info = c;
            }
        }

        private void displayStudentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Student_Details form = new Student_Details();

            //counts the number of students in class 1
            form.class1CountDisplay = classOneList.Items.Count.ToString();

            //counts the number of students in class 2
            form.class2CountDisplay = classTwoList.Items.Count.ToString();

            //shows the form
            form.ShowDialog();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            //resets all of the controls 
            firstNameTextbox.Text = "";
            lastNameTextBox.Text = "";
            genderComboBox.SelectedItem = "";
            gradeComboBox.Text = "";
            numAge.Value = 0;
            chkIsGraduate.Checked = false;
        }

        private void removeClassOneButton_Click(object sender, EventArgs e)
        {

            if (classOneList.SelectedItem != null)
            {
                classOneList.Items.RemoveAt(classOneList.SelectedIndex);
            }
        }

        private void removeClassTwoButton_Click(object sender, EventArgs e)
        {
            if (classTwoList.SelectedItem != null)
            {
                classTwoList.Items.RemoveAt(classTwoList.SelectedIndex);
            } 
        }

        private string _separator = "~|~";



        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var path = saveFileDialog1.FileName;
                StreamWriter writer = new StreamWriter(path);

                
                writer.WriteLine("Class One List");
                foreach (Student student in classOneList.Items)
                {
                    var studentString = convertStudentToString(student);

                    writer.WriteLine(studentString);

                }

                writer.WriteLine("Class Two List");
                foreach (Student student in classTwoList.Items)
                {
                  var studentString =  convertStudentToString(student);

                    writer.WriteLine(studentString);
                }
                writer.Close();
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = new StreamReader(openFileDialog1.FileName);

                ListBox targetList = null;

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("Class One List"))
                    {
                        targetList = classOneList;
                        continue;
                    }
                    else if(line.StartsWith("Class Two List"))
                    {
                        targetList = classTwoList;
                        continue;
                    }                   
                    

                    targetList.Items.Add(convertStringToStudent(line));

                }



                reader.Close();

            }
        }

        public Student convertStringToStudent (string line)
        {
            var Values = line.Split(new string[] { _separator }, StringSplitOptions.None);
            return new Student
            {
                FirstName = Values[0],
                LastName = Values[1],
                Gender = Values[2],
                Grade = Values[3],
               //need to convert string to decimal
                 Age = Convert.ToDecimal(Values[4]),
               //need to convert string to bool
                Graduate = Convert.ToBoolean(Values[5])
            
            };
        }

        public string convertStudentToString(Student student)
        {
            var studentString = "";
            studentString += student.FirstName.Replace(_separator, "_") + _separator;
            studentString += student.LastName.Replace(_separator, "_") + _separator;
            studentString += student.Gender.Replace(_separator, "_") + _separator;
            studentString += student.Grade.Replace(_separator, "_") + _separator;
                //need to convert string to decimal
            studentString += student.Age.ToString().Replace(_separator, "_") +_separator ;
                //need to convert string to bool
            studentString += student.Graduate.ToString().Replace(_separator, "_");
            return studentString;
        }

        private void btnMoveRightArrow_Click(object sender, EventArgs e)
        {
            //adds the list entry to list 2 and removes from list 1

            if (classOneList.SelectedItem != null)
            {
                classTwoList.Items.Add(Info);

                Info = new Student();

                classOneList.Items.RemoveAt(classOneList.SelectedIndex);
            }
        }

        private void btnMoveLeftArrow_Click(object sender, EventArgs e)
        {
            //adds the list entry to list 1 and removes from list 2

            if (classTwoList.SelectedItem != null)
            {
                classOneList.Items.Add(Info);

                Info = new Student();

                classTwoList.Items.RemoveAt(classTwoList.SelectedIndex);
            }
        }
    }


}
