﻿namespace MarkBacon_CE02
{
    partial class Student_Details
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentClassOneCountTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.studentClassTwoCountTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // studentClassOneCountTextBox
            // 
            this.studentClassOneCountTextBox.Location = new System.Drawing.Point(216, 61);
            this.studentClassOneCountTextBox.Name = "studentClassOneCountTextBox";
            this.studentClassOneCountTextBox.ReadOnly = true;
            this.studentClassOneCountTextBox.Size = new System.Drawing.Size(190, 31);
            this.studentClassOneCountTextBox.TabIndex = 0;
            this.studentClassOneCountTextBox.Text = "0";
            this.studentClassOneCountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Class 1 Count";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Class 2 Count";
            // 
            // studentClassTwoCountTextBox
            // 
            this.studentClassTwoCountTextBox.Location = new System.Drawing.Point(216, 107);
            this.studentClassTwoCountTextBox.Name = "studentClassTwoCountTextBox";
            this.studentClassTwoCountTextBox.ReadOnly = true;
            this.studentClassTwoCountTextBox.Size = new System.Drawing.Size(190, 31);
            this.studentClassTwoCountTextBox.TabIndex = 3;
            this.studentClassTwoCountTextBox.Text = "0";
            this.studentClassTwoCountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.studentClassTwoCountTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.studentClassOneCountTextBox);
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 159);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Class Details";
            // 
            // Student_Details
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 183);
            this.Controls.Add(this.groupBox1);
            this.Name = "Student_Details";
            this.Text = "Student_Details";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox studentClassOneCountTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox studentClassTwoCountTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}