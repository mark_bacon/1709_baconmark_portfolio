﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE02
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE02
     */

    public partial class Student_Details : Form
    {


        public Student_Details()
        {
            InitializeComponent();
        }

        public string class1CountDisplay
        {
            set
            {
                studentClassOneCountTextBox.Text = value;
            }
        }
        public string class2CountDisplay
        {
            set
            {
                studentClassTwoCountTextBox.Text = value;
            }
        }
    }
}
