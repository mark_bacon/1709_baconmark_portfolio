﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE04
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE04
     */
    public partial class StudentListView : Form
    {
        //create event for clearing all the data on the student list view page.
        public event EventHandler ClearAll;

        //property to hold the data of the selected item in the ListView
        public ListViewItem SelectedCharacter
        {
            get
            {
                //verify that an item is selected by getting 
                // a count of the selected items
                if (lvwStudents.SelectedItems.Count > 0)
                {
                    //select items is a collection, so we want to 
                    //select the first item in that collection
                    return lvwStudents.SelectedItems[0];
                }
                return null;
            }
        }

        private List<Student> _students;
        public List<Student> Students
        {
            get { return _students; }
            set
            {
                _students = value;
                UpdateStudentListView();
            }
        }

        public StudentListView(List<Student> students)
        {
            InitializeComponent();

            this.Students = students;
        }

        public void UpdateStudentListView()
        {
            lvwStudents.Clear();

            foreach (var student in Students)
            {
                ListViewItem lvi = new ListViewItem();

                //ListViewItem with several properties we can set
                lvi.Text = student.ToString();
                lvi.ImageIndex = student.GenderIndex;

                //allows us to reference the object so we can access everything
                lvi.Tag = student;

                lvwStudents.Items.Add(lvi);
            }
        }



        private void lvwStudents_DoubleClick(object sender, EventArgs e)
        {
            var selectedStudent = lvwStudents.SelectedItems[0].Tag as Student;

            var studentInputWindows = new UserInputForm(selectedStudent);

            studentInputWindows.StudentInput += StudentInputWindows_StudentInput;

            //need to add the UpdateCounts()
            

            studentInputWindows.Show();

            
        }

        private void StudentInputWindows_StudentInput(object sender, Student e)
        {
            UpdateStudentListView();
        }

        public void toolStripBtnClear_Click(object sender, EventArgs e)
        {

            


            if (ClearAll != null)
            {
                ClearAll(this, new EventArgs());
            }
        }
        
    }

}
