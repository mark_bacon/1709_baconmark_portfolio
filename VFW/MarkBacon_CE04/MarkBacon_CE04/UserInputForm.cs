﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE04
{

    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE04
     */
    public partial class UserInputForm : Form
    {
        // The form can open in 2 modes
        // Insert mode
        public UserInputForm()
        {
            InitializeComponent();
            this.Student = new Student();
        }

        // Update mode
        public UserInputForm(Student existingStudent)
        {
            InitializeComponent();
            btnAddStudent.Text = "Update Student";
            groupBox1.Text = "Update Student";
            this.Student = existingStudent;            
        }

        private Student _student;
        public Student Student
        {
            get { return _student; }
            set
            {
                _student = value;
                UpdateFields();
            }
            //get
            //{
            //    Student c = new Student();
            //    c.Name = txtName.Text;
            //    c.Age = numAge.Value;
            //    c.Gender = cmbGender.Text;
            //    c.IsStudent = chkStudent.Checked;
            //    //this will get the index number of male or female to setup pic icon
            //    c.GenderIndex = cmbGender.SelectedIndex;
            //    return c;
            //}
            //set
            //{
            //    numAge.Value = value.Age;
            //    txtName.Text = value.Name;
            //    cmbGender.Text = value.Gender;
            //    chkStudent.Checked = value.IsStudent;

            //}

        }

        private void UpdateFields()
        {
            numAge.Value = this.Student.Age;
            txtName.Text = this.Student.Name;
            cmbGender.Text = this.Student.Gender;
            chkStudent.Checked = this.Student.IsStudent;
        }


        
       
        



        //delegate for adding student info to the listview
        public event EventHandler<Student> StudentInput;

        private void OnStudentInput()
        {
            if (StudentInput != null)
                StudentInput(this, this.Student);
        }


        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            this.Student.Name = txtName.Text;
            this.Student.Age = numAge.Value;
            this.Student.Gender = cmbGender.Text;
            this.Student.IsStudent = chkStudent.Checked;
            //this will get the index number of male or female to setup pic icon
            this.Student.GenderIndex = cmbGender.SelectedIndex;

            // trigger the event
            OnStudentInput();

            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            numAge.Value = 0;
            cmbGender.Text = "";
            chkStudent.Checked = false;
        }
    }
}
