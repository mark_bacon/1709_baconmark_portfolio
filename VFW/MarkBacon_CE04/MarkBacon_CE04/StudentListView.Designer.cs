﻿namespace MarkBacon_CE04
{
    partial class StudentListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentListView));
            this.lvwStudents = new System.Windows.Forms.ListView();
            this.genderImages = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripBtnClear = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvwStudents
            // 
            this.lvwStudents.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwStudents.LargeImageList = this.genderImages;
            this.lvwStudents.Location = new System.Drawing.Point(0, 39);
            this.lvwStudents.Name = "lvwStudents";
            this.lvwStudents.Size = new System.Drawing.Size(450, 561);
            this.lvwStudents.SmallImageList = this.genderImages;
            this.lvwStudents.TabIndex = 0;
            this.lvwStudents.UseCompatibleStateImageBehavior = false;
            this.lvwStudents.View = System.Windows.Forms.View.SmallIcon;
            this.lvwStudents.DoubleClick += new System.EventHandler(this.lvwStudents_DoubleClick);
            // 
            // genderImages
            // 
            this.genderImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("genderImages.ImageStream")));
            this.genderImages.TransparentColor = System.Drawing.Color.Transparent;
            this.genderImages.Images.SetKeyName(0, "male student");
            this.genderImages.Images.SetKeyName(1, "female student");
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnClear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(450, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripBtnClear
            // 
            this.toolStripBtnClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnClear.Image")));
            this.toolStripBtnClear.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripBtnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnClear.Name = "toolStripBtnClear";
            this.toolStripBtnClear.Size = new System.Drawing.Size(89, 36);
            this.toolStripBtnClear.Text = "Clear";
            this.toolStripBtnClear.Click += new System.EventHandler(this.toolStripBtnClear_Click);
            // 
            // StudentListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 600);
            this.Controls.Add(this.lvwStudents);
            this.Controls.Add(this.toolStrip1);
            this.Location = new System.Drawing.Point(500, 0);
            this.Name = "StudentListView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "StudentListView";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvwStudents;
        private System.Windows.Forms.ImageList genderImages;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripBtnClear;
    }
}