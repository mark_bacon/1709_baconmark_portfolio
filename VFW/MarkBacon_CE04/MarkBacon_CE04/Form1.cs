﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE04
{

    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE04
     */

      /*  Structure / Efficiency: Excellent 10/10
           Layout: Good 7/10, The checkmark on the Display menu command is a little off.You need to tweak the ImageScaling property for it to appear correctly.
           User Input Window: Fair 10/25, The User Input Form should not update a student if it is opened by double-clicking a ListView item; it should have the same functionality to add a student but be populated by the data from the selected ListViewItem.Missing the required toolStrip and button for adding items,
           Main Window: Good 18/25, Missing the required File menu with Exit command. Opening a UserInputWindow by double-clicking a ListViewItem does not update the number of Windows listed on the Main form.
           ListView Form: Excellent 30/30
    */

    public partial class Form1 : Form
    {

        public List<Student> Students { get; set; } = new List<Student>();
        public List<UserInputForm> Inputs { get; set; } = new List<UserInputForm>();


        //create global variable
        StudentListView studentListView;

        public Form1()
        {
            InitializeComponent();
            
        }

       
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnAddNewUser_Click(object sender, EventArgs e)
        {
            //instantiate the new form
            UserInputForm userInputForm = new UserInputForm();
           

            //need a way to count the open windows
            Inputs.Add(userInputForm);

            UpdateCounts();

            userInputForm.FormClosed += UserinputForm_FormClosed;
            userInputForm.StudentInput += UserInputForm_StudentInput;
            


            userInputForm.Show();
        }

        private void UserInputForm_StudentInput(object sender, Student student)
        {
            // This means that a user was input
            Students.Add(student);

            UpdateCounts();

            // Update listview window if opened
            if (studentListView != null)
                studentListView.UpdateStudentListView();

        }

        private void UserinputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Inputs.Remove(sender as UserInputForm);
            UpdateCounts();
        }

            public void UpdateCounts()
            {
                txtNumberOfWindows.Text = Inputs.Count.ToString();
                txtStudentCount.Text = Students.Count.ToString();
            }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //instantiate the new list view form
            if (studentListView == null)
            {
                studentListView = new StudentListView(this.Students);
            
                // Alternative way to subscribe to an event using an anonymous function, so you dont dirty your code with
                // another private method
                studentListView.FormClosed += (senderObject, args) => studentListView = null;
                studentListView.ClearAll += clearToolStripMenuItem_Click;
            }
            studentListView.Show();            
        }

        

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Students.Clear();

            UpdateCounts();

            if (studentListView != null)
                studentListView.UpdateStudentListView();
        }
    }
}
