﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE04
{
    /*
     MarkBacon
     Visual Frameworks
     May 2018
     CE04
     */
    public class Student
    {
        string name;
        decimal age;
        string gender;
        bool isStudent;
        int genderIndex;

        public override string ToString()
        {
            return name + ": " + age;
        }

        public string Name { get => name; set => name = value; }
        public decimal Age { get => age; set => age = value; }
        public string Gender { get => gender; set => gender = value; }
        public bool IsStudent { get => isStudent; set => isStudent = value; }
        public int GenderIndex { get => genderIndex; set => genderIndex = value; }
    }

    
}
