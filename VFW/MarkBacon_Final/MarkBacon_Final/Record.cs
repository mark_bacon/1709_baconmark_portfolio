﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_Final
{
    /*Mark Bacon
     * VFW
     * May 2018
     * CE010
     */
    public class Record
    {
        
        
            int id;
            string dvdTitle;
            int year;
            string rating;
            decimal publicRating;

            public int Id { get => id; set => id = value; }
            public string DvdTitle { get => dvdTitle; set => dvdTitle = value; }
            public int Year { get => year; set => year = value; }
            public string Rating { get => rating; set => rating = value; }
            public decimal PublicRating { get => publicRating; set => publicRating = value; }
        
    }
}
