﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//directive to include MySQL
using MySql.Data.MySqlClient;
//directive to use File IO
using System.IO;


namespace MarkBacon_Final
{
    /*Mark Bacon
     * VFW
     * May 2018
     * CE010
     */

    public partial class Form1 : Form
    {
        //global connection variable
        MySqlConnection conn = new MySqlConnection();

        //make data table global so we can use it again. It is only populated in RetrieveData() method. 
        DataTable theData;

        

        public Form1()
        {
            InitializeComponent();
            //invoke the method to build the connection string
            string connString = BuildConnectionString();

            //invoke the method to create the connection
            Connect(connString);

            //get the data from the database
            RetrieveData();

            // Populate
            RefreshListView();

            // Update Interface
            UpdateInterface();

            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //method to build the connection string
        public string BuildConnectionString()
        {
            //variable to hold the IP address
            string serverIP = "";

            try
            {
                //open the text files using the stream reader
                using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
                {
                    //read the server data from the text file
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            //var builder = new MySqlConnectionStringBuilder();

            //builder.UserID = "dbsAdmin";
            //builder.Password = "password";
            //builder.Server = serverIP;
            //builder.Database = "exampleDatabase";
            //builder.Port = 8889;
            //this fixes the SSL bug so that the latest versions of MySQL directive can be installed. 
            //builder.SslMode = MySqlSslMode.None;

            //return builder.ToString();


            //return the entire connection string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;sslmode=None;port=8889";
        }

        //method to connect to mySql database
        public void Connect(string myConnString)
        {
            try
            {
                conn.ConnectionString = myConnString;
                // Test the connection by openning and closing it
                conn.Open();
                conn.Close();
                //DEBUG: show a message box
                //MessageBox.Show("Connected!");
            }
            catch (MySqlException e)
            {
                switch (e.Number)
                {
                    case 1042:
                        MessageBox.Show("Can't resolve the host address.\n" + myConnString);
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password.");
                        break;
                    default:
                        MessageBox.Show(e.ToString() + "\n" + myConnString);
                        break;
                }

            }
        }

        //method to retrieve data from example_1709 database
        public bool RetrieveData()
        {
            conn.Open();

            //create the SQL statement thats going to get our data
            string sql = "SELECT dvdId,DVD_Title, Year, Rating, publicRating FROM DVD LIMIT 25;";

            //create the mySQL data adapter
            var adr = new MySqlDataAdapter(sql, conn);

            //set the type for the select command
            adr.SelectCommand.CommandType = CommandType.Text;

            //now we can instantiate the table that will hold the record set
            // DataTable theData = new DataTable();

            //This method adds the rows from the data source to the DataTable using the MySQLDataAdapter
            theData = new DataTable();
            adr.Fill(theData);

            conn.Close();

            return true;
        }

        public void RefreshListView()
        {
            lvwDVDs.Clear();
            foreach (DataRow row in theData.Rows)
            {
                lvwDVDs.Items.Add(new ListViewItem(row["DVD_Title"].ToString(), 0));
            }
        }

        public void UpdateInterface()
        {
            if (lvwDVDs.SelectedItems.Count > 0)
            {
                txtDVDTitle.Tag = lvwDVDs.SelectedIndices[0]; // Store the selected index for future usage;

                txtDVDTitle.Text = theData.Rows[lvwDVDs.SelectedIndices[0]]["DVD_Title"].ToString();
                numYear.Text = theData.Rows[lvwDVDs.SelectedIndices[0]]["Year"].ToString();
                cmbRating.Text = theData.Rows[lvwDVDs.SelectedIndices[0]]["Rating"].ToString();
                numPublicRating.Text = theData.Rows[lvwDVDs.SelectedIndices[0]]["publicRating"].ToString();

                btnUpdate.Enabled = true;
            }
            else
            {
                btnUpdate.Enabled = false;
            }
        }

        private void lvwDVDs_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            

            UpdateInterface();
        }

        private void smallIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvwDVDs.View = View.SmallIcon;
            smallIconsToolStripMenuItem.Checked = true;
            largeIconsToolStripMenuItem.Checked = false;
        }

        private void largeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lvwDVDs.View = View.LargeIcon;
            smallIconsToolStripMenuItem.Checked = false;
            largeIconsToolStripMenuItem.Checked = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            var id = theData.Rows[(int)txtDVDTitle.Tag]["dvdId"];

            var sql = "UPDATE DVD SET DVD_Title = @DVD_Title, Year = @Year, Rating = @Rating, publicRating = @publicRating WHERE dvdId = @dvdId";
            var cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@DVD_Title", txtDVDTitle.Text);
            cmd.Parameters.AddWithValue("@Year", numYear.Value);
            cmd.Parameters.AddWithValue("@Rating", cmbRating.Text);
            cmd.Parameters.AddWithValue("@publicRating", numPublicRating.Value);
            cmd.Parameters.AddWithValue("@dvdId", id);

            conn.Open();

            cmd.ExecuteNonQuery();                      

            conn.Close();

            //Update Text
            lvwDVDs.Items[(int)txtDVDTitle.Tag].Text = txtDVDTitle.Text;


            RetrieveData();

            //for (int i = 0; i < theData.Rows.Count; i++)
            //{
            //    if(theData.Rows[i]["dvdId"].ToString() == txtDVDTitle.Tag.ToString())
            //    {
            //        lvwDVDs.Items[i].Text = txtDVDTitle.Text;
            //        break;
            //    }
            //}
        }

       
    }
}
