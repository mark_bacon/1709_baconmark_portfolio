﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE03
{

    /*Mark Bacon
         * Visual Frameworks
         * May 2018
         * CE03
         */
    public partial class Display : Form
    {
        //create event for clearing all the data on the display view page.
        public event EventHandler ClearAll;

        private SchoolInfo schoolInfo;

        public Display(List<Student> list, SchoolInfo form)
        {
            InitializeComponent();
            List = list;
            if (classOneList == null)
            {


                foreach (var item in list)
                    classOneList.Items.Add(item);

                schoolInfo = form;
                classOneList.SelectedValueChanged += ClassOneList_SelectedValueChanged;
                FormClosed += Display_FormClosed;

            }
        }

        private void Display_FormClosed(object sender, FormClosedEventArgs e)
        {
            schoolInfo.CheckChanged();
        }

        private void ClassOneList_SelectedValueChanged(object sender, EventArgs e)
        {
            var selected = classOneList.SelectedItem as Student;
            schoolInfo.SetStudent(selected);
        }

        public List<Student> List { get; }

        private void ToolStripButton1_ClearAll(object sender, EventArgs e)
        {
            /*List.Clear();
            classOneList.Items.Clear();
            schoolInfo.ClearInfo();
            */
            if (ClearAll != null)
            {
                ClearAll(this, new EventArgs());
            }

        }

       

        public void SetStudents(List<Student> students)
        {
            classOneList.Items.Clear();
            foreach (var item in students)
                classOneList.Items.Add(item);
        }
        
    }
}
