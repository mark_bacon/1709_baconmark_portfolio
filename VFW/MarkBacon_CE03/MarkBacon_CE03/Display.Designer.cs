﻿namespace MarkBacon_CE03
{
    partial class Display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Display));
            this.Class1groupbox = new System.Windows.Forms.GroupBox();
            this.classOneList = new System.Windows.Forms.ListBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_ClearAll = new System.Windows.Forms.ToolStripButton();
            this.Class1groupbox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Class1groupbox
            // 
            this.Class1groupbox.Controls.Add(this.classOneList);
            this.Class1groupbox.Location = new System.Drawing.Point(3, 78);
            this.Class1groupbox.Name = "Class1groupbox";
            this.Class1groupbox.Size = new System.Drawing.Size(498, 475);
            this.Class1groupbox.TabIndex = 4;
            this.Class1groupbox.TabStop = false;
            this.Class1groupbox.Text = "Class 1";
            // 
            // classOneList
            // 
            this.classOneList.FormattingEnabled = true;
            this.classOneList.ItemHeight = 25;
            this.classOneList.Location = new System.Drawing.Point(18, 50);
            this.classOneList.Name = "classOneList";
            this.classOneList.Size = new System.Drawing.Size(421, 404);
            this.classOneList.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_ClearAll});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(526, 39);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton_ClearAll
            // 
            this.toolStripButton_ClearAll.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_ClearAll.Image")));
            this.toolStripButton_ClearAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripButton_ClearAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_ClearAll.Name = "toolStripButton_ClearAll";
            this.toolStripButton_ClearAll.Size = new System.Drawing.Size(139, 36);
            this.toolStripButton_ClearAll.Text = "Clear All";
            this.toolStripButton_ClearAll.Click += new System.EventHandler(this.ToolStripButton1_ClearAll);
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 566);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.Class1groupbox);
            this.Name = "Display";
            this.Text = "Display";
            this.Class1groupbox.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox Class1groupbox;
        private System.Windows.Forms.ListBox classOneList;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_ClearAll;
    }
}