﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE03
{
    public partial class SchoolInfo : Form
    {
        /*Mark Bacon
         * Visual Frameworks
         * May 2018
         * CE03
         */


        //fixed groupbox size and position
        //adjusted reset button position
        //The toolbar on display form has been fixed to so text and image are single button.

        //still need to fix
        // 5/7/2018 feedback
        // Structure / Efficiency: Good 7/10, If you're going to have a catch, you should always give some kind of feedback to the user so they know why nothing happened. Otherwise, they will keep trying to do the same thing that doesn't work.
        // Layout: Good 11/15, The checkmark on the Display menu command is a little off.You need to tweak the imageScaling property.
        // User Input Controls: Excellent 10/10
        // Main Window - Functionality: Good 15/20, If something other than a comboBox list item is in the comboBox, the item is not added to the List or ListBox, but the user is never informed why.Or you could change the comboBox so the user can only select items in the list.
        // Custom Events: Fair 10/25, Only one custom event used in the project.
        // Second Window - Functionality: Fair 8/20, The ListBox does not populate when the form is initially opened. Selecting a ListBox item does not populate the ListBox with that item's data.




        private Display lastDisplay;

        // The ToolStrip should have only a Clear button with both an image and text. 
        // When clicked, this Clear button should clear the ListBox items and the main window’s list of items, 
        // and the List->Clear option in the main window’s menu bar should have the same functionality.  

        //set global for list

        public List<Student> Students { get; set; }

        public SchoolInfo()
        {
            InitializeComponent();
            Students = new List<Student>();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void CheckChanged()
        {
            displayMenuItem.Checked = false;
        }

        public void SetStudent(Student selected)
        {
            firstNameTextbox.Text = selected.FirstName;
            lastNameTextBox.Text = selected.LastName;
            genderComboBox.Text = selected.Gender.ToString("G");
            gradeComboBox.Text = selected.Grade.ToString("G");
            numAge.Value = selected.Age;
            chkIsGraduate.Checked = selected.Graduate;
        }

        public void ClearInfo()
        {
            firstNameTextbox.Clear();
            lastNameTextBox.Clear();
            genderComboBox.Text = "";
            gradeComboBox.Text = "";
            numAge.Value = 0;
            chkIsGraduate.Checked = false;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            firstNameTextbox.Text = "";
            lastNameTextBox.Text = "";
            genderComboBox.SelectedItem = "";
            gradeComboBox.Text = "";
            numAge.Value = 0;
            chkIsGraduate.Checked = false;
        }

        private void DisplayStudentDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!displayMenuItem.Checked)
            {
                lastDisplay = new Display(Students, this);
                //subscribe to the event
                lastDisplay.ClearAll += Form_ClearAll;
                lastDisplay.FormClosed += LastDisplay_FormClosed;
            }
            
            lastDisplay.Show();
            displayMenuItem.Checked = true;
        }

        private void LastDisplay_FormClosed (object sender, FormClosedEventArgs e)
        {
            displayMenuItem.Checked = false;
        }

        private void Form_ClearAll(object sender, EventArgs e)
        {
            this.Students.Clear();
            if(displayMenuItem.Checked)
            {
                lastDisplay.SetStudents(this.Students);
            }
           //form.classOneList.Items.Clear();
           // schoolInfo.ClearInfo();
        }

        private void AddClassOneButton_Click(object sender, EventArgs e)
        {
            try
            {
                Students.Add(new Student()
                {
                    FirstName = firstNameTextbox.Text,
                    LastName = lastNameTextBox.Text,
                    Gender = (Gender)Enum.Parse(typeof(Gender), genderComboBox.Text),
                    Grade = (Grade)Enum.Parse(typeof(Grade), gradeComboBox.Text)
                });

                firstNameTextbox.Text = "";
                lastNameTextBox.Text = "";
                genderComboBox.Text = "";
                gradeComboBox.Text = "";

                if(lastDisplay != null)
                {
                    lastDisplay.SetStudents(Students);
                }
            }
            catch (Exception)
            { // do nothing..}
            }
        }

    }
}
