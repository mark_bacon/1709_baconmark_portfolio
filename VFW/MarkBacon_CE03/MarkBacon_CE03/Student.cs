﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE03
{

    /*Mark Bacon
         * Visual Frameworks
         * May 2018
         * CE03
         */
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public Grade Grade { get; set; }
        public decimal Age { get; set; }
        public bool Graduate { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum Grade
    {
        A,
        B,
        C,
        F,
        D,
    }

}
