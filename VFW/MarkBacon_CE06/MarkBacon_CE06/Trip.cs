﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE06
{/*
     MarkBacon
     Visual Frameworks
     May 2018
     CE06
     */
    public class Trip
    {
        string direction;
        decimal miles;
        decimal hours;
        string mode;

        public string Direction { get => direction; set => direction = value; }
        public decimal Miles { get => miles; set => miles = value; }
        public decimal Hours { get => hours; set => hours = value; }
        public string Mode { get => mode; set => mode = value; }

        public override string ToString()
        {
            return ("Direction " + Direction);
        }
    }
}
