﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MarkBacon_CE06

/*
 MarkBacon
 Visual Frameworks
 May 2018
 CE06
 Structure / Efficiency: Good 7/10, There's no need for a custom event when working in a single form. You're merely subscribing to an event handler method that already exists on the same form.
Layout: Excellent 10/10
TreeView: Excellent 30/30
Tab Control: Excellent 10/10
Menu Options: Good 11/15, The New menu command should clear the TreeView, not the user input controls.
Display Controls: Excellent 25/25
 */
{
    public partial class Form1 : Form
    {

        public List<Trip> Trips { get; set; } = new List<Trip>();
        public Form1()
        {
            InitializeComponent();
            cmbDirection.Text = "Nowhere";

            this.TripAdded += Form1_TripAdded;
        }

        private void Form1_TripAdded(object sender, Trip addedTrip)
        {
            InsertTripOnTreeview(addedTrip);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cmbDirection.Text = "Nowhere";
            numHours.Value = 0;
            numMiles.Value = 0;
            txtMode.Text = "";
        }

        private void btnAddTripLeg_Click(object sender, EventArgs e)
        {
            // Being extra safe
            if (cmbDirection.Text == "Nowhere")
                return;

            var trip = new Trip();
            trip.Direction = cmbDirection.Text;
            trip.Miles = numMiles.Value;
            trip.Hours = numHours.Value;
            trip.Mode = txtMode.Text;

            Trips.Add(trip);

            //trigger the event
            OnTripAdded(trip);
        }

        //method for creating our tree view
        // pure function
        private void InsertTripOnTreeview(Trip trip)
        {

            // insert the last trip

         
            //create the root node for the TreeView
            TreeNode tripNode = new TreeNode();

            //make the text for the node equal to the object using
            //the overridden ToString method
            tripNode.Text = trip.Direction;

            // the imageindex is the one that shows by default
            // they all default to 0

            tripNode.SelectedImageIndex = 5; // The gear

            // for the assignment
            switch (trip.Direction)
            {
                case "North":
                    tripNode.ImageIndex = 3;
                    break;
                case "East":
                    tripNode.ImageIndex = 2;
                    break;
                case "South":
                    tripNode.ImageIndex = 0;
                    break;
                case "West":
                    tripNode.ImageIndex = 1;
                    break;
            }

            // we will add the nodes to this root node
            tripNode.Nodes.Add("miles", "Miles: " + trip.Miles, 7, 5);
            tripNode.Nodes.Add("hours", "Hours: " + trip.Hours, 6, 5);
            tripNode.Nodes.Add("mode", "Mode: " + trip.Mode, 4, 5);


            //add the first node as the root of the treeview
            treeView1.Nodes.Add(tripNode);

        }



        //delegate for adding trip info to the tree view
        public event EventHandler<Trip> TripAdded;

        private void OnTripAdded(Trip addedTrip)
        {
            if (TripAdded != null)
                TripAdded(this, addedTrip);
        }

        private void cmbDirection_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbDirection.Text == "Nowhere")
            {
                btnAddTripLeg.Enabled = false;
                lblWarning.Visible = true;
            }
            else
            {
                btnAddTripLeg.Enabled = true;
                lblWarning.Visible = false;

            }
        }

        private void UpdateTotals()
        {
            txtTotalLegs.Text = Trips.Count.ToString();

            decimal totalMiles = 0;
            decimal totalHours = 0;

            foreach (var trip in Trips)
            {
                totalMiles += trip.Miles;
                totalHours += trip.Hours;
            }

            txtTotalHours.Text = totalHours.ToString();
            txtTotalMiles.Text = totalMiles.ToString();


        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == this.tabTotals)
            {
                UpdateTotals();
            }
        }
    }
}
