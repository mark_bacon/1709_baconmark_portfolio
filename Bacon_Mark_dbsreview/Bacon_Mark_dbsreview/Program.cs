﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace Bacon_Mark_dbsreview
{
    class Program
    {
        MySqlConnection _con = null;

        static void Main(string[] args)
        {

            Program instance = new Program();
            
            bool running = true;
            instance._con = new MySqlConnection();

            while (running)
            {
                Console.Clear();

                Console.WriteLine("-----------------\nWeather App\n-----------------");
                Console.WriteLine("");

                string userInput = Utility.ReadString(promptMessage: "Please enter the city you would like to check the weather or exit:  ",
                                                    errorMessage: "You have entered an invalid option")
                                                    .ToLower();

                if (userInput == "exit")
                {
                    break;
                    //running = false;
                    //continue;
                }
                
                string sql = $"SELECT city, temp, pressure, humidity, createdDate FROM Weather WHERE LOWER(city) = '{userInput}' ORDER BY createdDate desc";

                //MySqlCommand cmd = new MySqlCommand();
                //cmd.CommandText = sql;
                //cmd.Connection = instance._con;
                //cmd.Parameters.Add("@1", userInput);
                //cmd.ExecuteReader();

                instance.Connect();

                DataTable data = instance.QueryDB(sql);
                DataRowCollection rows = data.Rows;

                if(rows.Count == 0)
                {
                    Console.WriteLine("No Data Available for the selected city");
                }
                else
                {
                    var row = rows[0];

                    var weatherInfo = new Weather
                    {
                        City = row["city"]?.ToString(),
                        Temp = row["temp"]  != DBNull.Value ? Convert.ToDecimal(row["temp"]) : 0,
                        Pressure = row["pressure"] != DBNull.Value ? Convert.ToDecimal(row["pressure"]) : 0,
                        Humidity = row["humidity"] != DBNull.Value ? Convert.ToInt32(row["humidity"]) : 0,
                        CreatedDate = row["createdDate"]?.ToString()
                    };

                    Console.WriteLine(weatherInfo);
                }

                instance._con.Close();

                Utility.PauseBeforeContinuing();

            }
        }

       

      

        void Connect()
        {
            BuildConString();

            try
            {
                _con.Open();
                Console.WriteLine("Connection Successful");
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);

            }
        }


        void BuildConString()
        {
            string ip = "";
            //alternative formats
            // "c:\\VFW\\connect.txt"
            // @"c:\VFW\connect.txt"
            using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string conString = $"Server={ip};";

            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=SampleAPIData;";
            conString += "port=8889;";

            _con.ConnectionString = conString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}





