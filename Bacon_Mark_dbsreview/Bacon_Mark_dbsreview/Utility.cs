﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_dbsreview
{
    class Utility
    {

        public static void PauseBeforeContinuing()
        {
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        

        
        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

    }
}
    

