﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_dbsreview
{
    class Weather
    {
        public string City { get; set; }
        public decimal Temp { get; set; }
        public decimal Pressure { get; set; }
        public int Humidity { get; set; }
        public string CreatedDate { get; set; }

        public Weather()
        {

        }

        public Weather(string city, decimal temp, decimal pressure, int humidity, string createdDate)
        {
            City = city;
            Temp = temp;
            Pressure = pressure;
            Humidity = humidity;
            CreatedDate = createdDate;
        }

        public override string ToString()
        {
            return $"City: {City} \n\tTemp: {Temp}°K \n\tPressure: {Pressure}mb \n\tHumidity: {Humidity}% \n\tDate: {CreatedDate}";
        }













    }
}
