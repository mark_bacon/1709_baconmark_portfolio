var topHeadline = document.querySelector("#blog h1");
topHeadline.innerHTML = "World Cup 2018 Soccer Stadiums";
topHeadline.style.fontWeight = '700';



var request = new Request('https://BACON2420.github.io/stadiumData.json');
console.log('ping'); // 0
fetch(request) // about 10s
    .then(function (response) {
        console.log('request done, parsing....')
        return response.json();
    }) // parser takes 1s
    .then(function(json) {
        console.log('parsing done, creating elements...')
        //Save the JSON data locally
        var data = json;

        console.log(data);
        //245848343.c5c321c.9949e985b8804e77a7589a523588cf4d
        //insert HTML articles & into section (insertAdjacentHTML)
        var locationSection = document.querySelector('#blog');
        var htmlArticles = document.getElementsByTagName('article');
        for(var i=0; i< htmlArticles.length; i++){
            var stadium = data.stadiums[i];

            var headline = htmlArticles[i].querySelector('h3');
            headline.innerHTML = stadium['name'];
            var image = htmlArticles[i].querySelector('img');
            image.src = stadium['image'];

            var city = htmlArticles[i].getElementsByTagName('dd')[0];
            city.innerHTML = stadium['city'];
            var cityKey = htmlArticles[i].getElementsByTagName('dt')[0];
            cityKey.innerHTML = "City";
            var latKey = htmlArticles[i].getElementsByTagName('dt')[1];
            latKey.innerHTML = "Latitude";
            var lonKey = htmlArticles[i].getElementsByTagName('dt')[2];
            lonKey.innerHTML = "Longitude";
            var lat = htmlArticles[i].getElementsByTagName('dd')[1];
            lat.innerHTML = stadium['lat'];
            var lon = htmlArticles[i].getElementsByTagName('dd')[2];
            lon.innerHTML = stadium['lng'];

        }

        document.querySelector('button').onclick = function(){

            this.remove();

            var articleTemplate = document.querySelector('article').innerHTML;

            for(var i=3; i< data.stadiums.length; i++){

                var newArticle = document.createElement('article');
                newArticle.innerHTML = articleTemplate;

                newArticle.querySelector('h3').innerHTML = data.stadiums[i].name;
                newArticle.querySelector('img').src = data.stadiums[i].image;
                document.getElementById('blog').appendChild(newArticle);

                newArticle.getElementsByTagName('dd')[0].innerHTML = data.stadiums[i].city;
                newArticle.getElementsByTagName('dt')[0].innerHTML = "City";
                newArticle.getElementsByTagName('dd')[1].innerHTML = data.stadiums[i].lat;
                newArticle.getElementsByTagName('dt')[1].innerHTML = "Latitude"
                newArticle.getElementsByTagName('dd')[2].innerHTML = data.stadiums[i].lng;
                newArticle.getElementsByTagName('dt')[2].innerHTML = "Longitude";

            }


        }

}); // take 20s
console.log('pong'); // 0