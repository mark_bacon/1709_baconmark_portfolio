var topHeadline = document.querySelector("#blog h2");
    topHeadline.innerHTML = "World Cup 2018 Soccer Stadiums";
    topHeadline.style.fontWeight = '700';


//use AJAX to load the JSON file
var xhr = new XMLHttpRequest();
xhr.onload = function () {
    //Save the JSON data locally
    var data = JSON.parse(xhr.responseText);
    console.log(data)

    //insert HTML articles & into section (insertAdjacentHTML)
var locationSection = document.querySelector('#blog');
var htmlArticles = document.getElementsByTagName('article');
for(var i=0; i< htmlArticles.length; i++){
    var stadium = data.stadiums[i];

    var headline = htmlArticles[i].querySelector('h3');
    headline.innerHTML = stadium['name'];
    var image = htmlArticles[i].querySelector('img');
    image.src = stadium['image'];
    var city = htmlArticles[i].getElementsByTagName('dd')[0];
    city.innerHTML = stadium['city'];
    var cityKey = htmlArticles[i].getElementsByTagName('dt')[0];
    cityKey.innerHTML = "City";
    var latKey = htmlArticles[i].getElementsByTagName('dt')[1];
    latKey.innerHTML = "Latitude";
    var lonKey = htmlArticles[i].getElementsByTagName('dt')[2];
    lonKey.innerHTML = "Longitude";
    var lat = htmlArticles[i].getElementsByTagName('dd')[1];
    lat.innerHTML = stadium['lat'];
    var lon = htmlArticles[i].getElementsByTagName('dd')[2];
    lon.innerHTML = stadium['lng'];

}

document.querySelector('button').onclick = function(){

    this.remove();

    var articleTemplate = document.querySelector('article').innerHTML;

    for(var i=3; i< data.stadiums.length; i++){

        var newArticle = document.createElement('article');
        newArticle.innerHTML = articleTemplate;

        newArticle.querySelector('h3').innerHTML = data.stadiums[i].name;
        newArticle.querySelector('img').src = data.stadiums[i].image;
        document.getElementById('blog').appendChild(newArticle);

        newArticle.getElementsByTagName('dd')[0].innerHTML = data.stadiums[i].city;
        newArticle.getElementsByTagName('dt')[0].innerHTML = "City";
        newArticle.getElementsByTagName('dd')[1].innerHTML = data.stadiums[i].lat;
        newArticle.getElementsByTagName('dt')[1].innerHTML = "Latitude"
        newArticle.getElementsByTagName('dd')[2].innerHTML = data.stadiums[i].lng;
        newArticle.getElementsByTagName('dt')[2].innerHTML = "Longitude";

    }


}

//put conditional to make sure location section of blog is found
/*   if(locationSection){
        var stadiums = '';
       for(var i=0; i< data.stadiums.length; i++) {
           stadiums += '<article>';
           stadiums += '<p class="thumbnail"><img src="'+data.stadiums[i].image +
               '" alt="' + data.stadiums[i].city + '"></p>';
            stadiums += '<h3>' + data.stadiums[i].name + '</h3>';
            stadiums += '<h4>' +data.stadiums[i].city + '</h4>';
            stadiums += '<p> </p>';

           stadiums += '</article>';


       }
        locationSection.querySelector('h2').insertAdjacentHTML("afterend", stadiums);

    } */
};
//Call AJAX
xhr.open('GET', 'https://BACON2420.github.io/stadiumData.json', true);
xhr.send(null);