
var contactForm = document.querySelector('#form');

//1. Change the title of the tag to CONTACT ME (or anything else you deem professional in nature and matching your chosen Theme).
var topHeadLine = document.querySelector("#form h2");
topHeadLine.innerHTML = "CONTACT US NOW";
topHeadLine.style.fontWeight = '700';
topHeadLine.style.color = "red";

//2. Change the background Image
document.body.style.backgroundImage = "url('https://thedubliner.gr/wp-content/uploads/fifa-world-cup-2018-logo-png-official-television-fifa-world-cup-2018-429.png')";
//3. Change the first paragraph to match your theme.
var paragraph = document.querySelector("#form p");
paragraph.innerHTML = "We are always interested in hearing your feedback and how we can improve the site. Please fill out the form below and we will contact you soon. Thanks.";
//4. Update the “placeholder” attribute to match your theme.
//var allParagraphs = document.querySelectorAll("#form p label");
//paragraph[3].innerHTML = "Favorite Team or Player";


//5. Disable the submit button if only all the validation checks are approved
function validateForm(event) {
    event.preventDefault()//stop button submission
    var form = document.querySelector('#form');
    var fields = form.querySelectorAll('.required');
    //check for valid values
    var valid = true;
    for(var i=0;i<fields.length;i++){
        if(!fields[i].value){valid = false;}
    }
    //open submit
    if(valid == true){
        var submit = form.querySelector('[type=submit]');
        submit.removeAttribute('class');
    }
}
//6. create validation errors for every single input.
function validateRequired(event){
    var target = event.target;
    var parent = target.parentElement;
    var error = '<label class="error"> This field is required!</label>';

    if(!target.value.length){
        if(!parent.querySelector('.error')){
            parent.insertAdjacentHTML('beforeend', error);
        }
    } //else {
     //   parent.removeChild(parent.querySelector('.error'));
   // }
}
var requiredFields = contactForm.querySelectorAll('.required');
for(var i=0; i<requiredFields.length; i++){
    requiredFields[i].addEventListener('input', validateForm);
    requiredFields[i].addEventListener('blur', validateRequired);
}

//7. Create a thank you screen
function send(event) {
    event.preventDefault();//stop submission
    var form = document.querySelector('#form');
    var message = '<h2>Thank You!</h2><p>Make sure to check back here for the latest on World Cup 2022!</p>';

    var target = event.target;
    var disabled = target.classList.contains('disabled');

    if(disabled === false){
        form.innerHTML = message;
    }
}
var submit = contactForm.querySelector('[type=submit]');
submit.addEventListener('click', send);