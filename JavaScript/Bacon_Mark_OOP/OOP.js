

class Vehicle {
    constructor(name, make, model, year, color) {
        this.name = name;
        this.make = make;
        this.model = model;
        this.year = year;
        this.color = color;
    }

    getDescription(){
        console.log(`This ${this.name} is a ${this.make} ${this.model} from the year ${this.year} and has a color of ${this.color}.`);
    }
}

class Car extends Vehicle{
    constructor(make, model, year, color){
        super('Car', make, model, year, color);
    }
}

class Truck extends Vehicle {
    constructor(numberOfTires, make, model, year, color){
        super('Truck', make, model, year, color);
        this.numberOfTires = numberOfTires;
    }

    getTollPrice(valuePerAxle) {
        console.log(`\$${this.numberOfTires/2 * valuePerAxle}`);
    }
}

var newCar = new Car("Jeep", "Cherokee", 2014, "white");
newCar.getDescription();

var newTruck = new Truck("20","Jeep", "Cherokee", 2014, "white");
newTruck.getDescription();
newTruck.getTollPrice(10);

var newMotorcycle = new Vehicle("Bike",'Harley', 'HD420', 2018, 'black');
newMotorcycle.getDescription();


