/*
* Mark Bacon
* 1/16/2018
* Project & Portfolio II
* Hangman
* */


String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
};

class Game {

    constructor(){
    }

    get wordList(){
        return ["pizza", "elephant", "baseball", "bicycle", "astronaut","dictionary", "alphabet", "tiger", "bear", "gorilla"];
    };

    randomWord() {
        let randomNumber = Math.floor(Math.random() * (this.wordList.length-1));
        return this.wordList[randomNumber];
    }

    promptForNewGame(){
        let answer = '';
        while(answer !== 'yes' && answer !== 'no')
        {
            let input = prompt('Do you want to start the game? (yes/no)', 'yes');
            if(input)
            {
                answer = input.toLowerCase();
            }
        }
        return answer === 'yes';
    }

    promptForDifficulty() {
        let answer = -1;
        while(answer < 0 || answer > 6)
        {
            let input = prompt('Choose a difficulty from 0 to 6, zero being the hardest:', '6');
            answer = isNaN(input) ? -1 : parseInt(input);
        }
        return answer;
    }

    promptForLetter(){
        let answer = ' ';
        let alphabet = 'abcdefghijklmnopqrstuvwxyz';
        while(alphabet.indexOf(answer) < 0)
        {
            let input = prompt('Type a letter:');
            if(input && input.length === 1)
            {
                answer = input.toLowerCase();
            }
        }
        return answer;
    }

    printState(){
        console.clear();
        let hangman = [
            '\n\n\n', // 0 misses
            ' O\n\n\n', // 1miss
            ' O\n |\n\n', // 2 misses
            ' O\n-|\n\n', // 3 misses
            ' O\n-|-\n\n', // 4 misses
            ' O\n-|-\n/\n', // 5 misses
            ' O\n-|-\n/ \\\n', // 6 misses
        ];

        if(this.misses < hangman.length)
        {
            console.log('%c' + hangman[this.misses] + `\n You have ${this.difficulty - this.misses} chances left.`, 'color: #ff2d40' );
        }

        let hitsPrintable = '';
        for(let i = 0; i < this.hits.length; i++){
            hitsPrintable += this.hits[i] + " ";
        }

        console.log('%c' + hitsPrintable, 'color: #6600FF');
    }

    play () {
        // ask if want to play
        // yes and no
        if(!this.promptForNewGame())
        {
            console.log('User chose not to start a game');
            return;
        }

        console.log('Starting game...');
        this.difficulty = this.promptForDifficulty();

        this.finished = false;
        this.word = this.randomWord();
        this.hits = '_'.repeat(this.word.length);
        this.misses = 0;
        this.printState();

        while (!this.finished) {

            let letter = this.promptForLetter();

            let isHit = this.word.indexOf(letter) >= 0;
            if(isHit) {
                for (let i = 0; i < this.word.length; i++){
                    if(this.word[i] === letter){
                        this.hits = this.hits.replaceAt(i,letter);
                    }
                }
                this.printState();
            }
            else
            {
                this.misses++;
                this.printState();
                console.log(`Ooops, ${letter} is a miss`);
            }


            if(this.hits === this.word) {
                console.log("YOU WIN");
                this.finished = true;
            }
            else if (this.misses > this.difficulty){
                console.log("YOU LOSE");
                this.finished = true;
            }

            // prompt for a letter
            // validate the input
            // check if it is a match
            // update the game state
            //      check if is win/lose
            // show the user the game state
           //  O
           // -|-
           // / \

       }

       this.play();
    }
}

let game = new Game();
game.play();

