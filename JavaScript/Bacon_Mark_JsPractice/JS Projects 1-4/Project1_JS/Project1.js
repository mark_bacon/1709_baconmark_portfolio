/* Mark Bacon
1/11/2008
Project & Portfolio II
Javascript - Conditionals
*/

//function promptValidation(input){
//  while(input === ""){
//    input = prompt("Please do not leave this response blank.")
//    }
//}
var firstItem;
var secondItem;
var thirdItem;
var totalPurchasedBeforeDiscount;
var totalWithTenDiscount;
var totalWithFiveDiscount;



//Problem #1: Temperature Converter
console.log("---Temperature Converter--- Problem #1");
var temp = prompt("----Temperature Converter----\r\nPlease enter a temperature: ");


//test to make sure temperature is not blank
while(temp === ""){
    //user left the field blank
    temp = prompt("Please do not leave this blank.\r\nPlease enter a number temperature: ");
}

var degree = prompt("----Temperature Converter----\r\nIs the temperature currently in Celsius or Fahrenheit? Please enter C or F: ");

while(degree === ""){
    //user left the field blank
    degree = prompt("Please do not leave this blank. Please enter C for Celsius or F for Fahrenheit: ");
}

if(degree === "c" || degree === "C"){
    //if degree is celsius
    //Convert to Fahrenheit
    var tempFahrenheit = (temp * 1.8) + 32;

    //display the results to the user
    alert("The temperature is " + tempFahrenheit + " degrees Fahrenheit.");
}
else if(degree === "f" || degree === "F"){
    //if degree is fahrenheit
    //convert to Celsius
    var tempCelsius = (temp -= 32)/1.8;
    alert("The temperature is " + tempCelsius + " degrees Celsius.");
} else {
    degree = prompt("You have entered something other than C or F for degrees. Please try again");
}

//Problem #2 Last Chance for Gas
console.log("---Last Chance for Gas--- Problem #2");
alert("****Last Chance for Gas.****\r\n         Gas Calculator");
alert("The next gas station is 200 miles away.\r\nCan we make it??????\r\nLet\'s find out.");

//ask for user input
//ask for gallons of car
var carGallons = prompt("How many gallons does your car tank hold?");

//validate input is not blank string
while(carGallons === ""){
    //user left the field blank
    carGallons = prompt("Please do not leave this blank. Please enter a number for car gallons: ");
}
//ask for how full the tank is
var carTankLevel = prompt("How full is your gas tank? Please enter a percentage: ");

while(carTankLevel ===""){
    //user left the field blank
    carTankLevel = prompt("Please do not leave this blank. Please enter a percentage for tank level: ");
}

var carMpg = prompt("How many miles per gallon does your car get?");

while(carMpg === ""){
    //user left the field blank
    carMpg = prompt("Please do not leave this blank. Please enter a number for car miles per gallon: ");
}

var totalMiles = (carGallons * (carTankLevel*.01)) * carMpg;

//create test for conditional statement
if(totalMiles > 200){
    alert("Yes you can drive " + totalMiles + " more miles and you can make it with out stopping for gas!");
}
else {
    alert("You only have " +totalMiles+" miles you can drive, better get gas now while you can!");
}

//Problem #3 Grade letter calculator
console.log("---Grade Letter Calculator--- Problem #3");
//explain to the user the title for the program
var courseGrade = prompt("***Welcome to the Full Sail Course Grade Calculator.***\r\nPlease enter the percentage grade you received for the course: ");
while(courseGrade === ""){
    //user left the field blank
    courseGrade = prompt("Please do not leave this blank. Please enter a percentage you received for the course: ");
}
//if user enters over 100
if(courseGrade >=101 || courseGrade <0){
    courseGrade = prompt("You have entered "+courseGrade+"%, this is outside the grading scale.\r\nPlease enter the percentage grade you received for the course: ")
}
else if(courseGrade <= 100 && courseGrade >= 90){
    alert("You have a "+courseGrade+"%, which means you have earned an A in the class!");
}
else if (courseGrade<= 89 && courseGrade>= 80 ){
    alert("You have a "+courseGrade+"%, which means you have earned a B in the class.");
}
else if (courseGrade<= 79 && courseGrade>= 73){
    alert("You have a "+courseGrade+"%, which means you have earned a C in the class.");
}
else if(courseGrade<= 72 && courseGrade>= 70){
    alert("You have a "+courseGrade+"%, which means you have earned a D in the class.")
}
else if(courseGrade<= 69 && courseGrade>= 0){
    alert("You have a "+courseGrade+"%, which means you have earned a F in the class.")
}

//Problem #4 Discount Double check
console.log("---Discount Double Check--- Problem #4");

firstItem = prompt("Welcome to Discount Double Check.\r\nWhat is the cost of the first item?");
while(firstItem === ""){
    //user left the field blank
    firstItem = prompt("Please do not leave this blank. Please enter the cost of the first item: ");
}
firstItem = Number(firstItem);

secondItem = prompt("What is the cost of the second item?");
while(secondItem === ""){
    //user left the field blank
    secondItem = prompt("Please do not leave this blank. Please enter the cost of the second item: ");
}
secondItem = Number(secondItem);

thirdItem = prompt("What is the cost of the third item?");
while(thirdItem ===""){
    //user left field blank
    thirdItem = prompt("Please do not leave this blank. Please enter the cost of the third item: ");
}
thirdItem = Number(thirdItem);


totalPurchasedBeforeDiscount = (firstItem + secondItem) + thirdItem;

alert("The total amount purchased before discount is " +totalPurchasedBeforeDiscount+ ".");

if(totalPurchasedBeforeDiscount >= 100){
     totalWithTenDiscount = totalPurchasedBeforeDiscount - (totalPurchasedBeforeDiscount*=.10);
     alert("Your total purchase with 10% discount is " +totalWithTenDiscount+ ".");
}
else if(totalPurchasedBeforeDiscount >= 50 && totalPurchasedBeforeDiscount< 100){
    totalWithFiveDiscount = (totalPurchasedBeforeDiscount- (totalPurchasedBeforeDiscount*=.05));
    alert("Your total purchase with 5% discount is " +totalWithFiveDiscount+".");
}
else if(totalPurchasedBeforeDiscount<50){
    alert("Your total purchase with 0% discount is " +totalPurchasedBeforeDiscount+".");
}

