/*Mark Bacon
    1/12/2018
    Project & Portfolio II
    Logic Loops - Project 2
 */

function pleaseEnterNumber(input){
    Number(input);
    return input;
}

function validation(input, message){
    while(input ===""){
        input = prompt(message);

    }

    return input;
}

//Problem #1: Logical Operators: Tire Pressure 1
console.log("---Tire Pressure check--- Problem #1");
var tires = [];
var frontTire1 = tires[0];
var frontTire2 = tires[1];
var rearTire1 = tires[2];
var rearTire2 = tires[3];

//front tire number 1
tires[0] = prompt("---Tire Pressure check---\r\nPlease enter the tire pressure for the first front tire: ");
while(tires[0] === ""){
    //user left the field blank
    tires[0] = prompt("Please do not leave this blank.\r\nPlease enter a number for the first front tire: ");
}
//convert string to number
tires[0] = pleaseEnterNumber(tires[0]);

//front tire number 2
tires[1] = prompt("Please enter the tire pressure for the second front tire: ");
while(tires[1] === ""){
    //user left the field blank
    tires[1] = prompt("Please do not leave this blank.\r\nPlease enter a number for the second front tire: ");
}
tires[1] = pleaseEnterNumber(tires[1]);

//rear tire number 1
tires[2] = prompt("Please enter the tire pressure for the first rear tire: ");
while(tires[2] === ""){
    //user left the field blank
    tires[2] = prompt("Please do not leave this blank.\r\nPlease enter a number for the first rear tire: ");
}
tires[2] = pleaseEnterNumber(tires[2]);


//rear tire number 2
tires[3] = prompt("Please enter the tire pressure for the second rear tire: ");
while(tires[3] === ""){
    //user left the field blank
    tires[3] = prompt("Please do not leave this blank.\r\nPlease enter a number for the second rear tire: ");
}
tires[3] = pleaseEnterNumber(tires[3]);

if(tires[0] === tires[1] && tires[2] === tires[3]){
    alert("The tires pass spec!")
} else {
    alert("Get your tires checked out!")
}


//Problem #2: Logical Operators: Movie Ticket Price
console.log("---Movie Ticket Price Calculator--- Problem #2");
var ticket = [7, 12];

//ask user for their age
var userAge = prompt("---Movie Ticket Price Calculator---\r\nWhat is your age?");
//validation function
userAge = validation(userAge, "Please do not leave this blank. Please enter your age?");
//converts to number
userAge = pleaseEnterNumber(userAge);

var movieTime = prompt("What time is the movie? Please enter the time for the movie in military time: ");
//validation response
movieTime = validation(movieTime, "Please do not leave this blank.\r\nPlease enter the time for the movie in military time: ");
//converts to number
movieTime = pleaseEnterNumber(movieTime);

if(userAge >= 55 || userAge <=10 ){
    alert("The ticket price is $" +ticket[0]+".");
}else if (movieTime>=1400 && movieTime<=1700){
    alert("The ticket price is $" +ticket[0]+".");
}else{
    alert("The ticket price is $" +ticket[1]+".");
}

//Problem #3: Logical Operators: Add up the odds or evens
console.log("---Odds or Evens Calculator--- Problem #3");

var userInput = prompt("---Odds or Evens Calculator---\r\nWould you like the sum of the EVEN or ODD numbers of the array?");
userInput = validation(userInput, "Please do not leave blank.\\r\\nPlease enter if you would like the sum of the EVEN or ODD numbers of the array: ");
userInput.toLowerCase();
//variable for testing if even or odd using modulo
var userDataEvenOrOdd = 0;

//variable for sum of integers
var sumOfNumbers = 0;

for(var i=1; i<=7; i++ ){
    userDataEvenOrOdd = i%2;

    if(userInput === "odd"){
        if(userDataEvenOrOdd === 1){
            sumOfNumbers+=i;

        }
    }
    else if(userInput === "even"){
        if(userDataEvenOrOdd ===0){
            sumOfNumbers+=i;
        }
    }
}
if(userInput === "odd"){
    alert("The sum of the odd numbers are " +sumOfNumbers+".");
}
else if(userInput === "even"){
    alert("The sum of the even numbers are " +sumOfNumbers+".");
}

//Problem #4: While Loop: Charge it!
console.log("---Charge It!!!--- Problem #4");

var creditLimit = prompt("---Charge It!!!---\r\nWhat is your credit limit?");

creditLimit = validation(creditLimit, "Please do not leave blank.\r\nPlease enter the credit limit: ");


var newPurchase;
while(creditLimit>=0) {
    newPurchase = prompt("How much was your next purchase?");
    newPurchase = validation(newPurchase);
    newPurchase = pleaseEnterNumber(newPurchase);
    creditLimit -= newPurchase;
    alert("With your current purchase of $" +newPurchase+ ", you can still spend $" +creditLimit+".");
}
if(creditLimit<= 0){
    alert("With your last purchase you have reached your credit limit and exceeded it by $" +creditLimit+".");
}