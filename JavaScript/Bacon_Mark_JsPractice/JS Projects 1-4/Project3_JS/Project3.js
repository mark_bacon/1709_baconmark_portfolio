/*Mark Bacon
* 1/12/2018
* Project & Portfolio II
* Count Fish - Project 3
* */

function validation(input, message){
    while(input ===""){
        input = prompt(message);

    }

    return input;
}



//Problem #1: Count the Fish
console.log("---Count the Fish--- Problem #1");


//declare array and define it
var myFish;
myFish  =  ["red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" ];

var userSelection;
userSelection = prompt("---Count the Fish---\r\nPlease choose a color of fish: \r\nChoose (1) for Red, (2) for Blue, (3) for Green, and (4) for Yellow.");

userSelection = validation(userSelection, "You have entered something other than a number.\r\nPlease enter a number from 1 to 4: ");
while(!(userSelection >= 1 && userSelection <= 4)){
    userSelection = prompt("You have entered a number that is not an available choice. Please try again: ")
}

var totalFish = 0;



//if statement for red selection input from user
if(userSelection === "1"){
    //loop through the array
    for(var i = 0; i< myFish.length ; i++){
        //pull out the red
        if(myFish[i] === "red"){
            //add up the iterations
            totalFish++;
        }
    }

}

//if statement for blue selection input from user
if(userSelection === "2"){
    //loop through the array
    for(i = 0; i<myFish.length; i++){
        //pull out the blue
        if(myFish[i] === "blue"){
            //add up the iterations
            totalFish++;
        }
    }

}

//if statement for blue selection input from user
if(userSelection === "3"){
    //loop through the array
    for(i = 0; i<myFish.length; i++){
        //pull out the green
        if(myFish[i] === "green"){
            //add up the iterations
            totalFish++;
        }
    }
}

//if statement for yellow selection input from user
if(userSelection === "4"){
    //loop through the array
    for(i = 0; i<myFish.length; i++){
        //pull out the blue
        if(myFish[i] === "yellow"){
            //add up the iterations
            totalFish++;
        }
    }
}

if(userSelection === "1"){
    alert("the total number of red fish are " +totalFish+".");
}
if(userSelection === "2"){
    alert("the total number of blue fish are " +totalFish+".");
}
if(userSelection === "3"){
    alert("the total number of green fish are " +totalFish+".");
}
if(userSelection === "4"){
    alert("the total number of yellow fish are " +totalFish+".");
}