/* Mark Bacon
* 1/13/2018
* project & portfolio II
* methods - project 4
* */

function validation(input, message){
    while(input ===""){
        input = prompt(message);

    }

    return input;
}

function pleaseEnterNumber(input){
    Number(input);
    return input;
}

function CalcPaint(width, height, coats, gallons) {
    var gallonsOfPaint;
    gallonsOfPaint = (width * height * coats) / gallons;

    return gallonsOfPaint;
}

function BeeStingCalc(weight) {
    var beeStings;
    beeStings = weight*9;
    return beeStings;
}

function ReverseCalc(itemList) {
    var backwardsList;
    backwardsList = [];
    for(var i = itemList.length-1; i>=0; i-- ){
        backwardsList.push(itemList[i]);

    }
    return backwardsList;
}

//Problem #1: Painting a Wall
console.log("---Paint Calculator--- Problem #1");
var width;
width = prompt("---Paint Calculator---\r\nWhat is the width of the wall in feet?");
width = validation(width, "Do not leave blank.\r\nWhat is the width of the wall?");
width = pleaseEnterNumber(width);

var height;
height = prompt("What is the height fo the wall in feet?");
height = validation(height, "Do not leave blank.\r\nWhat is the height of the wall?");
height = pleaseEnterNumber(height);

var coatsPaint;
coatsPaint = prompt("How many coats of paint will be applied?");
coatsPaint = validation(coatsPaint, "Do not leave blank.\r\nHow many coats of paint will be applied?");
coatsPaint = pleaseEnterNumber(coatsPaint);

var gallonPaintCovers;
gallonPaintCovers = prompt("What is the surface area one gallon of paint will cover in feet squared?");
gallonPaintCovers = validation(gallonPaintCovers, "Do not leave blank.\r\nWhat is the surface area one gallon of paint will cover in feet squared?");
gallonPaintCovers = pleaseEnterNumber(gallonPaintCovers);

alert("You typed in a width of "+width+" in feet, a height of "+height+" in feet, number of coats of paint is "+coatsPaint+", and the surface area for one gallon of paint is "+gallonPaintCovers+" in square feet.");

var paintCans;
paintCans = CalcPaint(width, height, coatsPaint, gallonPaintCovers);
paintCans = paintCans.toFixed(2);

alert("For "+coatsPaint+" coats of paint on that wall, you will need "+paintCans+" gallons of paint.");

//Problem #2: Stung
console.log("---Bee Sting Calculator--- Problem #2");
var animalWeight;
animalWeight = prompt("---Bee Sting Calculator---\r\nWhat is the weight of the animal in pounds?");
animalWeight = validation(animalWeight, "Do not leave blank.\r\nWhat is the weight of the animal in pounds?");
animalWeight = pleaseEnterNumber(animalWeight);

var totalBeeStings;
totalBeeStings = BeeStingCalc(animalWeight);

alert("It takes "+totalBeeStings+" bee stings to kill this animal.");


//Problem #3: Reverse It!
console.log("---Reverse It!--- Problem #3");

var listOfItems;
listOfItems = ["apple", "pear", "peach", "coconut", "kiwi"];

var finalList;
finalList = [];
finalList = ReverseCalc(listOfItems);

for(var i=0; i<finalList.length; i++){
    alert(finalList[i]);
}

