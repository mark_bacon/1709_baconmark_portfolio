﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_CountFish2
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Mark Bacon
             * 7/15/2017
             * SDI section 01
             * Count Fish
             
             */

            //declare array and define it
            string[] myFish = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };





            Console.WriteLine("Please choose a color of fish");
            Console.WriteLine("Choose (1) for Red, (2) for Blue, (3) for Green, and (4) for Yellow.");

            string userSelectionString = Console.ReadLine();

            //define variable to convert number
            int userSelectionNum;

            while (!(int.TryParse(userSelectionString, out userSelectionNum)))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter a number from 1 to 4:");

                //re-define the variable to save response
                userSelectionString = Console.ReadLine();
            }
                //this is an if statement that will catch if somebody enters a number NOT between 1 and 4
                while (!(userSelectionNum >= 1 && userSelectionNum <= 4))
                {
                    //prompt user that they have entered a choice that is not an option
                    Console.WriteLine("You have entered a number that is not an available choice. Please try again:");

                    //re-define the variable to save response
                    userSelectionString = Console.ReadLine();
                }
            


            //create variable to add up fish
            int totalFish = 0;



            //if statement for red selection input from user
            if (userSelectionNum == 1) {
                //loop through the array 
                foreach (string eachFish in myFish) {
                    //pull out the red
                    if (eachFish == "red") {
                        //add up the iterations
                        totalFish++;
                        
                    }
                    
                }
                


            }
            //if statement for blue selection input from user
            else if (userSelectionNum == 2) {
                //loop through the array
                foreach (string eachFish in myFish) {
                    //pull out the blue
                    if (eachFish == "blue") {
                        //add up the blue iterations
                        totalFish++;
                        
                        
                    }
                }

            }
            //if statement for green selection input from user
            else if (userSelectionNum == 3) {
                //loop through the array 
                foreach (string eachFish in myFish) {
                    //pull out the blue 
                    if (eachFish == "green") {
                        //add up the green iterations
                        totalFish++;
                        
                        
                    }
                }

            }
            //if statement for yellow seleciotn input from user
            else if (userSelectionNum == 4) {
                //loop through the array
                foreach (string eachFish in myFish) {
                    //pull out the yellow
                    if (eachFish == "yellow") {
                        //add up the yellow iterations
                        totalFish++;
                        
                    }
                }

            }
            //prompt user the amount of fish for the requested color
            if (userSelectionNum == 1)
            {
                Console.WriteLine("the total number of red fish are {0}", totalFish);
            }
            else if (userSelectionNum == 2) {
                Console.WriteLine("the total number of blue fish are {0}", totalFish);
            }
            else if (userSelectionNum == 3) {
                Console.WriteLine("the total number of green fish are {0}", totalFish);
            }
            else if (userSelectionNum == 4) {
                Console.WriteLine("the total number of yellow fish are {0}", totalFish);
            }


        }
    }
}
