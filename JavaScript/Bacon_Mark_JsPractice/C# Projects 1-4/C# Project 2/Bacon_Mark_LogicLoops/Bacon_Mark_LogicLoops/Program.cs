﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Mark Bacon
             * 7/15/2017
             * SDI section 01
             * logic loops
            */


            //Problem#1 - Logical Operators: Tire Pressure 1

            Console.WriteLine("Tire Pressure Check.");
            
            //create an array for the tires
            double[] tires = new double[4];



            //ask the user to enter the tire pressure for the first front tire
            Console.WriteLine("Please enter the tire pressure for the first front tire:");

            //capture user response
            string tireString = Console.ReadLine();

            
            //validate that the user entered a number
            while (!(double.TryParse(tireString, out tires[0]))) {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the tire pressure for the first front tire:");

                //re-define the variable to save response
                tireString = Console.ReadLine();
            }

            //convert and input to array in second location
            tires[0] = double.Parse(tireString);

            //ask the user to enter the tire pressure for the second front tire
            Console.WriteLine("Please enter the tire pressure for the second front tire:");

            //capture user response
            string tireString1 = Console.ReadLine();

            //validate that the user entered a number
            while (!(double.TryParse(tireString1, out tires[1])))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the tire pressure for the second front tire:");

                //re-define the variable to save response
                tireString1 = Console.ReadLine();
            }

            //convert and input to array in second location
            tires[1] = double.Parse(tireString1);

            //ask the user to enter the tire pressure for the first rear tire
            Console.WriteLine("Please enter the tire pressure for the first rear tire:");

            //capture user response
            string tireString2 = Console.ReadLine();

            //validate that user did not leave it blank
            while (!(double.TryParse(tireString2, out tires[2])))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the tire pressure for the first rear tire:");

                //re-define the variable to save response
                tireString2 = Console.ReadLine();
            }

            //convert and input to array in third location
            tires[2] = double.Parse(tireString2);

            //ask the user to enter the tire pressure for the second read tire
            Console.WriteLine("Please enter the tire pressure for the second rear tire:");

            //capture user response
            string tireString3 = Console.ReadLine();

            //validate that user did not leave it blank
            while (!(double.TryParse(tireString3, out tires[3])))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the tire pressure for the second rear tire:");

                //re-define the variable to save response
                tireString3 = Console.ReadLine();
            }

            //convert and input to array to fourth location
            tires[3] = double.Parse(tireString3);

            //if statement that test conditional of front tires having same pressure AND rear tires having same pressure
            if (tires[0] == tires[1] && tires[2]==tires[3]) {
                Console.WriteLine("The tires pass spec!");
            } else
            {
                Console.WriteLine("Get your tires checked out!");
            }

            /*test data
             * front left-32, front right-32, back left-30, back right-30 - tires ok
             * front left-36, front right 32, back left-25, back right 25 - check tires
             * front left 28, front right 28, back left 29, back right 31 - check tires
             * front left 28, front right 28, back left 30, back right 30 - tires ok
             * 
             */

            //Problem# 2 Logical Operators - Movie Ticket Price
            Console.WriteLine("Movie Ticket Price Calculator");

            //create an array holding the ticket prices
            decimal[] ticket = new decimal[2];

            ticket[0] = 7.00m;
            ticket[1] = 12.00m;


            //ask the user for their age
            Console.WriteLine("What is your age?");

            //catch the data
            string ageString = Console.ReadLine();

            //variable for converting
            decimal ageNum;

            //test to check that user entered a number
            while (!(decimal.TryParse(ageString, out ageNum )))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter your age:");

                //re-define the variable to save response
                ageString = Console.ReadLine();
            }

            //ask for the time of the movie in military time
            Console.WriteLine("What time is the movie? Please enter the time for the movie in military time:");

            //catch the data
            string movieTimeString = Console.ReadLine();

            //variable for converting
            decimal movieTimeNum;

            //test to check that user entered a number
            while(!(decimal.TryParse(movieTimeString, out movieTimeNum)))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the time for the movie in military time:");

                //re-define the variable to save response
                movieTimeString = Console.ReadLine();
            }

            //create if statement for seniors OR children
            if (ageNum >= 55 || ageNum <= 10) {
                //prompt the user of the price
                Console.WriteLine("The ticket price is ${0}", ticket[0]);

            }
            //additional if statement if the movie is between 1400 and 1700
            else if (movieTimeNum>=1400 && movieTimeNum<=1700) {
                //prompt the user of the price
                Console.WriteLine("The ticket price is ${0}", ticket[0]);

            }
            //final catch all for all other situations
            else
            {
                //prompt user of the price 
                Console.WriteLine("The ticket price is ${0}", ticket[1]);

            }

            /*test data
             * age 57, time 20, ticket price $7.00
             * age 9, time 20, ticket price $7.00
             * age 38, time 20, ticket price $12.00
             * age 25, time 16, ticket price $7.00
             * age 31, time 16, ticket price $7.00
             
             
             */


            //Problem#3 - For Loop: Add up the odds or Evens
            Console.WriteLine("Odds or Evens Calculator");

           


            //Prompt user to see sum of even or odd numbers
            Console.WriteLine("Would you like the sum of the EVEN or ODD numbers of the array?");

            //catch the response
            string userInput = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(userInput)) {
                //prompt user that they need to enter a command
                Console.WriteLine("Please do not leave blank.\r\nPlease type enter if you would like the sum of the EVEN or ODD numbers of the array:");
                //re-define the variable to save the response
                userInput = Console.ReadLine();
            }
            //variable for testing if even or odd using modulo
            int userDataEvenOrOdd = 0;

            //variable for sum of integers
            int sumDataOddOrEven = 0;

            

            for (int intData=1; intData<=7; intData++) {

                userDataEvenOrOdd = intData % 2;

                if (userInput == "odd" || userInput == "ODD")
                {
                    if (userDataEvenOrOdd == 1)
                    {
                        sumDataOddOrEven += intData;

                        // Console.WriteLine("The sum of the odd numbers are {0}", sumDataOddOrEven);

                    }
                }
                else if (userInput == "even" || userInput == "EVEN")
                {
                    if (userDataEvenOrOdd == 0)
                    {
                        sumDataOddOrEven += intData;

                    }



                }
                }
            if (userInput == "odd" || userInput == "ODD")
            {
                Console.WriteLine("The sum of the ODD numbers are {0}", sumDataOddOrEven);
            }

            else if (userInput == "even" || userInput == "EVEN")
            {
                Console.WriteLine("the sum of the EVEN numbers are {0}", sumDataOddOrEven);
            }



            
            //Console.WriteLine("The sum of the numbers are {0}", sumDataOddOrEven);


            /*test data
             * array:{1,2,3,4,5,6,7}, sum of evens- 12, sum of odds-16
             * array:{12,13,14,15,16,17}, sum of evens-42, sum of odds, 45
             * array:{21,22,23,24,25,26,27,28,29,30}, sum of evens-130 , sum of odds- 125
             
             */

            //Problem#4 - While Loop: Charge it!

            Console.WriteLine("Charge It!!");

            //prompt user for credit limit
            Console.WriteLine("What is your credit limit?");

            //capture the response 
            string creditLimitString = Console.ReadLine();

            //variable for converting to number
            decimal creditLimitNum;

            while (!(decimal.TryParse(creditLimitString, out creditLimitNum)))
            {
                //tell user what happened
                Console.WriteLine("You have entered something other than a number.\r\nPlease enter the credit limit:");

                //re-define the variable to save response
                creditLimitString = Console.ReadLine();
            }

            

            

            while ( creditLimitNum>=0 ) {
                Console.WriteLine("How much was your next purchase?");

                //capture text string
                string newPurchaseString = Console.ReadLine();
                //variable for converting to number
                decimal newPurchaseNum;

                //test to make sure a number was entered
                while (!(decimal.TryParse(newPurchaseString, out newPurchaseNum))) {

                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter the next purchase:");

                    //re-define the variable to save response
                    newPurchaseString = Console.ReadLine();
                }

                //take creditLimitNum - newPurchaseNum
                creditLimitNum -= newPurchaseNum;
                Console.WriteLine("With your current purchase of ${0}, you can still spend ${1}", newPurchaseNum, creditLimitNum);
                
                }
            //if the limit reaches below zero then it will prompt the user that they are done. 
            if (creditLimitNum < 0)
            {
                Console.WriteLine("With your last purchase you have reached your credit limit and exceeded it by {0}", creditLimitNum);
            }


            /*test data
             * credit limit- 20.00,
             *          purchase 1 - 5.00 - you can still spend $15.00
             *          purchase 2 - 12.00 - you can still spend $3.00
             *          purchase 3 - 7.00 - you have exceeded your credit limit by $4.00
             *  credit limit - 100,
             *          purchase 1 - 34 - you can still spend $66.00     
             *          purchase 2 - 45 - you can still spend $21.00
             *          purchase 3 - 25 - you have exceeded your credit limit by $4.00
             
             */
        }

    }
}
