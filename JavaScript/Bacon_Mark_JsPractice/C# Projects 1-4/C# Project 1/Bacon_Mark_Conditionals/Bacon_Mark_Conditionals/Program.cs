﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bacon_Mark_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             * Mark Bacon
             * 7/12/2017
             * SDI 01
             * Conditionals Assignment
             */



            //Problem #1: Temperature Converter

            Console.WriteLine("Temperature Converter");

            //Ask user for Temperature number
            Console.WriteLine("Please enter a number temperature:");

            //catch the temperature
            string temp = Console.ReadLine();

            //Convert string to number
            double tempNum = double.Parse(temp);

            //test to make sure temperature is not blank
            while (!(double.TryParse(temp, out tempNum)))
            {
                //user left the field blank
                Console.WriteLine("Please do not leave this bank. Please enter a number temperature:");

                //re-capture the response again
                temp = Console.ReadLine();

            }
            

            //create variable for final Celsius calculation
            double temperatureFahrenheit = 0;

            //create variable for final Fahrenheit calculation
            double temperatureCelsius = 0;

            //Ask user the user if the degree is in Celsius or Fahrenheit
            Console.WriteLine("Is the degree in Celsius or Fahrenheit? Please enter C or F:");
            //catch the degree type
            string degree = Console.ReadLine();


            if (degree == "c" || degree == "C")
            {
                //if degree is celsius 
                //Convert to Fahrenheit
                temperatureFahrenheit = (tempNum * 1.8) + 32;

                //display the result to the user 
                Console.WriteLine("The temperature is {0} degrees Fahrenheit", temperatureFahrenheit);

            }
            else if (degree == "f" || degree == "F")
            {
                //if degree is fahrenheit
                //convert to Celsius
                temperatureCelsius = (tempNum -= 32) / 1.8;

                //display the result to the user
                Console.WriteLine("The temperature is {0} degrees Celsius", temperatureCelsius);

            }
            else
            {
                //let the user know that they entered something other than C or F
                Console.WriteLine("You have entered something other than C or F for degrees. Please Try again:");

            }

            /*test inputs
             * 32F is 0C
             * 100C is 212F
             * 50c, does a lower case c make a difference, is 122F
             * 17C to 62.6F
             * 86F to 30C
             */


            //Problem #2  Last Chance for Gas

            Console.WriteLine("****Last Chance for Gas.****\r\n      Gas Calulator");

            Console.WriteLine("The next gas station is 200 miles away.\r\n Can we make it??????\r\n Let's find out");

            //ask for user input
            //ask for gallons of car
            Console.WriteLine("How many gallons does your car tank hold?");


            //capture car gallons
            string carGallonsText = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(carGallonsText))
            {
                Console.WriteLine("Please do not leave this bank. Please enter a number for car gallons:");

                //re-capture the response again
                carGallonsText = Console.ReadLine();
            }

            //convert string to number
            double carGallonsNum = double.Parse(carGallonsText);



            //ask for how full the gas tank is
            Console.WriteLine("How full is your gas tank? Please enter a percentage:");

            //Capture the gas tank level 
            string carTankLevelText = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(carTankLevelText))
            {
                Console.WriteLine("Please do not leave this bank. Please enter a number for car tank level:");

                //re-capture the response again
                carTankLevelText = Console.ReadLine();
            }
            //Convert string to number
            double carTankPercent = double.Parse(carTankLevelText);



            //ask the user for the miles per gallon of the car
            Console.WriteLine("How many miles per gallon does your car get?");

            //capture the mpg of the car
            string carMpg = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(carMpg))
            {
                Console.WriteLine("Please do not leave this bank. Please enter a number for car miles per gallon:");

                //re-capture the response again
                carMpg = Console.ReadLine();
            }
            //Convert string to number
            double carMpgNum = double.Parse(carMpg);

            //calculate the info with given inputs
            //create new variable for solutions
            //totalMiles 
            double totalMiles = (carGallonsNum * (carTankPercent * .01)) * carMpgNum;



            //create test for conditional statement

            if (totalMiles > 200)
            {
                Console.WriteLine("Yes you can drive {0} more miles and you can make it with out stopping for gas!", totalMiles);

            }
            else
            {

                Console.WriteLine("You only have {0} miles you can drive, better get gas now while you can!", totalMiles);
            }

            /*tests inputs 
             * gallons 20, gas tank 50%, MPG 25 - result: "yes you can drive 250 more miles and you can make it without stopping for gas!"
             * gallons 12, gas tank 60%, MPG 20 - result: "you only have 144 miles you can drive, better get gas now while you can!"
             * gallons 14, gas tank 80%, MPG 21 - result: "yes you can drive 235.2 more miles and you can make it without stopping for gas!"
             
             
             */


            //Problem #3 Grade Letter Calculator

            //explain to the user the title for the program
            Console.WriteLine("Welcome to the Full Sail Course Grade Calculator");

            //ask the user to enter the grade percentage they received for the course
            Console.WriteLine("Please enter the percentage grade you received for the course:");

            //catch the data entered
            string courseText = Console.ReadLine();

            //identify variable for number
            int courseNum = int.Parse(courseText);

            //use if and else if statements to breakdown the grade scale




            if (courseNum >= 101)
            {
                Console.WriteLine("You have entered {0}%, this is outside the grading scale.\r\n Please enter the percentage grade you received for the course:", courseNum);

                //recapture the user response
                courseText = Console.ReadLine();

                //reconvert string to number
                courseNum = int.Parse(courseText);

            }



            //if the user enters between 90 to 100
            if (courseNum <= 100 && courseNum >= 90)
            {

                Console.WriteLine("You have a {0}%, which means you have earned a(n) A in the class!", courseNum);
            }
            //if the user enters 80 to 89
            else if (courseNum <= 89 && courseNum >= 80)
            {

                Console.WriteLine("You have a {0}%, which means you have earned a(n) B in the class", courseNum);
            }

            //if the user enters 73 to 79
            else if (courseNum <= 79 && courseNum >= 73)
            {

                Console.WriteLine("You have a {0}%, which means you have earned a(n) C in the class", courseNum);
            }
            //if the user enters 70 to 72
            else if (courseNum <= 72 && courseNum >= 70)
            {

                Console.WriteLine("You have a {0}%, which means you have earned a(n) D in the class", courseNum);
            }
            //anything entered below 69
            else if (courseNum <= 69 && courseNum >= 0)
            {

                Console.WriteLine("You have a {0}%, which means you have earned a(n) F in the class", courseNum);
            }










            /* Test data
             * 92 - you have a 92%, which means you have earned an A in the class
             * 80 - you have a 80%, which means you have earned a B in the class
             * 67 - you have a 67%, which means you have earned a F in the class
             * 120 - you entered a 120%, this is outside the grading scale. Please enter the percent grade you received for the course- yes it prompts you to enter again.
             * 72 - you have a 72%, which means you have earned a D in the class


             */

            //Problem#4 - Discount Double Check
            Console.WriteLine("Welcome to Discount Double Check.\r\nLet's check your purchases to see if you get a discount!!!");

            
            
            //Prompt user for cost of first item
            Console.WriteLine("What is the cost of the first item?");

            //Capture string 
            string firstItemString = Console.ReadLine();

            //convert string to number
            decimal firstItemNumber = decimal.Parse(firstItemString);




            //Prompt user for cost of second item
            Console.WriteLine("What is the cost of the second item?");

            //Capture String
            string secondItemString = Console.ReadLine();

            //Convert string to number
            decimal secondItemNumber = decimal.Parse(secondItemString);


            //Calculate the total before discount
            decimal totalPurchaseBeforeDiscount = firstItemNumber + secondItemNumber;

            //report to user the total before discount
            Console.WriteLine("The total amount purchased before discount is ${0}", totalPurchaseBeforeDiscount);

            if (totalPurchaseBeforeDiscount >= 100m) {

                decimal totalWithTenDiscount = (totalPurchaseBeforeDiscount -(totalPurchaseBeforeDiscount*=.1m));

                Console.WriteLine("Your total purchase with 10% discount is ${0}", totalWithTenDiscount);

            }
            else if (totalPurchaseBeforeDiscount >= 50m && totalPurchaseBeforeDiscount < 100m) {

                decimal totalWithFiveDiscount = (totalPurchaseBeforeDiscount - (totalPurchaseBeforeDiscount *= .05m));

                Console.WriteLine("Your total purchase with 5% discount is ${0}", totalWithFiveDiscount);

            }
            else if (totalPurchaseBeforeDiscount<50m) {

                Console.WriteLine("Your total purchase with 0% discount is {0}", totalPurchaseBeforeDiscount);
            }

        

            /*Test data
             * first item $45.50, second item $75.00 - total amount $120.5, total amount with 10% discount $108.45
             * first item $30.00, second item $25.00 - total amount $55.00, total amount with 5% discount $52.25
             * first item $5.75, second item $12.50 - total amount $18.75, total amount with 0% discount $18.75 
             * first item $56.30, second item $89.00 - total amount $145.30, total with 10% discount $130.77
             */



        }







    }
    
}





