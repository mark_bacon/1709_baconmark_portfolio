﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;


namespace MarkBacon_CE09
{
    class Program
    {
        public static List<DVD> Inventory { get; set; } = new List<DVD>();

        public static List<DVD> ShoppingCart { get; set; } = new List<DVD>();

        MySqlConnection _con = null;



        static void Main(string[] args)
        {
            Program instance = new Program();

            instance._con = new MySqlConnection();
            instance.Connect();

            // Read 20 from database
            DataTable data = instance.QueryDB("SELECT DVD_title, price, publicRating FROM DVD LIMIT 20");
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                string dvdTitle = row["DVD_title"].ToString();
                decimal price = Convert.ToDecimal(row["price"]);
                float rating = (float) Convert.ToDecimal(row["publicRating"]);
                
                DVD newDVD = new DVD(dvdTitle, price, rating);

                Inventory.Add(newDVD);
            }

            instance._con.Close();


            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("-----------------\nDVD Emporium\n-----------------");
                Console.WriteLine("1. View Inventory");
                Console.WriteLine("2. View Shopping Cart");
                Console.WriteLine("3. Add DVD to cart");
                Console.WriteLine("4. Remove DVD from cart");
                Console.WriteLine("5. Exit");
                Console.WriteLine("Select an option: ");
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "view inventory":
                        {
                            foreach (DVD dvd in Inventory)
                            {
                                Console.WriteLine(dvd);
                                Console.WriteLine("--------------------------------------");
                            }
                        }
                        break;
                    case "2":
                    case "view shopping cart":
                        {
                            if (ShoppingCart.Count == 0)
                            {
                                Console.WriteLine("Please add some items to the cart first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            
                            foreach (DVD dvd in ShoppingCart)
                            {
                                Console.WriteLine(dvd);
                                Console.WriteLine("--------------------------------------");
                            }

                        }
                        break;
                    case "3":
                    case "add dvd to cart":
                        {
                            if(Inventory.Count == 0)
                            {
                                Console.WriteLine("Inventory is empty.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            DVD addDvd = DVD.SelectDVDFromList(Inventory);

                            // Remove it from the inventory
                            Inventory.Remove(addDvd);
                            // Add to the cart
                            ShoppingCart.Add(addDvd);                
                            
                            
                            //ShoppingCart.Add(addDvd);
                            
                            Console.WriteLine("DVD added to cart");

                        }
                        break;
                    case "4":
                    case "remove dvd from cart":
                        {
                            if (ShoppingCart.Count == 0)
                            {
                                Console.WriteLine("Please add some items to the cart first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            DVD addDvd = DVD.SelectDVDFromList(ShoppingCart);

                            // Remove it from the inventory
                            ShoppingCart.Remove(addDvd);
                            // Add to the cart
                            Inventory.Add(addDvd);

                            Console.WriteLine("DVD removed from the cart");

                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                    default:
                        Console.WriteLine("Please select a valid option");
                        break;                        
                }

                Utility.PauseBeforeContinuing();

            }

        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        void Connect()
        {
            BuildConString();

            try
            {
                _con.Open();
                Console.WriteLine("Connection Successful");
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);

            }
        }


        void BuildConString()
        {
            string ip = "";
            //alternative formats
            // "c:\\VFW\\connect.txt"
            // @"c:\VFW\connect.txt"
            using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string conString = $"Server={ip};";

            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=example_1709;";
            conString += "port=8889;";

            _con.ConnectionString = conString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}
