﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Manager : Salaried
    {
        private decimal _bonus;


        //Manager needs a constructor that takes name, address, salary, and bonus.
       // Name, address, and salary need to be passed to Manager’s parent constructor.

        public Manager(string name, string address, decimal salary, decimal bonus) : base(name, address, salary)
        {
            _bonus = bonus;
        }

        public override decimal CalculatePay()
        {
            return base.CalculatePay() + _bonus;
        }

    }
}
