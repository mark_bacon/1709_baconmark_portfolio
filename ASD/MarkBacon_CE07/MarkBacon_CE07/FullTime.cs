﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class FullTime: Hourly
    {

        // FullTime needs a constructor that takes parameters for name, address, and payPerHour.
        // Name, address, payPerHour, and a constant 40 for hours need to be passed to FullTime’s parent constructor.
        public FullTime(string name, string address, decimal payPerHour) : base(name, address, payPerHour, 40)
        {
           
        }
        

    }
}
