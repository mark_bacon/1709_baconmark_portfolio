﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Contractor: Hourly
    {

        private decimal _noBenefitsBonus;

        //Contractor needs a constructor that takes parameter for name, address, payPerHour, hoursPerWeek, and noBenefitsBonus.
        //All of the parameters except noBenefitsBonus need to be passed to Contractor’s parent constructor.
        public Contractor(string name, string address, decimal payPerHour, decimal hoursPerWeek, decimal noBenefitBonus) : base(name, address, payPerHour, hoursPerWeek)
        {
            _noBenefitsBonus = noBenefitBonus;
        }

        public override decimal CalculatePay()
        {
            return base.CalculatePay() * (1 + _noBenefitsBonus/100); 
        }

    }
}
