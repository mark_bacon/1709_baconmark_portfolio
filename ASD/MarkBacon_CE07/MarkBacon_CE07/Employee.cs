﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Employee : IComparable
    {

        private string _name;
        private string _address;

        public Employee(string name, string address)
        {
            _name = name;
            _address = address;
            
        }

        public string Name { get { return _name; } }

        public string Address { get { return _address; } }

        public virtual decimal CalculatePay()
        {
            return 0;
        }

        public Employee SelectEmployee()
        {
            DisplayEmployees(Program.EmployeeList);

            int choseOne = -1;

            while (choseOne <= 0 || choseOne > Program.EmployeeList.Count)
            {
                choseOne = Program.ReadInteger(promptMessage: "Please choose an employee from the list above: ", errorMessage: "You did not enter a number.");
            }

            choseOne--;

            return Program.EmployeeList[choseOne];
        }

        public static void DisplayEmployees(List<Employee> EmployeeList)
        {
            if (EmployeeList.Count == 0)
            {
                Console.WriteLine("There are no employees");
                return;
            }

            for (int i = 0; i < EmployeeList.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {EmployeeList[i]._name}");
            }
        }

        public static Employee SelectEmployeeFromTheList(List<Employee> EmployeeList)
        {

           
            DisplayEmployees(EmployeeList);
            int choseOne = -1;

            while (choseOne < 0 || choseOne > EmployeeList.Count)
            {
                choseOne = Program.ReadInteger(promptMessage: "Please choose an employee from the list above: ", errorMessage: "You did not enter a number.");
            }

            choseOne--;

            return EmployeeList[choseOne];

        }

        public int CompareTo(object obj)
        {
            Employee otherEmployee = obj as Employee;

            return this._name.CompareTo(otherEmployee._name);
        }
    }
}
