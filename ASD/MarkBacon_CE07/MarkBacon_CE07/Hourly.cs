﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Hourly : Employee
    {
        private decimal _payPerHour;
        private decimal _hoursPerWeek;

        public Hourly(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;

        }


        public override decimal CalculatePay()
        {
            return 52 * _payPerHour * _hoursPerWeek;
        }



    }
}
