﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Salaried: Employee
    {
        private decimal _salary;

        //Salaried needs a constructor that takes name, address, and salary
        //Name and address need to be passed to Salaried’s parent constructor.
        public Salaried(string name, string address, decimal salary) : base(name, address)
        {
            _salary = salary;
        }

        public override decimal CalculatePay()
        {
            return _salary;
        }

    }
}
