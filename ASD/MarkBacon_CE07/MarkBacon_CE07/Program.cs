﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class Program
    {
        public static List<Employee> EmployeeList { get; set; } = new List<Employee>();



        static void Main(string[] args)
        {




            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("-----------------\nEmployee Tracker\n-----------------");
                Console.WriteLine("1. Add employee");
                Console.WriteLine("2. Remove employee");
                Console.WriteLine("3. Display payroll");
                Console.WriteLine("4. Exit");
                Console.WriteLine("Select an option: ");
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "add employee":
                        {

                            // lets the user to choose to create an employee
                            // of type (FullTime, PartTime, Contractor, Salaried, or Manager)
                            string newEmployeeName = ReadString(promptMessage: "Please enter the name of the employee: ",
                                                          errorMessage: "Please enter a valid name for the employee.");

                            string newEmployeeAddress = ReadString(promptMessage: "Please enter the address of the employee: ",
                                                            errorMessage: "Please enter a valid address for the employee.");

                            List<string> employeesTypes = new List<string>()
                            {
                                "full time", "part time", "contractor", "salaried", "manager"
                            };
                            

                            string newEmployeeType = ReadString(promptMessage: "Please enter the type of the employee: (Full time/Part time/Contractor/Salaried/Manager)",
                                                          errorMessage: "Please enter a valid type for the employee.").ToLower();

                            while(!employeesTypes.Contains(newEmployeeType))
                            {
                                newEmployeeType = ReadString(promptMessage: "Please enter the type of the employee: (Full time/Part time/Contractor/Salaried/Manager)",
                                                          errorMessage: "Please enter a valid type for the employee.").ToLower();
                            }



                            Employee newEmployee = null;
                            switch (newEmployeeType)
                            {
                                case "full time":
                                    {
                                        decimal payPerHour = ReadDecimal("Please enter the pay per hour: ", "Please enter a valid number.");

                                        newEmployee = new FullTime(newEmployeeName, newEmployeeAddress, payPerHour);
                                    }
                                    break;
                                    
                                case "part time":
                                    {
                                        decimal payPerHour = ReadDecimal("Please enter the pay per hour: ", "Please enter a valid number.");

                                        decimal hoursPerWeek = ReadDecimal("How many hours per week did they work: ", "Please enter a valid number.");

                                        newEmployee = new PartTime(newEmployeeName, newEmployeeAddress, payPerHour, hoursPerWeek);
                                    }
                                    break;
                                case "contractor":
                                    {
                                        decimal payPerHour = ReadDecimal("Please enter the pay per hour: ", "Please enter a valid number.");

                                        decimal hoursPerWeek = ReadDecimal("How many hours per week did they work: ", "Please enter a valid number.");

                                        decimal noBenefitBonus = ReadDecimal("Please enter the contractor bonus percentage: ", "Please enter a valid number.");

                                        newEmployee = new Contractor(newEmployeeName, newEmployeeAddress, payPerHour, hoursPerWeek, noBenefitBonus);
                                    }
                                    break;
                                case "salaried":
                                    {
                                        decimal salary = ReadDecimal("Please enter the yearly salary: ", "Please enter a valid number.");

                                        newEmployee = new Salaried(newEmployeeName, newEmployeeAddress, salary);                                   
                                    }
                                    break;
                                case "manager":
                                    {
                                        decimal salary = ReadDecimal("Please enter the yearly salary: ", "Please enter a valid number.");

                                        decimal bonus = ReadDecimal("Please enter the yearly bonus: ", "Please enter a valid number.");

                                        newEmployee = new Manager(newEmployeeName, newEmployeeAddress, salary, bonus );
                                    }
                                    break;
                            }



                            EmployeeList.Add(newEmployee);
                            EmployeeList.Sort();
                            Console.WriteLine("New employee added successfully");
                        }
                        break;
                    case "2":
                    case "remove employee":
                        {

                            if (EmployeeList.Count == 0)
                            {
                                Console.WriteLine("Please create an employee first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            Employee selectedEmployee = Employee.SelectEmployeeFromTheList(EmployeeList);

                            //remove employee from the list
                             EmployeeList.Remove(selectedEmployee);


                            Console.WriteLine("Employee removed successfully.");
                            
                            // change employee to property
                          // Console.WriteLine($"Removed \"{selectedEmployee}\" from {EmployeeList}.");
                        }
                        break;
                    case "3":
                    case "display payroll":
                        {
                            // displays all of the employees (1 per line, using properties to access name / address from employee 
                            //and the CalculatePay method for the employee’s yearly pay)

                            if (EmployeeList.Count == 0)
                            {
                                Console.WriteLine("Please create an employee first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            foreach (Employee employee in EmployeeList)
                            {
                                if (employee is Contractor)
                                    Console.WriteLine("Contractor");
                                else if (employee is FullTime)
                                    Console.WriteLine("Full Time");
                                else if (employee is PartTime)
                                    Console.WriteLine("Part Time");
                                else if (employee is Salaried)
                                    Console.WriteLine("Salaried");
                                else if (employee is Manager)
                                    Console.WriteLine("Manager");
                                Console.WriteLine($"Name: {employee.Name}");
                                Console.WriteLine($"Address: {employee.Address}");
                                
                                Console.WriteLine($"Yearly Salary: {employee.CalculatePay()}");
                                Console.WriteLine("-----------------------------------------");
                            }

                            
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                }

                Utility.PauseBeforeContinuing();
            }

        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static float ReadFloat(string promptMessage, string errorMessage = "Please enter a float.")
        {
            float result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(float.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }


        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

    }
} 

