﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE07
{
    class PartTime: Hourly
    {

        //PartTime needs a constructor that takes parameters for name, address, payPerHour, and HoursPerWeek.
        //All of the parameter values need to be passed to PartTime’s parent constructor.
        public PartTime(string name, string address, decimal payPerHour, decimal hoursPerWeek) : base(name, address, payPerHour, hoursPerWeek)
        {
        }
        

    }
}
