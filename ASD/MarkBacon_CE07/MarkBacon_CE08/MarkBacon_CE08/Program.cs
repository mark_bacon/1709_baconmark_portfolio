﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE08
{
    class Program
    {
        static void Main(string[] args)
        {
            // Read the headers aka columns
            List<string> fieldNames = File.ReadAllText("DataFieldsLayout.txt")
                                          .Split(new string[] { "\n" }, StringSplitOptions.None)
                                          .ToList();



            string fileName = ReadString(promptMessage: "Please enter the file: (DataFile1/DataFile2/DataFile3)",
                                                          errorMessage: "Please enter a valid file name.");
           
            while(!(fileName == "DataFile1"|| fileName == "DataFile2" || fileName == "DataFile3"))
            {
                Console.WriteLine("Please enter a valid file name from the list. File names are case senstive.");
                fileName = Console.ReadLine();
            }
            

            // Read the data file
            List<Record> records = new List<Record>();

            using (var reader = new StreamReader($"{fileName}.txt"))
            {
                string currentLine;
                while ((currentLine = reader.ReadLine()) != null)
                {
                    // Check if line is header or footer
                    if (currentLine.StartsWith("BOF") || currentLine.StartsWith("EOF"))
                    {
                        continue;
                    }

                    // Convert each line to a Record
                    Record newRecord = new Record(currentLine);

                    // Store each record on a list
                    records.Add(newRecord);
                }
            }


            // Loop through the list writing the json file
            using(var writer = new StreamWriter($"{fileName}.json"))
            {
                writer.WriteLine("["); // opening array

                for (int i = 0; i < records.Count; i++)
                {
                    string convertedRecord = records[i].ToJson(fieldNames);

                    if(i!=0)
                    {
                        convertedRecord = "," + convertedRecord;
                    }
                    writer.WriteLine(convertedRecord);
                }
                writer.WriteLine("]"); // closing array
            }

            Console.WriteLine($"File converted successfully. {records.Count} records transfered.");
            Console.ReadLine();
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }
    }
}
