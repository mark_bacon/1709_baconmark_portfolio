﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MarkBacon_CE09
{
    class DVD
    {
        private string _name;
        private decimal _price;
        private float _rating;

        public string Name { get { return _name; }  }
        public decimal Price { get { return _price; } }
        public float Rating { get { return _rating; } }

        public DVD(string name, decimal price, float rating)
        {
            _name = name;
            _price = price;
            _rating = rating; 
        }

        public override string ToString()
        {
            return $"Title: {_name} \n\tPrice: {_price} \n\tRating: {_rating}";
        }



        public static DVD SelectDVDFromList(List<DVD> dvds)
        {
            DisplayDVDS(dvds);

            int choseOne = -1;

            while (choseOne <= 0 || choseOne > dvds.Count)
            {
                choseOne = Program.ReadInteger(promptMessage: "Please choose a DVD from the list above: ", errorMessage: "You did not enter a valid dvd.");
            }

            choseOne--;

            return dvds[choseOne];
        }

        public static void DisplayDVDS(List<DVD> dvds)
        {
            if (dvds.Count == 0)
            {
                Console.WriteLine("There are no dvds in the list");
                return;
            }

            for (int i = 0; i < dvds.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {dvds[i].Name}");
            }
        }

    }
}
