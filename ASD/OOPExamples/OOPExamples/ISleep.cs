﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPExamples
{
    interface ISleep
    {

        
        //anything put in an interface is assumed to be public.
        //no business logic is included here. only the API call itself.
        //API stands for application programming interface. 

        bool isSleeping { get; }
        void ToggleSleep();
    }
}
    

