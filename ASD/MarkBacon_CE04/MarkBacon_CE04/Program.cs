﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE04
{
    class Program
    {

        //Classes: Excellent 20/20
        //Log Implementation: Excellent 15/15
        // Car Implementation: Excellent 10/10
        // Menu: Excellent 25/25
        // Input Validation: Good 11.25/15, You have to watch your text case when making case labels for a switch - typing exit does not work because your case label is "Exit":
        // ****fixed the "exit" statement in the switch.
        // Main: Excellent 15/15

        // Needs a private static field of type Logger
        //this field should be initialized to null
        private static Logger _logger = null;

        //Needs a public static method GetLogger that returns the value held by the above field.
        public static Logger Getlogger()
        {
            return _logger;
        }


        static void Main(string[] args)
        {




            Car currentCar = null;
            bool running = true;

            while (running)
            {
                Console.Clear();

               
                if(_logger is LogToConsole)
                {

                    Console.Write($"Logging is currently: "); WriteLineColor($"enabled", ConsoleColor.DarkGreen);

                }
                else
                {
                    Console.Write($"Logging is currently: "); WriteLineColor($"disabled", ConsoleColor.DarkRed);
                }

                Console.WriteLine("");
                Console.WriteLine("---Main Menu---");
                Console.WriteLine("");
                Console.WriteLine("1. Turn logging off");
                Console.WriteLine("2. Turn logging on");
                Console.WriteLine("3. Create a car");
                Console.WriteLine("4. Drive the car");
                Console.WriteLine("5. Destroy the car");
                Console.WriteLine("6. Exit");



                //capture user selection
                string menuSelection = ReadString(promptMessage: "Please make a selection: ",
                                                errorMessage: "You have entered an invalid option");

                menuSelection = menuSelection.ToLower();

                switch (menuSelection)
                {
                    case "1":
                    case "turn logging off":
                        {
                            // Disable logging - create a new DoNotLog object and store it in Program’s static Logger field.
                            _logger = new DoNotLog();
                            continue;
                        }
                        break;
                    case "2":
                    case "turn logging on":
                        {
                            //Enable logging - create a new LogToConsole object and store it in Program’s static Logger field.
                            _logger = new LogToConsole();
                            continue;
                        }
                        break;
                    case "3":
                    case "create a car":
                        {
                            string userVehicleMake = ReadString(promptMessage: "Please enter the make of the car: ",
                                                          errorMessage: "Please enter a valid name for the car's make.");

                            string userVehicleModel = ReadString(promptMessage: "Please enter the model of the car: ",
                                                          errorMessage: "Please enter a valid car model.");

                            string userVehicleColor = ReadString(promptMessage: "Please enter the color of the car: ",
                                                          errorMessage: "Please enter a valid color for the car.");

                            int userVehicleYear = ReadInteger(promptMessage: "Please enter the year of the car: ",
                                                          errorMessage: "Please enter a valid year.");

                            float userVehicleMileage = ReadFloat(promptMessage: "Please enter the vehicle's mileage: ",
                                                           errorMessage: "Please enter a valid mileage for the car.");

                            currentCar = new Car(userVehicleMake, userVehicleModel, userVehicleColor, userVehicleMileage, userVehicleYear);
                            Console.WriteLine("Car created successfully");

                        }
                        break;
                    case "4":
                    case "drive the car":
                        {
                            if(currentCar == null)
                            {
                                Console.WriteLine("Please enter a car first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            //Drive the car - prompt the user for how far they are driving the car and call the Drive method on currentCar.
                            float milesDriven = ReadFloat(promptMessage: "Please enter how many miles you would like to drive: ",
                                                            errorMessage: "Please enter a valid mileage.");
                            currentCar.Drive(milesDriven);

                            Console.WriteLine($"You have driven a total of {currentCar.Mileage}");
                        }
                        break;
                    case "5":
                    case "destroy the car":
                        {
                            if (currentCar == null)
                            {
                                Console.WriteLine("Please enter a car first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            string verifyDestroy = ReadString(promptMessage: "Are you sure you want to destroy the car? (yes/no)",
                                                                errorMessage: "Please enter yes or no.");
                            if (verifyDestroy == "yes" || verifyDestroy == "Yes")
                            {
                                currentCar = null;
                                Console.WriteLine("Car has been destroyed successfully.");
                            }
                            else
                            {
                                continue;
                            }
                        }
                        break;
                    case "6":
                    case "exit":

                        {
                            Console.WriteLine("Bye bye");
                            running = false;
                        }
                        break;






                }
                Utility.PauseBeforeContinuing();
            }
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static float ReadFloat(string promptMessage, string errorMessage = "Please enter a float.")
        {
            float result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(float.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        public static void WriteLineColor(string text, ConsoleColor color)
        {
            var temp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = temp;
        }

        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

    }

}