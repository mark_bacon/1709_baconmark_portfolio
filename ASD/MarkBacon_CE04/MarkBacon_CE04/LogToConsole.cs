﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE04
{
    public class LogToConsole : Logger
    {
        //        LogToConsole
        //Log - should use Console.Writeline to output the lineNumber and the string that was passed in.  It should also increase the lineNumber.
        //LogD - should use Console.Writeline to output the lineNumber, the word DEBUG, and the string that was passed in.  It should also increase the lineNumber.
        //LogW - should use Console.Writeline to output the lineNumber, the word WARNING, and the string that was passed in.  It should also increase the lineNumber.

        public override void Log(string message)
        {
            Console.WriteLine($"{lineNumber++} - {message}");            
        }

        public override void LogD(string message)
        {
            Console.WriteLine($"{lineNumber++} - Debug - {message}");
        }

        public override void LogW(string message)
        {
            Console.WriteLine($"{lineNumber++} - Warning - {message}");
        }
    }
}
