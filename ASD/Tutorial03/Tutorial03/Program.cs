﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial03
{
    class Program
    {
        private static object instance;

        static void Main(string[] args)
        {
            User currentUser = null;

            bool running = true;

            while (running)
            {
                Console.Clear();
                Console.WriteLine("1. Create a user");
                Console.WriteLine("2. Set preferences");
                Console.WriteLine("3. Display current user");
                Console.WriteLine("4. Exit");
                Console.WriteLine("Select an option: ");
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "create a user":
                        {
                            currentUser = CreateUser();
                        }
                        break;
                    case "2":
                    case "set preferences":
                        {
                            SetPreferences(currentUser);
                        }
                        break;
                    case "3":
                    case "display current user":
                        {
                            DisplayUser(currentUser);
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                }

                Utility.PauseBeforeContinuing();
            }
        }

        private static User CreateUser()
        {
            User newUser = null;
            Console.WriteLine("1. User");
            Console.WriteLine("2. Super User");
            Console.WriteLine("What type of user is being created? ");
            string choice = Console.ReadLine().ToLower();

            Console.WriteLine("Enter the user's name: ");
            string name = Console.ReadLine();

            Console.WriteLine("Enter the user's address: ");
            string address = Console.ReadLine();

            int age = Validation.GetInt(18, 120, "Enter the user's age: ");
            switch (choice)
            {
                case "1":
                case "user":
                    {
                        newUser = new User(name, address, age);
                    }
                    break;
                case "2":
                case "super user":
                    {
                        //parent variable can hold objects a objects of any 
                        //of the parent child classes.
                        newUser = new Super(name, address, age);
                    }
                    break;
            }

            return newUser;
        }

        private static void SetPreferences(User currentUser)
        {
            if (currentUser == null)
            {
                Console.WriteLine("Please create a user first.");
            }
            else
            {
                Console.WriteLine();
                bool shouldAutoPlay = Validation.GetBool("Should videos autoplay? (yes/no): ");
                double volumeLevel = Validation.GetDouble("Set the volume level (1-10): ");

                if (currentUser.Prefs == null)
                {
                    currentUser.Prefs = new Preferences(shouldAutoPlay, volumeLevel);
                }
                else
                {
                    currentUser.Prefs.ShouldAutoPlay = shouldAutoPlay;
                    currentUser.Prefs.VolumeLevel = volumeLevel;

                }

            }
        }
        private static void DisplayUser(User currentUser)
        {
            Console.WriteLine();
            if (currentUser == null)
            {
                Console.WriteLine("Please create a user first. ");
            }
            else
            {
                Console.WriteLine($"user name: {currentUser.Name}");
                Console.WriteLine($"user address: {currentUser.Address}");
                //Property and method access is limited to whatever is available
                //to the variables type. Casting can be used to gain access to the other
                //parts of the object if it is of a child type.
                if (currentUser is Super)
                {
                    Super temp = currentUser as Super;
                    temp.DisplayStatus();
                }
                Console.WriteLine($"security level: {currentUser.SecurityLevel}");

                if (currentUser.Prefs != null)
                {
                    Console.WriteLine("user settings: ");
                    Console.WriteLine($"\tautoplay enabled: {currentUser.Prefs.ShouldAutoPlay}");
                    Console.WriteLine($"\tvolume level: {currentUser.Prefs.VolumeLevel}");
                }
            }



        }
    }
}
    

