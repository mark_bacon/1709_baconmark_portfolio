﻿namespace Tutorial03
{
    internal class Super : User
    {
        public Super(string name, string address, int age) : base(name, address, age)
        {
        }

        public override int SecurityLevel
        {
            get
            {
                return 10;
            }
        }

        public void DisplayStatus()
        {
           System.Console.WriteLine("user status: super user");
        }
    }
}