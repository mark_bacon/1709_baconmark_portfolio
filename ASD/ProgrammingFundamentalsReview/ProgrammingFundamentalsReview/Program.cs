﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingFundamentalsReview
{
    class Program
    {
        static void Main(string[] args)
        {

            /*

            ////DO NOT USE//////
            //goto
            //label name
            //var

            /////////////////////

            //  Variables //
            //like a box. what it holds. A name by which you will identify the box. 
            //Syntax for a variable.
            //type (name);
            bool programRunning;

            //(name) = value;
            programRunning = true;

            //type (name) = value;
            bool programIsRunning = true;


            //Basic Operations
            // 
            //Math operations
            //()
            //Multiplication & Division
            //Addition & Subtration
            //Modulus %
            // Integer Division
            int v1 = 2;
            int v2 = 4;
            int v3 = 10;

            int result = v1 + v1;
            //string interpolation used to display the result
            // cw + tab x2
            Console.WriteLine($"result of {v1} + {v1} = {result}");

            result = v2 - v1;
            Console.WriteLine($"result of {v2} - {v1} = {result}");

            result = v2 * v1;
            Console.WriteLine($"result of {v2} * {v1} = {result}");

            result = v3 / v1;
            Console.WriteLine($"result of {v3} / {v1} = {result}");

            result = v2 + v1 * v3;
            Console.WriteLine($"result of {v2} + {v1} * {v3} = {result}");

            result = (v2 + v1) * v3;
            Console.WriteLine($"result of ({v2} + {v1}) * {v3} = {result}");

            result = v3 % v2;
            Console.WriteLine($"result of {v2} + {v1} = {result}");

            result = v3 / v2;
            Console.WriteLine($"result of {v3} / {v2} = {result}");

            //Compound Operations/////
            //
            // *=
            // /=
            // +=
            // -=
            // variable = variable + 2;
            // variable += 2;
            float currentValue = 0;
            Console.WriteLine($"result of {currentValue} += {2} is {currentValue += 2}");
            Console.WriteLine($"result of {currentValue} -= {1} is {currentValue -= 1}");
            Console.WriteLine($"result of {currentValue} *= {10} is {currentValue *= 10}");
            Console.WriteLine($"result of {currentValue} /= {4} is {currentValue /= 4}");


            //Increment and decrement          
            //++variable; (pre - increment)
            //variable++; (post - increment)
            //--variable; (pre - decrement)
            //variable--; (post - decrement)

            

            // Increment ops
            Console.WriteLine($"pre increment ++variable on {currentValue} in a statement results in the value {++currentValue}");
            Console.WriteLine($"post increment variable++ on {currentValue} in a statement results in the value {currentValue++}");

            // decrement ops
            Console.WriteLine($"pre decrement --variable on {currentValue} in a statement results in the value {--currentValue}");
            Console.WriteLine($"post decrement variable-- on {currentValue} in a statement results in the value {currentValue--}");


           

            //relational operators////
            // >, <, ==, >=, <=, !
            int leftValue = 10;
            int rightValue = 5;
            int anotherValue = 10;
            bool bTrue = true;
            Console.WriteLine($"{leftValue} > {rightValue} is {leftValue > rightValue}");
            Console.WriteLine($"{leftValue} < {rightValue} is {leftValue < rightValue}");
            Console.WriteLine($"{leftValue} >= {rightValue} is {leftValue >= rightValue}");
            Console.WriteLine($"{leftValue} <= {rightValue} is {leftValue <= rightValue}");
            Console.WriteLine($"{leftValue} == {anotherValue} is {leftValue == anotherValue}");
            Console.WriteLine($"!{bTrue} is {!bTrue}");

            //Input and Output
            // Input - Console.Readline(); -  reads line of text ending when the user hits return.
            //          Console.Readkey(); - reads in a single key press from the user and continue.
            //          Console.Read();
            // output Console.Writeline(); - display text and start a new line after.
            //          Console.Write(); - display a line of text.
            Console.WriteLine("This is a sample line of output \n\n");
            Console.WriteLine("Please enter your name: ");
            string name = Console.ReadLine();

            Console.WriteLine($"Hello {name}\n\n");
            Console.Write("How old are you? (in years): ");
            string stringAge = Console.ReadLine();
            int age = Convert.ToInt32(stringAge);
            Console.WriteLine($"{name} is {age} years old. \n\n");

           
            //Arrays///////////////
            //
            //int[] arrayOfInts;
            //arrayOfInts = new int[size];
            //arrayOfInts = (1, 2, 4, 6, 7, 3, 1, 5); 

            //multi dimensional arrays
            //int[,] multiArrayOfInts = new int[size_1, size_2];
            //
            //Accessing array
            //Index - value placed inside []. starts at 0 for the first item. 
            //last index is - arrayOfInts.Length - 1.
            //arrayOfInts[2] == 4;
            string[] names;
            names = new string[3];
            Console.WriteLine("Please enter three names: ");
            Console.WriteLine("Enter name 1: ");
            names[0] = Console.ReadLine();
            Console.WriteLine("Enter name 2: ");
            names[1] = Console.ReadLine();
            Console.WriteLine("Enter name 3: ");
            names[2] = Console.ReadLine();

            Console.WriteLine($"Name 1 is {name[0]}");
            Console.WriteLine($"Name 2 is {name[1]}");
            Console.WriteLine($"Name 3 is {name[2]}");

            names[1] = "Bob";

            Console.WriteLine($"Name 2 is now {names[1]}\n\n");


            //Conditionals///////
            //
            // if ("statement that evaluates to true or false")
            //  this line only executes if the statement is true
            // this line still happens no matter what

            // if (statement)
            // {
            //      multiple lines of code
            // }
            //  this line still happens no matter what
            //
            // if (statement) 
            //      this line only runs if the statement was true
            // else
            //      this line only runs if the stateent was false
            // 
            // if (statement)
            // {}
            // else if (statement)
            // {}
            // else
            // {}
            //
            // Switch (itemToCheck)
            // {
            //
            //      case 1:
            //          code here
            //          break;
            //      case "Hello":
            //          break;
            //      default:
            //      code here
            //          break;
            // }
            Console.Write("Pick a number between 1 and 100: ");
            string userInput = Console.ReadLine();
            int userChoice = Convert.ToInt32(userInput);

            if (userChoice == 42)
            {
                Console.WriteLine("The answer to life the universe and everything\n\n");
            }
            else if (userChoice == 7)
            {
                Console.WriteLine("This seems like a pretty good number");
            }
            else
            {
                Console.WriteLine("You feel like there are other numbers that are more important than that one.");
            }

    
            // hide everything that was done before this line
            Console.Clear();

            /// Logical operators //////////
            //
            // And - &&
            // Or - ||
            // Not - !
            // Grouping - ()

            Console.WriteLine("Please enter a number between 1 and 100: ");
            string userInput = Console.ReadLine();
            int userChoice = Convert.ToInt32(userInput);

            if (userChoice > 0 && userChoice < 101)
            {
                Console.WriteLine("You selected a value in the desired range.\n\n");
            }
            else
            {
                Console.WriteLine("Your choice was out of bounds. \n\n");
            }

            */
            // hide everything that was done before this line
            Console.Clear();

            //////Loops/////////
            //
            /*( for(i= 0; i < 10; ++i )
             {
                 //  This block of code repeats
             }

             bool programRunning = true;
             while (programRunning)
             {
                 programRunning = false;
             }

             do
             {

             } while (programRunning);


             int[] values = { 1, 3, 2, 4, 8 }; 
             foreach (int currentValue in values)
             {
                 Console.WriteLine($"Current Value is {currentValue}");
             }
             */
             //string loop
            string[] usersFirstNames = new string[5];
            string[] usersLastNames = new string[5];
        /*
            Console.Write("Enter user 1's first name: ");
            usersFirstNames[0] = Console.ReadLine();
            Console.Write("Enter user 1's last name: ");
            usersLastNames[0] = Console.ReadLine();

            Console.Write("Enter user 2's first name: ");
            usersFirstNames[0] = Console.ReadLine();
            Console.Write("Enter user 2's last name: ");
            usersLastNames[0] = Console.ReadLine();

            Console.Write("Enter user 3's first name: ");
            usersFirstNames[0] = Console.ReadLine();
            Console.Write("Enter user 3's last name: ");
            usersLastNames[0] = Console.ReadLine();

            Console.Write("Enter user 4's first name: ");
            usersFirstNames[0] = Console.ReadLine();
            Console.Write("Enter user 4's last name: ");
            usersLastNames[0] = Console.ReadLine();

            Console.Write("Enter user 5's first name: ");
            usersFirstNames[0] = Console.ReadLine();
            Console.Write("Enter user 5's last name: ");
            usersLastNames[0] = Console.ReadLine();
        
           

            for (int i = 0; i<usersFirstNames.Length && i < usersFirstNames.Length; ++i)
            {
                Console.Write("Enter user {i + 1}'s first name: ");
                usersFirstNames[i] = Console.ReadLine();
                Console.Write("Enter user {i + 1}'s last name: ");
                usersLastNames[i] = Console.ReadLine();


            }

            Console.WriteLine("\n---- List of Users ----");
            for (int userIndex = 0; userIndex < usersFirstNames.Length; ++userIndex)
            {
                Console.WriteLine($"{userIndex + 1}: {usersFirstNames[userIndex]} {usersLastNames[userIndex]} ");
            }
        */
            //Methods and functions
            displayGreeting("Hello World!");

            int userSelectedNumber = GetInt();

            Console.WriteLine($"The user entered the value {userSelectedNumber}");

            //Pause before closing
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();


        }

        //Procedure
        // return type: void
        // method name: displayGreeting
        // method parameters: 
        //      string - message
        static void displayGreeting(string message)
        {
            Console.WriteLine(message);
        }

        //Validation - GetInt
        static int GetInt()
        {
            int validatedInt = 0;
            string input = null;

            do
            {
                Console.Write("Enter an integer: ");
                input = Console.ReadLine();
            } while (Int32.TryParse(input, out validatedInt) == false);

            return validatedInt;
            
        }
    }
}
