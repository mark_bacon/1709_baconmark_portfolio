﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE04
{
    interface ILog
    {
        //must contain the following methods, Log, LogD, LogW

        //The ILog interface must contain the following methods
        //Log - returns nothing and takes a string parameter
        void Log(string message);

        //LogD - returns nothing and takes a string parameter
        void LogD(string message);

        //LogW - returns nothing and takes a string parameter
        void LogW(string message);

    }
}
