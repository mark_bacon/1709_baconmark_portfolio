﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE04
{
    class Car
    {
        // Car should have the following fields
        //make of type string
        //model of type string
        //color of type string
        //mileage of type float
        //year of type int

        
        
        

        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public float Mileage { get; set; }
        public int Year { get; set; }

        //Needs a constructor that takes in parameters for each of its fields and initializes them
        public Car(string make, string model, string color, float mileage, int year)
        {
            this.Make = make;
            this.Model = model;
            this.Color = color;
            this.Mileage = mileage;
            this.Year = year;

            //The new values of the car’s porperties should be logged using the LogD method of the static Logger object in the Program class.
            Logger logger = Program.Getlogger();
            if (logger != null)
            {
                logger.LogD($"Make: {Make}");
                logger.LogD($"Model: {Model}");
                logger.LogD($"Color: {Color}");
                logger.LogD($"Mileage: {Mileage}");
                logger.LogD($"Year: {Year}");
            }
        }
        //Needs a Drive method that returns nothing and takes a parameter of type float
        //The value passed in should be used to update the car’s milage.
        //The new milage should be logged using the LogW method of the static Logger object in the Program class.

        public void Drive(float mileage)
        {
            this.Mileage += mileage;
            Program.Getlogger()?.LogW($"Mileage - {this.Mileage}");
        }

    }
}
