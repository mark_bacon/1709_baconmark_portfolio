﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOTO1710_Week3
{
    class Dog : Animal
    {
        int _loyalty;

        public int Loyalty
        {
            get
            {
                return _loyalty;
            }
            set
            {
                _loyalty = value;
            }
        }

        public Dog(string name, int loyalty) : base(name)
        {
            _loyalty = loyalty;
        }
    }
}
