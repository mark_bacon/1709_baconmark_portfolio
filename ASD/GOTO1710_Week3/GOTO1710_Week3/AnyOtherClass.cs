﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOTO1710_Week3
{
    // ctrl+shift+a
    class AnyOtherClass
    {
        public AnyOtherClass()
        {
            Logger current = Program.GetLogger();

            Console.WriteLine($"Current logger is: {current}");
        }
    }
}
