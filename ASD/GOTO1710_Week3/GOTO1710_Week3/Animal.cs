﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOTO1710_Week3
{
    class Animal
    {
        string _name;

        // prop + tabx2
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public Animal(string name)
        {
            _name = name;
        }
    }
}
