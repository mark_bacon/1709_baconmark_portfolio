﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOTO1710_Week3
{
    class Program
    {

        /*
         * File I/O - CE08
         * 
         * 4 text files
         *      1 - layout file
         *              150 items each on its own line (labels or keys)
         *      3 - data files
         *              header line
         *              100 records
         *                  each record on its own line
         *                  150 data items (values)
         *                  each item is separated by '|'
         *                  Split()
         * 
         *  JSON
         *      .JSON extension
         *      Has to be in the JSON format/layout
         *      
         *      [] - denotes array
         *      {} - start/end JSON object
         *      "label" : "value"
         *      , - expect a nother item of the same type (used with label/value pairs and between objects).
         *      
         *   streamreader
         *   streamwriter
         *   
         *   focus on getting to the code to work converting a single record to JSON
         *   go back and add looping so it works for all 100 records.
         *   
         *   jsonlint.com - 15,120.
         *   
         *   single.json 
         *   
         *   USE RELATIVE FILE PATHS NOT ABSOLUTE
         *   
         *   Absolute path "C:/Windows/Users/Bob/Documents/MyFiles.txt"
         *   
         *   Relative "Output/MyFile.txt"
         *            "MyFile.txt"
         *            "../../MyFile.txt"
         */






        private static Logger currentLogger;

        static void Main(string[] args)
        {
            //currentLogger = null;
            currentLogger = new Logger();

            AnyOtherClass a = new AnyOtherClass();



            ////////////////
            // Inheritance example
            //
            Animal ani = new Animal("Spiky");
            Dog d = new Dog("Fido", 100);

            Console.WriteLine($"My dog {d.Name} is {d.Loyalty}% loyal to me.");

            ani = d;

            // "is" and "as"
            if (ani is Dog)
            {
                Dog temp = ani as Dog;
                temp.Loyalty = 101; // Gave Fido a treat
            }




            // cw+tab x2
            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }

        public static Logger GetLogger()
        {
            return currentLogger;
        }
    }
}
