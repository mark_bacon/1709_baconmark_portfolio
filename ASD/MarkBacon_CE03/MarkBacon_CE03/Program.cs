﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE03
{
    class Program
    {
        static void Main(string[] args)
        {
           /* Structure / Efficiency: Good 7.5 / 10, Line 15 - please see the video in research 01 - Do Not Use.
Classes: Good 11.25 / 15, Teacher should have a string[] for knowledge not just a string.The knowledge should be splitable into individual items not held as a single string.
  Constructors: Excellent 15 / 15
  Menu: Fair 7.5 / 25, Create teacher does not assign the newly created Teacher object to the currentCourse variable.Display does not show all of the required information.
  Main: Excellent 15 / 15
  Input Validation: Good 15 / 20, Crash - select create a student before anything else.

    */


              Course currentCourse = null;
            int studentscount = 0;

            bool running = true;

            while (running)
            {
                Console.WriteLine("");
                Console.WriteLine("---- School Terminal ----");
                Console.WriteLine("\r\nPlease select a menu option:");
                Console.WriteLine("Chose:\r\n(1) Create a Course\r\n(2) Create a Teacher\r\n(3) Create a Student\r\n(4) Display Course\r\n(5) Exit the program ");

                //capture user selection
                string menuChoice = Console.ReadLine().ToLower();




                switch (menuChoice)
                {
                    case "1":
                    case "create a course":

                        {
                            //create a customer
                           
                            string courseName = ReadString(promptMessage: "Please enter the name for the course: ",
                                                           errorMessage: "Please enter a valid name for the course.");
                         
                            string courseDescription = ReadString(promptMessage:"Please enter a description for the course: ",
                                                                  errorMessage: "Please enter a valid description for the course.");

                            int courseEnrolledLimit = ReadInteger(promptMessage: "Please enter the amount of students allowed to enroll in the course: ",
                                                                  errorMessage: "Please enter a valid number for the amount of students that can enroll.");                          
                           
                            currentCourse = new Course(courseName, courseDescription, courseEnrolledLimit);
                            Console.WriteLine("Course Created Successfully");
                        }
                        break;
                    case "2":
                    case "create a teacher":
                        {
                            Teacher newTeacher = null;
                            //check if course has been created
                            if (currentCourse == null)
                            {
                                Console.WriteLine("Please create a course first before creating the teacher.");

                            }
                            //create a teacher
                            
                            string teacherName = ReadString(promptMessage: "Please enter the name of a teacher: ", 
                                                            errorMessage:"You have entered something other than a teacher name.");
                            
                            string teacherDescription = ReadString(promptMessage: "Please enter a description for the teacher: ", 
                                                                    errorMessage:"You have entered something other than a teacher description.");

                            int teacherAge = ReadInteger(promptMessage: "Please enter the age of the teacher: ",
                                                         errorMessage: "Please enter a valid age for the teacher.");
                                                      
                            string teacherKnowledge = ReadString(promptMessage: "Please enter what knowlege the teacher has: ", 
                                                                errorMessage:"You have not entered a valid response.");

                            newTeacher = new Teacher(teacherName, teacherDescription, teacherAge, teacherKnowledge);
                            Console.WriteLine("Teacher Created Successfully");
                        }
                        break;
                    case "3":
                    case "create a student":
                        {
                            if(studentscount >= currentCourse.numberOfStudents)
                            {
                                // error message
                                Console.WriteLine("You have exceeded the maxium amount of students allowed in the course.");
                                continue;
                            }

                             Student newStudent = null;
                           
                            string studentName = ReadString(promptMessage: "Please enter the name of the student: ", 
                                                            errorMessage: "Please enter a valid student name." );
                    
                            string studentDescription = ReadString(promptMessage: "Please enter a description for the student: ", 
                                                                    errorMessage: "Please enter a valid student description.");

                            int studentAge = ReadInteger(promptMessage: "Please enter the age of the student: ", 
                                                        errorMessage: "Please enter a valid age for the student." );

                            int studentGrade = ReadInteger(promptMessage: "Please enter a number grade for the student: ", 
                                                        errorMessage: "Please enter a valid number for the grade.");
                            
                            newStudent = new Student(studentName, studentDescription, studentAge, studentGrade);
                            Console.WriteLine("Student was created successfully.");

                            currentCourse.enrolledStudents[studentscount] = newStudent;
                            studentscount++;
                        }
                        break;
                    case "4":
                    case "display a course":
                        {
                            Console.WriteLine("");
                            if (currentCourse == null)
                            {
                                Console.WriteLine("Please create a course first. ");
                            }
                            else
                            {
                                Console.WriteLine($"Course name: {currentCourse.courseTitle}");
                                Console.WriteLine($"Course description: {currentCourse.courseDescription}");
                                Console.WriteLine($"Student list: {currentCourse.enrolledStudents}");
                            }
                        }
                        break;
                    case "5":
                    case "exit the program":
                        {
                            Console.WriteLine("Bye bye");
                            running = false;
                        }
                        break;


                }

                Utility.PauseBeforeContinuing();
            }
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }
    }
}
