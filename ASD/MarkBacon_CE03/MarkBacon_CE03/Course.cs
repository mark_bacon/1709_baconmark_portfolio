﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE03
{
    class Course
    {
        public string courseTitle { get; set; }
        public string courseDescription { get; set; }

        public Teacher teacher { get; set; }
        public Student[] enrolledStudents { get; set; }
        public int numberOfStudents
        {
            get
            {
                return enrolledStudents.Length;
            }
        }

        public Course(string _courseTitle, string _courseDescription, int _numberOfStudents)
        {
            courseTitle = _courseTitle;
            courseDescription = _courseDescription;

            enrolledStudents = new Student[_numberOfStudents];
        }


    }
}

