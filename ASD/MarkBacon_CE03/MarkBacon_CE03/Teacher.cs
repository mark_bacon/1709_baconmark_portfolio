﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE03
{
    class Teacher:Person
    {
        private string Knowledge { get; set; }

        public Teacher(string name, string description, int age, string _knowledge):base(name, description, age)
        {
            Knowledge = _knowledge;
        }
    }
}
