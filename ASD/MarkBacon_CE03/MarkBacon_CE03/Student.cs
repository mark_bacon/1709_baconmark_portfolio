﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE03
{
    class Student:Person
    {
        private int Grade { get; set; }

        public Student(string name, string description, int age, int _grade) :base(name, description, age)
        {
            Grade = _grade;

            
        }
    }
}
