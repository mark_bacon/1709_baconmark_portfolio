﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceEncapsulationTutorial
{
    class Preferences
    {
        private bool _shouldAutoPlay;
        private double _volumeLevel;

        //Create Constructor
        public Preferences(bool shouldVideosAutoPlay, double volumeLevel)
        {
            _shouldAutoPlay = shouldVideosAutoPlay;
            _volumeLevel = volumeLevel;
        }


        public bool ShouldAutoPlay
        {
            get
            {
                return _shouldAutoPlay;
            }
            set
            {
                _shouldAutoPlay = value;
            }
        }
        public double VolumeLevel
        {
            get
            {
                return _volumeLevel;
            }
            set
            {
                _volumeLevel = value;
            }
        }

      

    }


}
