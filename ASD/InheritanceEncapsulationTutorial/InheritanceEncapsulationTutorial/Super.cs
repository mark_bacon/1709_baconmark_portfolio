﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceEncapsulationTutorial
{   //INHERITANCE - Super inherits from User
    class Super : User
    {
        //Parent is part of the class so Super can hand name/address/age to base 
        //for the parent to handle.
        public Super(string name, string address, int age) : base(name, address, age)
        {
            //no code written but Super has name/address/age functionality
        }

        public void DisplayStatus()
        {
            Console.WriteLine("user status: Super User");
        }
        

        }
    }
}
