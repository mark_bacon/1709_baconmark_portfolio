﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceEncapsulationTutorial
{
    class User
    {
       private string _name;
       private string _address;
       private int _age;

        //Add field of type Preferences
       private Preferences _preferences;


        //Create the constructor
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;

            _preferences = null;
        }


        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
       
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public Preferences Prefs
        {
            get
            {
                return _preferences;
            }
            set
            {
                _preferences = value;
            }


        }
        public virtual int SecurityLevel
        {
            get
            {
                return 1;
            }
        }
        
        

        //Inheritance - Super inherits from User.
       class Super: User
        {
            //Parent is part of the class so Super can handle name/address/age to base
            //for the parent to handle.
            public Super(string name, string address, int age) : base(name, address, age)
            {
                //No code written but Super has name/address/age functionality
            }

            public override int SecurityLevel
            {
                get
                {
                    return 10;
                }
            }
            public void DisplayStatus()
            {
                Console.WriteLine("user status: Super User"  );
            }

                }
            }

           
        } 
        
    

