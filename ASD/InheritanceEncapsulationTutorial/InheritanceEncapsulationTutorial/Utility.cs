﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceEncapsulationTutorial
{
    class Utility
    {
        public static void PauseBeforeContinuing()
        {
            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }

    }
}
