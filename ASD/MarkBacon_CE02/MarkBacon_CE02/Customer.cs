﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE02
{
    class Customer
    {        
        public Customer(string _customerName)
        {
            CustomerName = _customerName;
        }

        public string CustomerName { get; set; }
        
        public CheckingAccount Account { get; set; }

        
    }
}
