﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE02
{
    class Program
    {


        static void Main(string[] args)
        {
            Customer currentCustomer = null;

            var maximumCustomers = 3;

            // creating a array with 100 empty SLOTS FOR customers
            Customer[] customers = new Customer[maximumCustomers];
            var createdCustomersCount = 0;

            int menuChoiceNumber;

            do
            {
                Console.WriteLine("");
                Console.WriteLine("---- Checking Account Terminal ----");
                Console.WriteLine("\r\nPlease select a number for a menu option:");
                Console.WriteLine("Choose:\r\n(1) Create a Customer\r\n(2) Create a Checking Account\r\n(3) Set Account Balance\r\n(4) Display Account Balance\r\n(5) Exit the program ");
                if (currentCustomer != null)
                {
                    Console.WriteLine("Selected customer: {0}", currentCustomer.CustomerName);
                    Console.WriteLine("(6) Select another customer");
                }

                //capture user selection
                String menuChoiceString = Console.ReadLine();
            
                //define variable to convert number
                if (!(int.TryParse(menuChoiceString, out menuChoiceNumber)))
                {
                    Console.WriteLine("You have entered something other than a number.");
                    continue;
                }

                // this is an if statement that will catch if somebody enters a number NOT between 1 and 4
                if (!(menuChoiceNumber >= 1 && menuChoiceNumber <= 5) &&
                    !(currentCustomer != null && menuChoiceNumber == 6))
                {
                    //prompt user that they have entered a choice that is not an option
                    Console.WriteLine("You have entered a number that is not an available choice. Please try again:");
                    continue;
                }

                if (menuChoiceNumber == 1)
                {

                    if (createdCustomersCount >= maximumCustomers)
                    {
                        Console.WriteLine("You have exceeded the amount of customers in our database. Maximum is {0}", maximumCustomers);
                        continue;
                    }

                    //create a customer
                    var newAccountName = ReadString(promptMessage: "Please enter the customer's name for the new account:",
                                                    errorMessage: "Please enter a valid name for the customer.");
                   
                    currentCustomer = new Customer(newAccountName);
                    customers[createdCustomersCount] = currentCustomer;
                    createdCustomersCount++;
                    Console.WriteLine("Customer Created Successfully");

                }
                else if (menuChoiceNumber == 2)
                {
                    // Check if customer has been created
                    if (currentCustomer == null)
                    {
                        Console.WriteLine("Please create a customer first before creating the checking account.");
                        continue;
                    }

                    // Ask for the account number
                    int accountNumber = ReadInteger("Please enter the account number:",
                        "Please enter a valid account number.");

                    

                    var accountBalance = ReadDecimal(promptMessage: "Please enter the account balance: ",
                                                     errorMessage: "Please enter a valid account balance.");


                    // Create a checking account and assinging to customer
                    currentCustomer.Account = new CheckingAccount(accountNumber, accountBalance);
                    Console.WriteLine("Account Created Successfully");
                }
                else if (menuChoiceNumber == 3)
                {
                    // check if user is created
                    if (currentCustomer == null)
                    {
                        Console.WriteLine("Please create a customer and checking account before setting the balance.");
                        continue;
                    }

                    // check if account is created
                    if (currentCustomer.Account == null)
                    {
                        Console.WriteLine("Please create a checking account before setting the balance.");
                        continue;
                    }

                    //prompt for new balance
                    var accountBalance = ReadDecimal(promptMessage: "Please enter the account balance: ",
                                                     errorMessage: "Please enter a valid account balance.");
                    

                    //set the new ballence on customer account
                    currentCustomer.Account.Balance = accountBalance;

                    //set account balance
                    Console.WriteLine("Balance was updated successfully");

                }
                else if (menuChoiceNumber == 4)
                {
                    //get account balance
                    if (currentCustomer == null)
                    {
                        Console.WriteLine("Please create a customer and checking account before setting the balance.");
                        continue;
                    }

                    // check if account is created
                    if (currentCustomer.Account == null)
                    {
                        Console.WriteLine("Please create a checking account before setting the balance.");
                        continue;
                    }
                    Console.WriteLine("The balance is {0}.", currentCustomer.Account.Balance);
                }
                else if (menuChoiceNumber == 5)
                {
                    //exit the program

                    Console.WriteLine("Bye bye");
                }
                else if (menuChoiceNumber == 6)
                {
                    HappyWriteLine($"\t # \t Customer", ConsoleColor.DarkYellow);
                    for (int i = 0; i < createdCustomersCount; i++)
                    {
                        Console.WriteLine($"\t {i + 1} \t {customers[i].CustomerName}");
                    }

                    var selectedNumber = ReadInteger(promptMessage: "Enter the number of the customer that you would like to choose: ",
                                                     errorMessage: "Please enter a valid number");

                    // 1 -> 0
                    // 2 -> 1
                    selectedNumber--;

                    if (selectedNumber >= 0 && selectedNumber < createdCustomersCount)
                    {
                        // valid number
                        currentCustomer = customers[selectedNumber];
                    }
                    else
                    {
                        // invalid number
                        Console.WriteLine("Select a number of a customer from the list.");
                        continue;
                    }
                }



            } while (menuChoiceNumber != 5);
        }

        public static void HappyWriteLine(string text, ConsoleColor color)
        {
            var temp = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = temp;
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();
            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;


        }
    }

}
