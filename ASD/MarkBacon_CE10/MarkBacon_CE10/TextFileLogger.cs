﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE10
{
    public class TextFileLogger : Logger
    {
        private string _filename;

        public TextFileLogger(string filename)
        {
            _filename = filename;
        }

        public override void Log(string message)
        {
            using (StreamWriter writer = new StreamWriter(_filename, append: true))
            {
                writer.WriteLine($"[{DateTime.Now}] - {lineNumber++} - {message}");
            }               
        }
    }
}
