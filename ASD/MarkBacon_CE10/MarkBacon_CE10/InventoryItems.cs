﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MarkBacon_CE10
{
    class InventoryItems
    {

        public static List<InventoryItems> Inventory { get; set; } = new List<InventoryItems>();

        public static List<InventoryItems> ShoppingCart { get; set; }

        private string _title;
        private decimal _retailPrice;


        public string Title { get { return _title; } }
        public decimal RetailPrice { get { return _retailPrice; } }


        public InventoryItems(string title, decimal retailPrice)
        {
            _title = title;
            _retailPrice = retailPrice;
        }

        public override string ToString()
        {
            return $"Title: {_title}\n\tRetail Price: ${_retailPrice}";
        }
    }
}
