﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MarkBacon_CE10
{
    class Program
    {
        

        MySqlConnection _con = null;

        static void Main(string[] args)
        {
            Dictionary<Customers, List<InventoryItems>> accounts = new Dictionary<Customers, List<InventoryItems>>();
            Customers selectedCustomer = null;
            ILog logger = new TextFileLogger("log.txt");

            logger.Log("Starting System");

            Program instance = new Program();

            instance._con = new MySqlConnection();
            instance.Connect();

            // Read 50 from database
#if DEBUG
            DataTable data = instance.QueryDB("SELECT firstname, lastname FROM USERS LIMIT 10");
#else
            DataTable data = instance.QueryDB("SELECT firstname, lastname FROM USERS LIMIT 50");
#endif
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                string firstName = row["firstname"].ToString();
                string lastName = row["lastname"].ToString();
                

                Customers newCustomer = new Customers(firstName, lastName);
                
                accounts.Add(newCustomer, new List<InventoryItems>());
            }
           

            
            DataTable item = instance.QueryDB("SELECT title, retailPrice FROM ITEM LIMIT 50");
            DataRowCollection itemRows = item.Rows;

            foreach (DataRow row in itemRows)
            {
                string title = row["title"].ToString();
                decimal retailPrice = Convert.ToDecimal(row["retailPrice"]);

                if (retailPrice == 0)
                    retailPrice = 10;

               InventoryItems newItem = new InventoryItems(title, retailPrice);

               InventoryItems.Inventory.Add(newItem);
            }


            instance._con.Close();

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();

            

            bool running = true;

            while (running)
            {

                Console.Clear();
                if(selectedCustomer != null)
                    Console.WriteLine($"Selected Shopper: {selectedCustomer}");
                Console.WriteLine("");
                Console.WriteLine("---Main Menu---");
                Console.WriteLine("");
                Console.WriteLine("1. Select Current Shopper");
                Console.WriteLine("2. View Store Inventory");
                Console.WriteLine("3. View Shopping Cart");
                Console.WriteLine("4. Add Item to Cart");
                Console.WriteLine("5. Remove Item from Cart");
                Console.WriteLine("6. Complete Purchase");
                Console.WriteLine("7. Exit");

                string menuSelection = Utility.ReadString(promptMessage: "Please make a selection: ",
                                                   errorMessage: "You have entered an invalid option");

                menuSelection = menuSelection.ToLower();

                switch (menuSelection)
                {
                    case "1":
                    case "select current shopper":
                        {
                            selectedCustomer = Utility.SelectFromList(accounts.Keys.ToList(), addNewItemMessage: "Add a new shopper");

                            if(selectedCustomer == null)
                            {
                                string newCustomerFirstName = Utility.ReadString(promptMessage: "Please enter the first name of the shopper: ",                           
                                                                                 errorMessage: "Please enter a valid name");

                                string newCustomerLastName = Utility.ReadString(promptMessage: "Please enter the last name of the shopper: ", 
                                                                                errorMessage: "Please enter a valid name");

                                selectedCustomer = new Customers(newCustomerFirstName, newCustomerLastName);
                                accounts.Add(selectedCustomer, new List<InventoryItems>());
                            }

                            if (accounts.ContainsKey(selectedCustomer))
                            {
                                InventoryItems.ShoppingCart = accounts[selectedCustomer];
                            }

                            logger.Log($"Customer selected: {selectedCustomer}");
                        }

                        break;
                    case "2":
                    case "view store inventory":
                        {
                            Utility.DisplayList(InventoryItems.Inventory, separator: "--------------------------------------");
                        }

                        break;
                    case "3":
                    case "view shopping cart":
                        {
                            if (InventoryItems.ShoppingCart == null || InventoryItems.ShoppingCart.Count == 0)
                            {
                                Console.WriteLine("Please add some items to the cart first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            Utility.DisplayList(InventoryItems.ShoppingCart, separator: "--------------------------------------");
                        }

                        break;
                    case "4":
                    case "add item to cart":
                        {

                            if (InventoryItems.ShoppingCart == null || selectedCustomer == null)
                            {
                                Console.WriteLine("Please select a shopper first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            InventoryItems selectedItem = Utility.SelectFromList(InventoryItems.Inventory);
                            InventoryItems.Inventory.Remove(selectedItem);
                            InventoryItems.ShoppingCart.Add(selectedItem);

                            logger.Log($"Item added to the shopping cart: {selectedItem.Title}");
                        }

                        break;
                    case "5":
                    case "remove item from cart":
                        {
                            if (InventoryItems.ShoppingCart == null || InventoryItems.ShoppingCart.Count == 0)
                            {
                                Console.WriteLine("Please add some items to the cart first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            InventoryItems removeItem = Utility.SelectFromList(InventoryItems.ShoppingCart);
                            InventoryItems.ShoppingCart.Remove(removeItem);
                            InventoryItems.Inventory.Add(removeItem);
                            logger.Log($"Item removed from the shopping cart: {removeItem.Title} (added back to inventory)");
                        }

                        break;
                    case "6":
                    case "complete purchase":
                        {
                            if (InventoryItems.ShoppingCart == null || selectedCustomer == null)
                            {
                                Console.WriteLine("Please select a shopper first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            // Sum up the total of the cart
                            decimal cartTotal =  InventoryItems.ShoppingCart.Sum(shoppingCartItem => shoppingCartItem.RetailPrice);

                            // Show it
                            Console.WriteLine($"The total of the cart is {cartTotal}");
                            // Remove user
                            accounts.Remove(selectedCustomer);

                            logger.Log($"Shopper {selectedCustomer} purchased a total of {cartTotal}");

                            selectedCustomer = null;
                            InventoryItems.ShoppingCart = null;
                        }

                        break;
                    case "7":
                    case "exit":
                        {
                            Console.WriteLine("Bye bye");
                            running = false;
                        }


                        break;
                }
                Utility.PauseBeforeContinuing();
            }
        }

        void Connect()
        {
            BuildConString();

            try
            {
                _con.Open();
                Console.WriteLine("Connection Successful");
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);

            }
        }


        void BuildConString()
        {
            string ip = "";
            //alternative formats
            // "c:\\VFW\\connect.txt"
            // @"c:\VFW\connect.txt"
            using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string conString = $"Server={ip};";

            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=example_1709;";
            conString += "port=8889;";

            _con.ConnectionString = conString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);
            
            return data;
        }

    }

}