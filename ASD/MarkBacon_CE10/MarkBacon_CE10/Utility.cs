﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MarkBacon_CE10
{
    class Utility
    {
        public static void PauseBeforeContinuing()
        {
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        public static T SelectFromList<T>(
            List<T> list, 
            string promptMessage = "Please choose an item from the list above: ",
            string errorMessage="Please enter a valid choice.",
            string addNewItemMessage = ""  )
        {
            DisplayList(list);

            int choseOne = -1;
            int upperBound = list.Count;

            if (!string.IsNullOrEmpty(addNewItemMessage))
            {
                Console.WriteLine($"{++upperBound} - {addNewItemMessage}");
            }  

            while (choseOne <= 0 || choseOne > upperBound)
            {
                choseOne = ReadInteger(promptMessage: promptMessage, 
                                        errorMessage: errorMessage);
            }

            choseOne--;

            return list.ElementAtOrDefault(choseOne); 
            
        }

        public static void DisplayList<T>(List<T> list, string separator = "")
        {
            if (list.Count == 0)
            {
                Console.WriteLine("There are no items in the list");
                return;
            }

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {list[i].ToString()}");
                if(!string.IsNullOrEmpty(separator))
                    Console.WriteLine(separator);
            }
        }

        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

    }
}
