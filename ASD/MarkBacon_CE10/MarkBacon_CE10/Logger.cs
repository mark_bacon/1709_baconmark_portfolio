﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE10
{
public abstract class Logger: ILog
    {
        //logger needs a protected static int field called lineNumber
        // this field will track the number of lines logged which will
        // used when data is logged
        //logger does not have to provide a body for the ILog methods
        //it can declare them as abstract so that its children have to implement them

        protected static int lineNumber;
                
        public virtual void Log(string message)
        {
            
        }

        public virtual void LogD(string message)
        {            
        }

        public virtual void LogW(string message)
        {            
        }
    }
}
