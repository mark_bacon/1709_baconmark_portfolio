﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace MarkBacon_CE10
{
    class Customers
    {
        //public static List<Customers> CustomerList { get; set; } = new List<Customers>();
        

        private string _firstName;
        private string _lastName;
       

        public string FirstName { get { return _firstName; } }
        public string LastName { get { return _lastName; } }
        

        public Customers(string firstName, string lastName)
        {
            _firstName = firstName;
            _lastName = lastName;
        }

        public override string ToString()
        {
            return $"{_firstName} {_lastName}";
        }


    }
}
