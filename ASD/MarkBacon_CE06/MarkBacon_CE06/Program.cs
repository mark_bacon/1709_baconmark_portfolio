﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            //Utility methods like ReadString belong in their own class instead of dumping them in the Program class.
            //Classes: Excellent 20/20
            // Dictionary and List Usage: Excellent 25 / 25
            //Menu: Excellent 25 / 25
            //Main: Excellent 15 / 15
            //Input Validation: Excellent 15 / 15, typing exit does not work because the case you check in the switch is "Exit".
            // ***fixed "exit" in switch statement

            CollectionManager currentManager = null;

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("");
                Console.WriteLine("---Main Menu---");
                Console.WriteLine("");
                Console.WriteLine("1. Create Collection Manager");
                Console.WriteLine("2. Create a card");
                Console.WriteLine("3. Add a card to a collection");
                Console.WriteLine("4. Remove a card from a collection");
                Console.WriteLine("5. Display a collection");
                Console.WriteLine("6. Display all collections");
                Console.WriteLine("7. Exit");

                //capture user selection
                string menuSelection = ReadString(promptMessage: "Please make a selection: ",
                                                errorMessage: "You have entered an invalid option");

                menuSelection = menuSelection.ToLower();

                switch (menuSelection)
                {
                    case "1":
                    case "create collection manager":
                        {
                            // Create new collection manager 
                            //Create collection manager - this option creates a new CollectionManager object and assigns it to the 
                            //currentManager variable.  This is the one instance where created items can be lost. 
                         

                            

                            if(currentManager != null)
                            {
                                string verifyCollection = ReadString(promptMessage: "Are you sure you want to create a new manager? (yes/no)",
                                                                errorMessage: "Please enter yes or no.");
                                if (!verifyCollection.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                                { 
                                    continue;
                                }
                            }

                            currentManager = new CollectionManager();
                            Console.WriteLine( "Collection manager was created successfully");
                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            // Add a null check here for the manager

                            if (currentManager == null)
                            {
                                Console.WriteLine("Please create a collection first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            //create a card
                            //Create a card - this option prompts the user for the input necessary to create a new Card and adds 
                            //it to the available cards list in the currentManager.
                            Card newCard = new Card();

                            newCard.name = ReadString(promptMessage: "Please type the name of the card you would like to add: ",
                                                        errorMessage: "You have not typed a name for the card.");
                            newCard.description = ReadString(promptMessage: "Please type the description of the card you would like to add: ",
                                                        errorMessage: "You have not typed a description for the card.");
                            newCard.value = ReadDecimal(promptMessage: "Please type the value of the card you would like to add: ",
                                                        errorMessage: "You have not typed a valid value for the card.");

                            currentManager.CardList.Add(newCard);

                            Console.WriteLine("You have created a card successfully.");
                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            //Add a card to a collection - this option prompts the user for a collection to add the card to, presents 
                            //the user with a list of the available cards, the user selects a card, the selected card is removed from 
                            //the available cards list, and is added to the specified collection.  If the collection does not exist it 
                            //should be created and the card added to it.
                            if (currentManager == null)
                            {
                                Console.WriteLine("Please create a collection manager first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            if (currentManager.CardList.Count == 0)
                            {
                                Console.WriteLine("Please create a card first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            
                            string selectedCollectionName = currentManager.SelectCollection(message: "Please choose one of the collections above or type a new name: ");

                            if (!currentManager.CollectionDictionary.ContainsKey(selectedCollectionName))
                            {
                                currentManager.CollectionDictionary.Add(selectedCollectionName, new List<Card>()); // Created a new pile
                            }

                            Card selectedCard = currentManager.SelectCard();

                            // remove the card from the card list
                            currentManager.CardList.Remove(selectedCard);

                            // add the card to the collection
                            currentManager.CollectionDictionary[selectedCollectionName].Add(selectedCard);

                            Console.WriteLine($"Added \"{selectedCard.name}\" to {selectedCollectionName}.");
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            //Remove a card from a collection -this option prompts the user for a collection to remove the card from, 
                            //presents the user with a list of cards in that collection, the user selects a card, the selected card is 
                            //removed from the collection and is added to the list of available cards.
                            if (currentManager == null)
                            {
                                Console.WriteLine("Please create a collection manager first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            
                            
                            string selectedCollectionName = currentManager.SelectCollection();

                            if (!currentManager.CollectionDictionary.ContainsKey(selectedCollectionName))
                            {
                                Console.WriteLine("The collection you entered does not exist.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            if (currentManager.CollectionDictionary[selectedCollectionName].Count == 0)
                            {
                                Console.WriteLine("Add a card to the collection first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }



                            Card selectedCard = currentManager.SelectCardFromTheCollection(selectedCollectionName);

                            // remove the card from the colection
                            currentManager.CollectionDictionary[selectedCollectionName].Remove(selectedCard);

                            // add the card to the card list
                            currentManager.CardList.Add(selectedCard);

                            Console.WriteLine($"Removed \"{selectedCard.name}\" from {selectedCollectionName}.");
                        }
                        break;
                    case "5":
                    case "display a collection":
                        {
                            //Display a collection - this option prompts the user for a collection to display and then either displays
                            //all of the cards in the collection or displays a message if the collection does not exist.
                            if (currentManager == null)
                            {
                                Console.WriteLine("Please create a collection manager first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            string selectedCollectionName = currentManager.SelectCollection();

                            if (!currentManager.CollectionDictionary.ContainsKey(selectedCollectionName))
                            {
                                Console.WriteLine("The collection you entered does not exist.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }
                            else
                            {
                                // Display
                                List<Card> cards = currentManager.CollectionDictionary[selectedCollectionName];
                                currentManager.DisplayCards(cards);
                            }
                        }
                        break;
                    case "6":
                    case "display all collections":
                        {
                            //Display all collections - this option loops through all of the collections in the dictionary and displays
                            //   all of the cards in each collection.
                            if (currentManager == null)
                            {
                                Console.WriteLine("Please create a collection manager first.");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            if(currentManager.CollectionDictionary.Count == 0)
                            {
                                Console.WriteLine("There are no collections");
                                Utility.PauseBeforeContinuing();
                                continue;
                            }

                            foreach (string collectionName in currentManager.CollectionDictionary.Keys)
                            {
                                Console.WriteLine($"Displaying: {collectionName}");
                                List<Card> cards = currentManager.CollectionDictionary[collectionName];
                                currentManager.DisplayCards(cards);
                                Console.WriteLine("--------------------------------------------------------");
                            }

                        }
                        break;
                    case "7":
                    case "exit":

                        {
                            Console.WriteLine("Bye bye");
                            running = false;
                        }
                        break;


                }


                Utility.PauseBeforeContinuing();
            }
            
        }


        public static string ReadString(string promptMessage, string errorMessage = "Please enter a string.")
        {

            Console.WriteLine(promptMessage);
            string result = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(errorMessage);
                result = Console.ReadLine();

            }
            return result;
        }

        static void PrintValues<t>(List<t> _list)
        {
            if (_list.Count > 0)
            {
                Console.WriteLine("\nList Values-----------");
                int i = 0;
                foreach (t item in _list)
                {
                    Console.WriteLine(" [" + i + "] Value: " + item);
                    ++i;
                }
            
            }
            else
            {
                Console.WriteLine("No values to print.");
            }

            Console.WriteLine("\n");
        }

        static void PrintValuesDictionary<tk, tv>(Dictionary<tk, tv> _dictionary)
        {
            if (_dictionary.Count>0)
            {
                Console.WriteLine("\nDictionary Keys & Values:\n-------------------------");
                int i = 0;
                foreach(tk key in _dictionary.Keys)
                {
                    Console.WriteLine(i + ". Key: \""+ key +"\" Value: "+ _dictionary[key]);
                    ++i;
                }
                
            }
            else
            {
                Console.WriteLine("No values to print.");
            }
            Console.WriteLine("\n");
        }

        public static decimal ReadDecimal(string promptMessage, string errorMessage = "Please enter a decimal.")
        {
            decimal result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(decimal.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

        public static int ReadInteger(string promptMessage, string errorMessage = "Please enter a integer.")
        {
            int result;
            Console.WriteLine(promptMessage);
            string temp = Console.ReadLine();

            while (!(int.TryParse(temp, out result)))
            {
                Console.WriteLine(errorMessage);
                temp = Console.ReadLine();
            }

            return result;
        }

    }
}


