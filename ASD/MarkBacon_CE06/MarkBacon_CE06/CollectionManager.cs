﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE06
{
    class CollectionManager
    {
        public List<Card> CardList { get; set; } = new List<Card>();

        public Dictionary<string, List<Card>> CollectionDictionary { get; set; } = new Dictionary<string, List<Card>>();


        public string SelectCollection(string message = "Please choose one of the collections above: ")
        {
            // List the colletions
            ShowCollections();

            // Prompt th user to choose one of them
            string chosenOne = Program.ReadString(promptMessage: message,
                                                    errorMessage: "You have entered an invalid input.");
            return chosenOne;
        }

        private void ShowCollections()
        {
            if (CollectionDictionary.Count == 0)
            {
                Console.WriteLine("There are no collections available");
                return;
            }

            foreach (string key in CollectionDictionary.Keys)
            {
                Console.WriteLine($"{key}");
            }
        }

        public Card SelectCard()
        {
           DisplayCards(CardList);

            int choseOne = -1;

            while (choseOne <= 0 || choseOne > CardList.Count)
            {
                choseOne = Program.ReadInteger(promptMessage: "Please choose a card from the list above: ", errorMessage: "You did not enter a number.");
            }

            choseOne--;

            return CardList[choseOne];
        }

      

        public Card SelectCardFromTheCollection(string collectionName)
        {
            
            List<Card> cards = CollectionDictionary[collectionName];
            DisplayCards(cards);
            int choseOne = -1;

            while (choseOne < 0 || choseOne > cards.Count)
            {
                choseOne = Program.ReadInteger(promptMessage: "Please choose a card from the list above: ", errorMessage: "You did not enter a number.");
            }

            choseOne--;

            return cards[choseOne];

        }

       

        
        public void DisplayCards(List<Card> cards)
        {
            if (cards.Count == 0)
            {
                Console.WriteLine("There are no cards");
                return;
            }

            for (int i = 0; i < cards.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {cards[i].name}");
            }
        }
        }
}
