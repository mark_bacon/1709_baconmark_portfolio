﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE08
{
    public class Record
    {
        public List<string> Values { get; set; }

        public Record(string line)
        {
            Values = line.Split('|').ToList();
        }

        public string ToJson(List<string> fieldNames)
        {
            string result = "{";
            
            for (int i = 0; i < Values.Count - 1; i++)
            {
                if (i != 0)
                    result += ",";
                result += $@"""{fieldNames[i]}"" : ""{Values[i]}""";
            }

            result += "}";

            return result;
        }
    }
}
