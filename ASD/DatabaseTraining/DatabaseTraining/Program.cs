﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace DatabaseTraining
{
    class Program
    {
        MySqlConnection _con = null;

        static void Main(string[] args)
        {
            Program instance = new Program();

            instance._con = new MySqlConnection();
            instance.Connect();

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();

            DataTable data = instance.QueryDB("SELECT make, model, year FROM vehicle LIMIT 30");
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                Console.WriteLine($"Make: {row["make"].ToString()}");
                Console.WriteLine($"Model: {row["model"].ToString()}");
                Console.WriteLine($"Year: {row["year"].ToString()}");
                Console.WriteLine("");
                //add Convert.ToInt16() if we need to use the numbers but since we are just
                //displaying the results it is not necessary. In this example we are converting all to strings.


            }

            instance._con.Close();

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }

        void Connect()
        {
            BuildConString();

            try
            {
                _con.Open();
                Console.WriteLine("Connection Successful");
            }
            catch(MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    case 0:
                        {
                            msg = e.ToString();
                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;
                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username/password";
                            break;
                        }
                    default:
                        {
                            msg = e.ToString();
                            break;
                        }
                }

                Console.WriteLine(msg);

            }
        }

        void BuildConString()
        {
            string ip = "";
            //alternative formats
            // "c:\\VFW\\connect.txt"
            // @"c:\VFW\connect.txt"
            using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                ip = sr.ReadLine();
            }

            string conString = $"Server={ip};";

            conString += "uid=dbsAdmin;";
            conString += "pwd=password;";
            conString += "database=example_1709;";
            conString += "port=8889;";

            _con.ConnectionString = conString;
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapter = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapter.SelectCommand.CommandType = CommandType.Text;
            adapter.Fill(data);

            return data;
        }
    }
}
