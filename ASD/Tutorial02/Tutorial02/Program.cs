﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial02
{
    class Program
    {
        static void Main(string[] args)
        {

            //method overloading - calling
            int anyNumber = GetInt();
            int numberBetween1and10 = GetInt(1, 10);
            //Display the result using string interpolation
            Console.WriteLine($"You picked the number: {anyNumber}");
            Console.WriteLine($"and between (1-10): {numberBetween1and10}");

            // Classes
            // User
            Console.WriteLine("Please enter your name: ");
            string name = Console.ReadLine();
            Console.WriteLine($"{name}, please enter your address: ");
            string address = Console.ReadLine();
            int age = GetInt(0, 120, $"{name}, please enter your age (1-120): ");

            

            //declare a variable of type user
            User user;
            user = new User(name, address, age); //Create a new instance or object of type user
            // these can ber done on a single line 
            // User user = new User(name, address, age)

            //Preferences
            bool shouldAutoPlay = GetBool("Should videos auto play? (yes/no)");
            double volumeLevel = GetDouble(1, 10, $"Enter a desired volume for sounds to play at. (1-10): ");

            Preferences prefs = new Preferences(shouldAutoPlay, volumeLevel);
            
            //Null checking
            if(user != null)
            {
                user.SetPreferences(prefs);
            }
        }
       
    }

}
