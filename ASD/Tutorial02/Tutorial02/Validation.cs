﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial02
{
    class Validation
    {

        //Scope & Static
        //Global scope

        //method signature
        //adding message parameter with a default value of "Enter an interger: "
        static int GetInt(string message = "Enter an integer: ")
        // end method signature
        //body of method
        {
            int validatedInt = 0;
            string input = null;

            do
            {
                Console.Write(message); //Displaying message instead of the previous string
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out validatedInt));

            return validatedInt;
        }
        //End body of method

        //Method Signature
        static int GetInt(int min, int max, string message = "Enter an integer: ")
        {
            int validatedInt = 0;
            string input = null;

            do
            {
                Console.Write(message); //Displaying message instead of previous string.
                input = Console.ReadLine();
                //added () after the ! and around the rest of the validation logic.
                //added && (valdidatedInt >= min && validatedInt <= max) to force the value
                // to be between min and max inclusive
            } while (!(Int32.TryParse(input, out validatedInt) && (validatedInt >= min && validatedInt <= max)));

            return validatedInt;
        }

        static bool GetBool(string message = "Please enter Y or N: ")
        {
            bool selectedOption = true; // default value so visual studio doesnt error on using 
                                        // unassigned local variable

            bool keepAsking = true;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                    case "t":
                    case "true":
                        {
                            selectedOption = true;
                            keepAsking = false;
                        }
                        break;
                    case "n":
                    case "no":
                    case "f":
                    case "false":
                        {
                            selectedOption = true;
                            keepAsking = false;
                        }
                        break;
                }
            } while (keepAsking);

            return selectedOption;
        }

        static double GetDouble(string message = "Enter a number: ")
        {
            double validatedDouble;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
            } while (!Double.TryParse(input, out validatedDouble));

            return validatedDouble;
        }

        static double GetDouble(double min, double max, string message = "Enter a number: ")
        {
            double validatedDouble;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
            } while (!Double.TryParse(input, out validatedDouble));

            return validatedDouble;

        }

    }
}
