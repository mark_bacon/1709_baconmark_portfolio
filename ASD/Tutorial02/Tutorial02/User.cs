﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tutorial02
{
    class User
    {
        string _name;
        string _address;
        int _age;

        //Add field of type Preferences
        Preferences _prefs;


        //Create the constructor
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;
        }



        //create getters for each 
        public string GetName()
        {
            return _name;
        }

        public string GetAddress()
        {
            return _address;
        }

        public int GetAge()
        {
            return _age;
        }
        //add getter for preferences field
        public Preferences GetPreferences()
        {
            return _prefs;
        }

        //create setters for each
        public void SetName(string name)
        {
            _name = name;
        }

        public void SetAddress(string address)
        {
            _address = address;
        }

        public void SetAge(int age)
        {
            _age = age;
        }
        //Add setter for preferences field
        public void SetPreferences(Preferences prefs)
        {
            _prefs = prefs;
        }

    }
}
