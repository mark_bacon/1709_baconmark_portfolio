﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkBacon_CE01
{
    class Program
    {
        /* FEEDBACK and corrections to make:
         * William JensenTue Nov 28 @ 1:42 pm CST
Structure/Efficiency: Good 7.5/10, Infinite loop if an invalid selection number is selected. lines 39 - 46 will always fail because menuChoiceString is not parsed into menuChoiceNumber after the first loop.
Array Setup: Good 15/20, The length array is supposed to hold floats and not ints.
User Menu: Excellent 20/20
Input Validation: Excellent 20/20
Search: Excellent 20/20
Final Output: Excellent 10/10
         * 
         * 
         */

        static void Main(string[] args)
        {

            Console.WriteLine("---- Big Blue Fish ----");

            string[] fishColor = new string[] { "red", "blue", "green", "yellow", "red", "blue", "green", "yellow", "red", "blue", "green", "yellow" };

            int[] fishLength = new int[] { 5, 4, 7, 10, 12, 6, 8, 9, 1, 3, 2, 11 };
            while (true)
            {
                Console.WriteLine("\r\nPlease select a number for a color of fish:");
                Console.WriteLine("Chose:\r\n(1) for Red\r\n(2) for Blue\r\n(3) for Green\r\n(4) for Yellow ");

                //capture user selection
                String menuChoiceString = Console.ReadLine();

                //define variable to convert number
                int menuChoiceNumber;

                while (!(int.TryParse(menuChoiceString, out menuChoiceNumber)))
                {
                    //tell user what happened
                    Console.WriteLine("You have entered something other than a number.\r\nPlease enter a number from 1 to 4:");

                    //re-define the variable to save response
                    menuChoiceString = Console.ReadLine();
                }
                //this is an if statement that will catch if somebody enters a number NOT between 1 and 4
                while (!(menuChoiceNumber >= 1 && menuChoiceNumber <= 4))
                {
                    //prompt user that they have entered a choice that is not an option
                    Console.WriteLine("You have entered a number that is not an available choice. Please try again:");

                    //re-define the variable to save response
                    menuChoiceString = Console.ReadLine();
                }


                int biggestLength = 0;

                string selectedFishColor = null;

                switch (menuChoiceNumber)
                {
                    case 1:
                        selectedFishColor = "red";
                        break;
                    case 2:
                        selectedFishColor = "blue";
                        break;
                    case 3:
                        selectedFishColor = "green";
                        break;
                    case 4:
                        selectedFishColor = "yellow";
                        break;
                }

             

                
                    
                    for (int i = 0; i < fishColor.Length; ++i)
                    {
                        var currentFishColor = fishColor[i];
                        var currentFishLength = fishLength[i];

                        //if (currentFishColor == selectedFishColor)
                        if (currentFishColor.Equals(selectedFishColor))
                        {
                            // Check For the length
                            if (currentFishLength > biggestLength)
                            {
                                biggestLength = currentFishLength;
                            }
                        }

                    }


                    Console.WriteLine("The largest {1} fish is {0}.", biggestLength, selectedFishColor);

                //finish
              


            }
        }
    }
}
        
    

